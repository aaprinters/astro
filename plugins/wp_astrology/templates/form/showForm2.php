<?php
function showForm2($atts) {

$args = array(
'post_type' => 'stern_taxi_car_type',
'nopaging' => true,
'order'     => 'ASC',
'orderby' => 'meta_value',
'meta_key' => '_stern_taxi_car_type_organizedBy'
);

$allTypeCars = get_posts( $args );	
$args = array(
'post_type' => 'stern_listAddress',
'nopaging' => true,
'meta_query' => array(

array(
'key'     => 'typeListAddress',
'value'   => 'destination',
'compare' => '=',
),				

array(
'key'     => 'isActive',
'value'   => 'true',
'compare' => '=',
),
),				
);

$allListAddressesDestination = get_posts( $args );			
$backgroundColor= get_option('stern_taxi_fare_bcolor');
if($backgroundColor!="")
{
$backgroundColor='background-color:'.stern_taxi_fare_hex2rgba($backgroundColor,get_option('stern_taxi_fare_bTransparency'));
}
global $woocommerce;
$currency_symbol = get_woocommerce_currency_symbol();
if (isBootstrapSelectEnabale()=="true")
{
$class = "selectpicker show-tick";
} else {
$class = "form-control";
}

$class_full_row12 = (get_option('stern_taxi_fare_full_row') != "true") ? "class='col-lg-12 col-md-12 col-sm-12 col-xs-12'" : "class='col-lg-12 col-md-12 col-sm-12 col-xs-12'";

$class_full_row6 = (get_option('stern_taxi_fare_full_row') != "true") ? "class='col-lg-6 col-md-6 col-sm-12 col-xs-12'" : "class='col-lg-12 col-md-12 col-sm-12 col-xs-12'";

$class_full_row3 = (get_option('stern_taxi_fare_full_row') != "true") ? "class='col-lg-3 col-md-3 col-sm-12 col-xs-12'" : "class='col-lg-12 col-md-12 col-sm-12 col-xs-12'";

$classMain = (get_option('stern_taxi_fare_form_full_row') != "true") ? "class='col-xs-12 col-sm-6 col-lg-6'" : "class='col-xs-12 col-sm-12 col-lg-12'";
?>

<div class="container1 row">
<div class="col col-lg-9 stern-taxi-fare">

<?php

$form_setVehicleInfo = get_option( 'vehicle_dataTpl' );
$coverage_dataTpl = get_option( 'frontend_dataTpl' );

foreach ( $coverage_dataTpl as $data_dataTpl ) {
	$veh_cat_2[] = $data_dataTpl[4]; 	
	$tpl_cylenders[] = $data_dataTpl[6]; 		
} 

foreach ( $form_setVehicleInfo as $data_vbrand ) {
$name_arr[] = $data_vbrand[0]; 
$medel_year[] = $data_vbrand[1]; 
$modelno_submodel[] = $data_vbrand[2]; 
$vehicle_cat[] = $data_vbrand[3]; 			
} 

$vehicle_brand = array_unique($name_arr);
$veh_medel_year = array_unique($medel_year);
$veh_modelno = array_unique($modelno_submodel);
$veh_vehicle_cat = array_unique($vehicle_cat);
//$veh_sub_model = array_unique($sub_model);
$vehicle_cat_2 = array_unique($veh_cat_2);
//$repair_condition = array_unique($repair_con);
$tpl_cylenders_arr = array_unique($tpl_cylenders);
?>

<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700" rel="stylesheet"> 
<link href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css" rel="stylesheet">

<div style="@display:none;" id="bread_crums_custom">
<div class="steps clearfix">
<ul id="tablist11">
<li class="first">
<a id="goto_form" ><span class="number">Vehicle Details</span></a>
</li>
<li class="second">
<a id="goto_table"><span class="number">Insurance Quote</span></a>
</li>
<!--<li class="second">
<a id="goto_checkout"><span class="number">2 Goto Checkout</span></a>
</li> -->			
</ul>
</div>
</div>
<?php global $redux_demo; ?>
<div class="smart-wrap">
<div id="XXstep1" class="smart-forms smart-container wrap">
<div class="form-body stp-six steps-theme-primary steps-theme-blue11">

<form id="smart-form" method="post">
<h2>WIG PHOTO</h2>
<fieldset>

<!-- STEP 1 -->

<div class="frm-row">
<div class="section colm colm12">
<h2>UPLOAD YOUR WIG PHOTO</h2>
</div>

<div class="frm-row">
<div class="section colm colm12">	
		<div class="dropzone" id="myDropzone">
			<h3 class="dz-drop-title"> Upload Your Wig Photo </h3>
			<p class="dz-clicker"> <span> Select Photo </span> </p>                                                            
			<div class="fallback">
				<input name="file" type="file">
			</div>
		</div>   		
<input type="hidden" id="upload_imgPrice" value="<?php echo $redux_demo['step1_price'];?>">
<input type="hidden" id="total_price_image" value="0.00">
<input type="hidden" id="total_price_length" value="0.00">
<input type="hidden" id="total_price_texture" value="0.00">

</div>
</div>


</div>
</fieldset>

<h2>Length</h2>
<fieldset>

<!-- STEP 2 -->                                           
<div class="frm-row ">						
<div class="colm colm12">
<h2 class="">WANT MORE INCHES?</h2>
</div>

	<!-- Sample Wig Image -->
	<div class="colm colm6">

	<div class="cd-product-builder">
	  <div class="cd-builder-steps">
			<ul>       	        	
				<li data-selection="length-page" class="builder-step length-slide active" style="overflow-x: hidden;">
					<section class="cd-step-content">
					
						<div class="product-box">
							<div class="product-img-box wow zoomIn" style="visibility: visible; animation-name: zoomIn;">
								   
								<div class="background-product">
								<ul id="dummy_girl" class="loaded">
								<?php 
									$redux_length = $redux_demo['step2_length'];
									$redux_length_price = $redux_demo['step2_length_price'];
									$length_girl = array_combine($redux_length, $redux_length_price);
									$n=0;
									foreach($length_girl as $length=>$redux_length_gril){					
									if($n==6){	
										//echo '<li id="'.$length.'" value="'.$redux_length_price2.'" class="length-active" style="display: inline;">'.$length.'"</li>';
										echo '<li id="l-'.$length.'" class="line_selected" data-length-label="'.$length.'" data-content="Length: '.$length.'&quot; - $'.$redux_length_gril.'" data-length-price="'.$redux_length_gril.'">
										<em>&nbsp;'.$length.'</em><span></span><h9>&nbsp;'.$length.'</h9>
										</li>';
									}else{
										echo '<li id="l-'.$length.'" class="" data-length-label="'.$length.'" data-content="Length: '.$length.'&quot; - $'.$redux_length_gril.'" data-length-price="'.$redux_length_gril.'">
										<em>&nbsp;'.$length.'</em><span></span><h9>&nbsp;'.$length.'</h9>
										</li>';
									}
									$n++;
									}
								?>
								
<!--								
								<li id="l-8" class="" data-length-label="8" data-content="Length: 8&quot; - $70.00" data-length-price="70.00" data-variant-id="410026475540">
									<em>&nbsp;    8"</em><span></span><h9>&nbsp;8"</h9>
								</li>                                    

								<li id="l-10" class="" data-length-label="10" data-content="Length: 10&quot; - $75.00" data-length-price="75.00" data-variant-id="410026508308">
									<em>10"</em>
									<span></span>
									<h9>10"</h9>
								</li>


								<li id="l-12" class="" data-length-label="12" data-content="Length: 12&quot; - $80.00" data-length-price="80.00" data-variant-id="410026541076">
								<em>12"</em>
								<span></span>
								<h9>12"</h9>
								</li>


								<li id="l-14" class="" data-length-label="14" data-content="Length: 14&quot; - $85.00" data-length-price="85.00" data-variant-id="410026573844">
								<em>14"</em>
								<span></span>
								<h9>14"</h9>
								</li>


								<li id="l-16" class="" data-length-label="16" data-content="Length: 16&quot; - $90.00" data-length-price="90.00" data-variant-id="410026606612">
								<em>16"</em>
								<span></span>
								<h9>16"</h9>
								</li>


								<li id="l-18" class="" data-length-label="18" data-content="Length: 18&quot; - $95.00" data-length-price="95.00" data-variant-id="410026639380">
								<em>18"</em>
								<span></span>
								<h9>18"</h9>
								</li>


								<li id="l-20" class="" data-length-label="20" data-content="Length: 20&quot; - $100.00" data-length-price="100.00" data-variant-id="410026672148">
								<em>20"</em>
								<span></span>
								<h9>20"</h9>
								</li>


								<li id="l-22" class="line_selected" data-length-label="22" data-content="Length: 22&quot; - $105.00" data-length-price="105.00" data-variant-id="410026704916">
								<em>22"</em>
								<span></span>
								<h9>22"</h9>
								</li>


								<li id="l-24" class="" data-length-label="24" data-content="Length: 24&quot; - $110.00" data-length-price="110.00" data-variant-id="410026737684">
								<em>24"</em>
								<span></span>
								<h9>24"</h9>
								</li>


								<li id="l-26" class="" data-length-label="26" data-content="Length: 26&quot; - $115.00" data-length-price="115.00" data-variant-id="410026770452">
								<em>26"</em>
								<span></span>
								<h9>26"</h9>
								</li>


								<li id="l-28" class="" data-length-label="28" data-content="Length: 28&quot; - $120.00" data-length-price="120.00" data-variant-id="410026803220">
								<em>28"</em>
								<span></span>
								<h9>28"</h9>
								</li>


								<li id="l-30" class="" data-length-label="30" data-content="Length: 30&quot; - $125.00" data-length-price="125.00" data-variant-id="410026835988">
								<em>30"</em>
								<span></span>
								<h9>30"</h9>
								</li>
-->										
								</ul>
								</div>
								<p></p>
								
						  </div>
					  </div>
					</section>
				</li>
				   
			</ul>
		</div>
	</div>
	</div>
	<!--End Sample Wig -->

	<div class="colm colm6">
	
	<!-- Range Slider -->	
	<?php //print_r($redux_demo['step2_length']);?>
	<div class="product-shop wow zoomIn animated" style="visibility: visible; animation-name: zoomIn;">                      
		<ul id="lenth_values" class="rule-length">
			<?php 
				$redux_length = $redux_demo['step2_length'];
				$redux_length_price = $redux_demo['step2_length_price'];
				$length_comb = array_combine($redux_length, $redux_length_price);
				
				$n=0;
				foreach($length_comb as $length=>$redux_length_price2){					
				if($n==6){	
					echo '<li id="'.$length.'" value="'.$redux_length_price2.'" class="length-active" style="display: inline;">'.$length.'"</li>';
				}else{
					echo '<li id="'.$length.'" value="'.$redux_length_price2.'" class="" style="display: inline;">'.$length.'"</li>';
				}
				$n++;
				}
			?>	
<!-- 		<li id="8" value="8" class="" style="display: inline;">8"</li>
			<li id="10"  value="10" class="" style="display: inline;">10"</li>
			<li id="12" value="12" class="" style="display: inline;">12"</li>
			<li id="14" value="14" class="" style="display: inline;">14"</li>
			<li id="16" value="16" class="" style="display: inline;">16"</li>
			<li id="18" value="18" class="" style="display: inline;">18"</li>
			<li id="20" value="20" class="length-active" style="display: inline;">20"</li>
			<li id="22" value="22" class="" style="display: inline;">22"</li>
			<li id="24" value="24" class="" style="display: inline;">24"</li>
			<li id="26" value="26" class="" style="display: inline;">26"</li>
			<li id="28" value="28" class="" style="display: inline;">28"</li>                            
			<li id="30" value="30" class="" style="display: inline;">30"</li> -->
		</ul>
		<div class="clear"></div>
		<p></p>
		<div class="""slidecontainer ui-slider ui-corner-all ui-slider-horizontal ui-widget ui-widget-content">
		  <input type="range" min="8" max="30" step="2" class="slider ui-widget-content" id="myRange">		  
		</div>   
		<p></p>   
		<p class="bx-btm">Length: <span id="length_chosen">22"</span> </p>
		<center>Selector is for illustrative purposes only.<br>
		<font style="text-transform: uppercase; font-size:12px; letter-spacing: 2px;" class="gray_text">(Actual hair length may vary in proportion to height and installation)</font>
		</center>
	</div>	
	</div>	
	

<script>
var slider = document.getElementById("myRange");
var output = document.getElementById("length_chosen");
output.innerHTML = slider.value;

  slider.oninput = function() {
  output.innerHTML = this.value;
  var current_val = this.value;
  var listItems_slider = jQuery("#lenth_values li");
  var dummy_girl_ul = jQuery("#dummy_girl li");
  
  // Range Slider Code
  listItems_slider.each(function(idx, li) {
	var length_li = jQuery(li);
	var id1 = length_li.attr('id');	
	var selectorid1 = "#"+id1;	
		jQuery(selectorid1).removeClass("length-active");
	
  });

  listItems_slider.each(function(idx, li) {
	var length_li = jQuery(li);
	var id = length_li.attr('id');
	var length_val = length_li.attr('value');
		
	var selectorid = "#"+id;	
	
	if(current_val == id ){
		//console.log(current_val);
		console.log("Selected Val.."+length_val);
		show_price_length(length_val);
		jQuery(selectorid).addClass("length-active");
	}	
  });
  
  //Dummy Wig Code
  dummy_girl_ul.each(function(idx, li1) {
	var lady_li = jQuery(li1);
	var id1 = lady_li.attr('data-length-label');
	console.log("id1.."+id1);
	var sel_lady = "#l-"+id1;	
		jQuery(sel_lady).removeClass("line_selected");
	
  });

  dummy_girl_ul.each(function(idx, li1) {
	var lady_li = jQuery(li1);
	var id = lady_li.attr('data-length-label');	
	var sel_lady1 = "#l-"+id;	
	
	if(current_val == id ){
		console.log(current_val);
		jQuery(sel_lady1).addClass("line_selected");
	}	
  });   
}

</script>

</div>

</fieldset> 

<h2>Texture</h2>
<fieldset>

<!-- STEP 3 -->                                           

<div class="frm-row ">						
<div class="section colm colm12">
<h2 class="step2_labels">IT’S ALL ABOUT TEXTURE!</h2>
<div class="">
	<h3>Select the hair texture you would like for your wig.</h3>
	<h3>*Prices may vary according to your desired hair texture.</h3>
</div>
</div>

<div class="section colm colm12">

<!-- Cs Circular -->
<div class="cd-builder-steps">
		<ul id="texture_ul">       	          
            <li data-selection="texture-page" class="builder-step active" style="overflow-x: hidden;">
                <section class="cd-step-content">
                    
                    <div class="circular-box wow zoomIn" style="visibility: visible; animation-name: zoomIn;">
                        <div class="cs-selectbox circular-select">
                                        
                        <div id="cs_select_texture" class="cs-select cs-skin-circular" tabindex="0">
						<span class="cs-placeholder">Select A Texture</span>
						<div class="cs-options">
						<ul id="texture_images_ul">
							<li data-option="" data-price="<?php echo $redux_demo['step3_texture_price'][0]; ?>" style="background-image: url(<?php echo $redux_demo['step3_texture1']['url']; ?>);" data-value="1"><span>1</span></li>
							<li data-option="" data-price="<?php echo $redux_demo['step3_texture_price'][1]; ?>"style="background-image: url(<?php echo $redux_demo['step3_texture2']['url']; ?>);" data-value="2"><span>2</span></li>
							<li data-option="" data-price="<?php echo $redux_demo['step3_texture_price'][2]; ?>"style="background-image: url(<?php echo $redux_demo['step3_texture3']['url']; ?>);" data-value="3"><span>3</span></li>
							<li data-option="" data-price="<?php echo $redux_demo['step3_texture_price'][3]; ?>"style="background-image: url(<?php echo $redux_demo['step3_texture4']['url']; ?>);" data-value="4"><span>4</span></li>
							<li data-option="" data-price="<?php echo $redux_demo['step3_texture_price'][4]; ?>"style="background-image: url(<?php echo $redux_demo['step3_texture5']['url']; ?>);" data-value="5"><span>5</span></li>
							<li data-option="" data-price="<?php echo $redux_demo['step3_texture_price'][5]; ?>"style="background-image: url(<?php echo $redux_demo['step3_texture6']['url']; ?>);" data-value="6"><span>6</span></li>
							<li data-option="" data-price="<?php echo $redux_demo['step3_texture_price'][6]; ?>"style="background-image: url(<?php echo $redux_demo['step3_texture7']['url']; ?>);" data-value="7"><span>7</span></li>
							<li data-option="" data-price="<?php echo $redux_demo['step3_texture_price'][7]; ?>"style="background-image: url(<?php echo $redux_demo['step3_texture8']['url']; ?>);" data-value="8"><span>8</span>
							</li>
						</ul>
						</div>
						<select class="cs-select cs-skin-circular" data-required="1" data-alert="Wait! Select a texture first...">
					   
					  <option value="" disabled="" selected="">Select A Texture</option>
					  <option data-price="50.00" data-content="Body Wave" data-image="<?php echo $redux_demo['step3_texture1']['url']; ?>" data-variant-id="410022608916" value="1">1</option>

					  <option data-price="50.00" data-content="Straight" data-image="https://cdn.shopify.com/s/files/1/2299/3373/products/2.png?v=1510960026" data-variant-id="410022641684" value="2">2</option>

					  <option data-price="50.00" data-content="Curly" data-image="https://cdn.shopify.com/s/files/1/2299/3373/products/3.png?v=1510960026" data-variant-id="410022674452" value="3">3</option>

					  <option data-price="50.00" data-content="Wavy" data-image="https://cdn.shopify.com/s/files/1/2299/3373/products/4.png?v=1510960026" data-variant-id="410022707220" value="4">4</option>

					  <option data-price="50.00" data-content="Exotic Curl" data-image="https://cdn.shopify.com/s/files/1/2299/3373/products/5.png?v=1510960026" data-variant-id="410022739988" value="5">5</option>

					  <option data-price="50.00" data-content="Exotic Wave" data-image="https://cdn.shopify.com/s/files/1/2299/3373/products/6.png?v=1510960026" data-variant-id="410022772756" value="6">6</option>

					  <option data-price="50.00" data-content="Malaysian Body Wave" data-image="https://cdn.shopify.com/s/files/1/2299/3373/products/7.png?v=1510960026" data-variant-id="410022805524" value="7">7</option>

					  <option data-price="50.00" data-content="Malaysian Straight" data-image="https://cdn.shopify.com/s/files/1/2299/3373/products/8.png?v=1510960026" data-variant-id="410022838292" value="8">8</option>

                      </select>
				      </div>
					  </div>
                      <div class="circularPreview">Select A Texture</div>
                    </div>
                </section>
            </li>
		</ul>
	</div>
<!-- Cs Circular -->

</div>
</div>

</fieldset> 

<h2>Color</h2>
<fieldset>

<!-- STEP 4 -->                                           

<div class="frm-row ">						
<div class="section colm colm12">
<h2 >The Fun Part</h2>
</div>
	<?php echo $redux_demo['step4_c1']['url']; ?>
	<?php echo $redux_demo['step4_c2']['url']; ?>
	<?php echo $redux_demo['step4_color2']['url']; ?>
<div class="section colm colm12">

<!-- Color HTML -->
<div class="cs-selectbox boxes-select  wow zoomIn" style="visibility: visible; animation-name: zoomIn;">

<div id="color_boxes_step4" class="cs-select cs-skin-boxes cs-active" tabindex="0">
<div class="boxesPreview"><img src="https://cdn.shopify.com/s/files/1/2299/3373/products/ombre-10T24.png?v=1511009727" border="0"><span>Ombre 10t24</span></div>
<span id="color_boxes_trigger" class="cs-placeholder">Ombre 10t24</span>
<div class="cs-options">
<ul id="step4boxes_ul">	
	
	<?php 
		$redux_colorBg = $redux_demo['step4_color_main'];
		$redux_color_price = $redux_demo['step4_color_price'];
		
		for($n = 0; $n <= 25; $n++){
			$url_key = 'step4_color'.$n;
			$url_info = pathinfo($redux_demo[$url_key]['url']);
			$color_name = $url_info['filename'];
			?><li class="" data-price="<?php echo $redux_demo['step4_color_price'][$n] ?>" style="background-image: url(<?php echo $redux_demo[$url_key]['url']; ?>);" data-option="" data-value="<?php echo $n;?>"><span><?php echo $color_name; ?></span></li>
			<?php	
			
		} 
	?>
	
	<!--							
	<li class="swatch-1-jet-black" style="background-image: url(<?php// echo $redux_demo['step4_color1']['url']; ?>);" data-price="<?php// echo $redux_demo['step4_color_price'][0]; ?>" data-value="1 Jet Black"><span>1 Jet Black</span></li>
	<li class="swatch-1b-natural-black" data-option="" data-value="1b Natural Black"><span>1b Natural Black</span></li>
	<li class="swatch-2-darkest-brown" data-option="" data-value="2 Darkest Brown"><span>2 Darkest Brown</span></li>
	<li class="swatch-4-dark-brown" data-option="" data-value="4 Dark Brown"><span>4 Dark Brown</span></li>
	<li class="swatch-8-light-chestnut-brown" data-option="" data-value="8 Light Chestnut Brown"><span>8 Light Chestnut Brown</span></li>
	<li class="swatch-30-auburn" data-option="" data-value="30 Auburn"><span>30 Auburn</span></li>
	<li class="swatch-99j-plum" data-option="" data-value="99j Plum"><span>99j Plum</span></li>
	<li class="swatch-613-blonde" data-option="" data-value="613 Blonde"><span>613 Blonde</span></li>
	<li class="swatch-613-platinum-blonde" data-option="" data-value="613 Platinum Blonde"><span>613 Platinum Blonde</span></li>
	<li class="swatch-ash-blonde" data-option="" data-value="Ash Blonde"><span>Ash Blonde</span></li>
	<li class="swatch-blue-steel" data-option="" data-value="Blue Steel"><span>Blue Steel</span></li>
	<li class="swatch-burgundy-bistro" data-option="" data-value="Burgundy Bistro"><span>Burgundy Bistro</span></li>
	<li class="swatch-ruby-red" data-option="" data-value="Ruby Red"><span>Ruby Red</span></li>
	<li class="swatch-burgundy" data-option="" data-value="Burgundy"><span>Burgundy</span></li>
	<li class="swatch-copper" data-option="" data-value="Copper"><span>Copper</span></li>
	<li class="swatch-light-blonde" data-option="" data-value="Light Blonde"><span>Light Blonde</span></li>
	<li class="swatch-mahogany" data-option="" data-value="Mahogany"><span>Mahogany</span></li>
	<li class="swatch-mystic-turquoise" data-option="" data-value="Mystic Turquoise"><span>Mystic Turquoise</span></li>
	<li class="swatch-ombre-1b30" data-option="" data-value="Ombre 1b30"><span>Ombre 1b30</span></li>
	<li class="swatch-ombre-1bt10" data-option="" data-value="Ombre 1bt10"><span>Ombre 1bt10</span></li>
	<li class="swatch-ombre-2t27" data-option="" data-value="Ombre 2t27"><span>Ombre 2t27</span></li>
	<li class="swatch-ombre-4t30" data-option="" data-value="Ombre 4t30"><span>Ombre 4t30</span></li>
	<li class="swatch-ombre-6t27" data-option="" data-value="Ombre 6t27"><span>Ombre 6t27</span></li>
	<li class="swatch-ombre-10t24 cs-selected" data-option="" data-value="Ombre 10t24"><span>Ombre 10t24</span></li>
	<li class="swatch-ombre-18t22" data-option="" data-value="Ombre 18t22"><span>Ombre 18t22</span></li>
	<li class="swatch-pink-pearl" data-option="" data-value="Pink Pearl"><span>Pink Pearl</span></li>
	<li class="swatch-purple-rain" data-option="" data-value="Purple Rain"><span>Purple Rain</span></li>
	<li class="swatch-silver" data-option="" data-value="Silver"><span>Silver</span></li>
	<li class="swatch-silver-grey-black" data-option="" data-value="Silver Grey Black"><span>Silver Grey Black</span></li>
	<li class="swatch-ultra-violet" data-option="" data-value="Ultra Violet"><span>Ultra Violet</span></li> -->
</ul>
</div>

<select class="cs-select cs-skin-boxes" data-required="1" data-alert="Wait! Select a color first..." data-picked="0">

<option value="" disabled="" selected="">Pick your color</option>
 <option value="1 Jet Black" data-price="200.00" data-class="swatch-1-jet-black" data-image="https://cdn.shopify.com/s/files/1/2299/3373/products/1-jet-black.png?v=1511009486" data-thumb="https://cdn.shopify.com/s/files/1/2299/3373/products/1-jet-black.png?v=1511009486" data-variant-id="410023034900">1 Jet Black</option>

<option value="1b Natural Black" data-price="200.00" data-class="swatch-1b-natural-black" data-image="https://cdn.shopify.com/s/files/1/2299/3373/products/1b-natural-black.png?v=1511009486" data-thumb="https://cdn.shopify.com/s/files/1/2299/3373/products/1b-natural-black.png?v=1511009486" data-variant-id="410023100436">1b Natural Black</option>

<option value="2 Darkest Brown" data-price="200.00" data-class="swatch-2-darkest-brown" data-image="https://cdn.shopify.com/s/files/1/2299/3373/products/2-darkest-brown.png?v=1511009486" data-thumb="https://cdn.shopify.com/s/files/1/2299/3373/products/2-darkest-brown.png?v=1511009486" data-variant-id="410023133204">2 Darkest Brown</option>

<option value="4 Dark Brown" data-price="200.00" data-class="swatch-4-dark-brown" data-image="https://cdn.shopify.com/s/files/1/2299/3373/products/4-dark-brown.png?v=1511009486" data-thumb="https://cdn.shopify.com/s/files/1/2299/3373/products/4-dark-brown.png?v=1511009486" data-variant-id="410023165972">4 Dark Brown</option>

<option value="8 Light Chestnut Brown" data-price="200.00" data-class="swatch-8-light-chestnut-brown" data-image="https://cdn.shopify.com/s/files/1/2299/3373/products/8-light-chestnut-brown.png?v=1511009486" data-thumb="https://cdn.shopify.com/s/files/1/2299/3373/products/8-light-chestnut-brown.png?v=1511009486" data-variant-id="410023198740">8 Light Chestnut Brown</option>

<option value="30 Auburn" data-price="200.00" data-class="swatch-30-auburn" data-image="https://cdn.shopify.com/s/files/1/2299/3373/products/30-auburn.png?v=1511009486" data-thumb="https://cdn.shopify.com/s/files/1/2299/3373/products/30-auburn.png?v=1511009486" data-variant-id="410023231508">30 Auburn</option>

<option value="99j Plum" data-price="200.00" data-class="swatch-99j-plum" data-image="https://cdn.shopify.com/s/files/1/2299/3373/products/99J-plum.png?v=1511009486" data-thumb="https://cdn.shopify.com/s/files/1/2299/3373/products/99J-plum.png?v=1511009486" data-variant-id="410023264276">99j Plum</option>

<option value="613 Blonde" data-price="200.00" data-class="swatch-613-blonde" data-image="https://cdn.shopify.com/s/files/1/2299/3373/products/613-blonde.png?v=1511009486" data-thumb="https://cdn.shopify.com/s/files/1/2299/3373/products/613-blonde.png?v=1511009486" data-variant-id="410023297044">613 Blonde</option>

<option value="613 Platinum Blonde" data-price="200.00" data-class="swatch-613-platinum-blonde" data-image="https://cdn.shopify.com/s/files/1/2299/3373/products/613-platinum-blonde.png?v=1511009496" data-thumb="https://cdn.shopify.com/s/files/1/2299/3373/products/613-platinum-blonde.png?v=1511009496" data-variant-id="410023329812">613 Platinum Blonde</option>

<option value="Ash Blonde" data-price="200.00" data-class="swatch-ash-blonde" data-image="https://cdn.shopify.com/s/files/1/2299/3373/products/ash-blonde.png?v=1511009502" data-thumb="https://cdn.shopify.com/s/files/1/2299/3373/products/ash-blonde.png?v=1511009502" data-variant-id="410023362580">Ash Blonde</option>

<option value="Blue Steel" data-price="200.00" data-class="swatch-blue-steel" data-image="https://cdn.shopify.com/s/files/1/2299/3373/products/blue-steel.png?v=1511009508" data-thumb="https://cdn.shopify.com/s/files/1/2299/3373/products/blue-steel.png?v=1511009508" data-variant-id="410023395348">Blue Steel</option>

<option value="Burgundy Bistro" data-price="200.00" data-class="swatch-burgundy-bistro" data-image="https://cdn.shopify.com/s/files/1/2299/3373/products/burgundy-bistro.png?v=1511009515" data-thumb="https://cdn.shopify.com/s/files/1/2299/3373/products/burgundy-bistro.png?v=1511009515" data-variant-id="410023428116">Burgundy Bistro</option>

<option value="Ruby Red" data-price="200.00" data-class="swatch-ruby-red" data-image="https://cdn.shopify.com/s/files/1/2299/3373/products/ruby-red.png?v=1511009583" data-thumb="https://cdn.shopify.com/s/files/1/2299/3373/products/ruby-red.png?v=1511009583" data-variant-id="418989735956">Ruby Red</option>

<option value="Burgundy" data-price="200.00" data-class="swatch-burgundy" data-image="https://cdn.shopify.com/s/files/1/2299/3373/products/burgundy.png?v=1511009522" data-thumb="https://cdn.shopify.com/s/files/1/2299/3373/products/burgundy.png?v=1511009522" data-variant-id="410023460884">Burgundy</option>

<option value="Copper" data-price="200.00" data-class="swatch-copper" data-image="https://cdn.shopify.com/s/files/1/2299/3373/products/copper.png?v=1511009531" data-thumb="https://cdn.shopify.com/s/files/1/2299/3373/products/copper.png?v=1511009531" data-variant-id="410023493652">Copper</option>

<option value="Light Blonde" data-price="200.00" data-class="swatch-light-blonde" data-image="https://cdn.shopify.com/s/files/1/2299/3373/products/light-blonde.png?v=1511009557" data-thumb="https://cdn.shopify.com/s/files/1/2299/3373/products/light-blonde.png?v=1511009557" data-variant-id="410023526420">Light Blonde</option>

<option value="Mahogany" data-price="200.00" data-class="swatch-mahogany" data-image="https://cdn.shopify.com/s/files/1/2299/3373/products/mahogany.png?v=1511009568" data-thumb="https://cdn.shopify.com/s/files/1/2299/3373/products/mahogany.png?v=1511009568" data-variant-id="410023559188">Mahogany</option>

<option value="Mystic Turquoise" data-price="200.00" data-class="swatch-mystic-turquoise" data-image="https://cdn.shopify.com/s/files/1/2299/3373/products/mystic-turquoise.png?v=1511009606" data-thumb="https://cdn.shopify.com/s/files/1/2299/3373/products/mystic-turquoise.png?v=1511009606" data-variant-id="410023624724">Mystic Turquoise</option>

<option value="Ombre 1b30" data-price="200.00" data-class="swatch-ombre-1b30" data-image="https://cdn.shopify.com/s/files/1/2299/3373/products/ombre-1B30.png?v=1511009623" data-thumb="https://cdn.shopify.com/s/files/1/2299/3373/products/ombre-1B30.png?v=1511009623" data-variant-id="410023657492">Ombre 1b30</option>

<option value="Ombre 1bt10" data-price="200.00" data-class="swatch-ombre-1bt10" data-image="https://cdn.shopify.com/s/files/1/2299/3373/products/ombre-1BT10.png?v=1511009634" data-thumb="https://cdn.shopify.com/s/files/1/2299/3373/products/ombre-1BT10.png?v=1511009634" data-variant-id="410023690260">Ombre 1bt10</option>

<option value="Ombre 2t27" data-price="200.00" data-class="swatch-ombre-2t27" data-image="https://cdn.shopify.com/s/files/1/2299/3373/products/ombre-2T27.png?v=1511009690" data-thumb="https://cdn.shopify.com/s/files/1/2299/3373/products/ombre-2T27.png?v=1511009690" data-variant-id="410023723028">Ombre 2t27</option>

<option value="Ombre 4t30" data-price="200.00" data-class="swatch-ombre-4t30" data-image="https://cdn.shopify.com/s/files/1/2299/3373/products/ombre-4T30.png?v=1511009706" data-thumb="https://cdn.shopify.com/s/files/1/2299/3373/products/ombre-4T30.png?v=1511009706" data-variant-id="410023755796">Ombre 4t30</option>

<option value="Ombre 6t27" data-price="200.00" data-class="swatch-ombre-6t27" data-image="https://cdn.shopify.com/s/files/1/2299/3373/products/ombre-6T27.png?v=1511009718" data-thumb="https://cdn.shopify.com/s/files/1/2299/3373/products/ombre-6T27.png?v=1511009718" data-variant-id="410023788564">Ombre 6t27</option>

<option value="Ombre 10t24" data-price="200.00" data-class="swatch-ombre-10t24" data-image="https://cdn.shopify.com/s/files/1/2299/3373/products/ombre-10T24.png?v=1511009727" data-thumb="https://cdn.shopify.com/s/files/1/2299/3373/products/ombre-10T24.png?v=1511009727" data-variant-id="410023821332">Ombre 10t24</option>

<option value="Ombre 18t22" data-price="200.00" data-class="swatch-ombre-18t22" data-image="https://cdn.shopify.com/s/files/1/2299/3373/products/ombre-18T22.png?v=1511009741" data-thumb="https://cdn.shopify.com/s/files/1/2299/3373/products/ombre-18T22.png?v=1511009741" data-variant-id="410023854100">Ombre 18t22</option>

<option value="Pink Pearl" data-price="200.00" data-class="swatch-pink-pearl" data-image="https://cdn.shopify.com/s/files/1/2299/3373/products/pink-pearl.png?v=1511009753" data-thumb="https://cdn.shopify.com/s/files/1/2299/3373/products/pink-pearl.png?v=1511009753" data-variant-id="410023886868">Pink Pearl</option>

<option value="Purple Rain" data-price="200.00" data-class="swatch-purple-rain" data-image="https://cdn.shopify.com/s/files/1/2299/3373/products/purple-rain.png?v=1511009762" data-thumb="https://cdn.shopify.com/s/files/1/2299/3373/products/purple-rain.png?v=1511009762" data-variant-id="410023919636">Purple Rain</option>

<option value="Silver" data-price="200.00" data-class="swatch-silver" data-image="https://cdn.shopify.com/s/files/1/2299/3373/products/silver-fox.png?v=1511009864" data-thumb="https://cdn.shopify.com/s/files/1/2299/3373/products/silver-fox.png?v=1511009864" data-variant-id="410024083476">Silver</option>

<option value="Silver Grey Black" data-price="200.00" data-class="swatch-silver-grey-black" data-image="https://cdn.shopify.com/s/files/1/2299/3373/products/silver-grey-black.png?v=1511009873" data-thumb="https://cdn.shopify.com/s/files/1/2299/3373/products/silver-grey-black.png?v=1511009873" data-variant-id="410024116244">Silver Grey Black</option>

<option value="Ultra Violet" data-price="200.00" data-class="swatch-ultra-violet" data-image="https://cdn.shopify.com/s/files/1/2299/3373/products/ultra-violet.png?v=1511009819" data-thumb="https://cdn.shopify.com/s/files/1/2299/3373/products/ultra-violet.png?v=1511009819" data-variant-id="410024149012">Ultra Violet</option>


</select></div></div>


<!-- Color HTML -->

</div>
</div>

</fieldset> 

<h2>Review</h2>
<fieldset>

<!-- STEP 5 -->                                           

<div class="frm-row ">						
<div class="section colm colm12">
<h2 class="">Review Your Wig Order</p>
</div>

<div class="section colm colm12">

<!-- Review Order Html -->
<div class="summary-list">

<div class="row summery-box wow slideInUp" data-summary="model" style="visibility: visible; animation-name: slideInUp;">
<div class="heading-box"><img src="http://wig.logicsbuffer.com/wp-content/uploads/2018/02/wig-photo-title.jpg" border="0"></div>
<div class="content-box">
<div class="imgbox"><img id="wig_photosummry" src="uploads/1517897372.png" border="0" class="img-responsive"></div>
<div class="content">
<h1 class="title"><span class="yellow_text">Your Wig Photo</span></h1>
<p>Alonzo Arnold will create your wig<br>based off the photo you have provided.</p>
</div>
<a href="#model-page" class="summary-nav edit">Edit</a>
<div class="clearfix"></div>                          
</div>
</div>

<div class="row summery-box wow slideInUp" data-summary="length" style="visibility: visible; animation-name: slideInUp;">                            
<div class="content-box">
<div class="heading-box"><img src="http://wig.logicsbuffer.com/wp-content/uploads/2018/02/length-title.jpg" border="0" alt="Model Length Title"></div>
<div class="imgbox"><img src="http://wig.logicsbuffer.com/wp-content/uploads/2018/02/length-summery.png" alt="Model Length" border="0" class="img-responsive"></div>
<div class="content">
<h1 class="title"><span class="yellow_text">Length:</span> <span class="length-label">22" - $105</span> </h1>
<p>This is how long the hair for your wig will be.<br>*Wig total price may vary depending on desire length.</p>
</div>
<a href="#length-page" class="summary-nav edit">Edit</a>
<div class="clearfix"></div>                          
</div>
</div>

<div class="row summery-box wow slideInUp" data-summary="texture" style="visibility: visible;">
<div class="heading-box"><img src="http://wig.logicsbuffer.com/wp-content/uploads/2018/02/texture-title.jpg" alt="Model Texture Title" border="0"></div>
<div class="content-box">
<div class="imgbox"><img src="https://cdn.shopify.com/s/files/1/2299/3373/products/1.png?v=1510960026" border="0" class="img-responsive"></div>
<div class="content">
<h1 class="title"><span class="yellow_text">Texture: </span><span class="texture-label">Body Wave - $50</span> </h1>
<p>This is how long the hair for your wig will be.<br>*Wig total price may vary depending on desire length.</p>
</div>
<a href="#texture-page" class="summary-nav edit">Edit</a>
<div class="clearfix"></div>                          
</div>
</div>

<div class="row summery-box wow slideInUp" data-summary="model-color" style="visibility: visible;">
<div class="heading-box"><img src="http://wig.logicsbuffer.com/wp-content/uploads/2018/02/color-title.jpg" alt="Model Color Title" border="0"></div>
<div class="content-box">
<div class="imgbox"><img src="https://cdn.shopify.com/s/files/1/2299/3373/products/99J-plum.png?v=1511009486" alt="Model Color" border="0" class="img-responsive"></div>
<div class="content">
<h1 class="title"><span class="yellow_text">Color: </span><span class="boxes-label">99j Plum - $200</span> </h1>
<p>This color treatment will be applied to the hair of your wig.</p>
</div>
<a href="#color-page" class="summary-nav edit">Edit</a>
<div class="clearfix"></div>                          
</div>
</div>

</div>
<!-- Review Order Html End -->

</div>
</div>

</fieldset> 

<h2>Your Info</h2>
<fieldset>

<!-- STEP 6 -->                                           

<div class="frm-row ">						
<div class="section colm colm6">
<p class="step2_labels">IT’S ALL ABOUT TEXTURE!</p>
</div>

<div class="section colm colm6">
<label for="firstName" class="field prepend-icon">
<input type="text" name="firstName" placeholder="Your Full Name" class="gui-input required percentage" id="firstname" value=""  required> 
<b class="tooltip tip-right-top"><em> Hey buddy! iam a top left tooltip.</em></b>
<span class="field-icon"><i class="fa fa-user"></i></span>  					
</label>
</div>
</div>

</fieldset> 
</form>
	<div class="tot-price">
        Total : 
        <span class="yellow_text">$<b id="total_priceshow">0.00</b></span>
    </div>
	
	</div>
</div>
</div>

<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>

<!-- Jquery  Code -->

<?php  

$json_form_setVehicleInfo = json_encode($form_setVehicleInfo); 
$json_coverage_dataTpl = json_encode($coverage_dataTpl); 
$uniq_id = uniqid();
?>    

<script>
function show_price_image(){
		var image_price = jQuery("#upload_imgPrice").val();			
		var total_price;		
		total_price = parseFloat(image_price);						
		
		jQuery("#total_priceshow").html(total_price);
		jQuery("#total_price_image").val(total_price);
		
		var get_image64bit = jQuery(".dz-image img").attr('src');
		console.log(get_image64bit);
		//
		jQuery("#wig_photosummry").attr('src', get_image64bit);
		
}

function show_price_length(length_val){
		var length_val = length_val;
		console.log("length_val.."+length_val);
		var price_previous = jQuery("#total_price_image").val();			
		var total_price = parseFloat(price_previous) + parseFloat(length_val);				
		
		jQuery("#total_priceshow").html(total_price);
	    jQuery("#total_price_length").val(length_val);
		

}

function show_price_texture(texture_val){
		var texture_val = texture_val;
		console.log("texture_val.."+texture_val);
		var price_image = jQuery("#total_price_image").val();			
		var price_length = jQuery("#total_price_length").val();			
		var total_pricetexture = parseFloat(price_image) + parseFloat(price_length) + parseFloat(texture_val);				
		
		jQuery("#total_priceshow").html(total_pricetexture);
	    jQuery("#total_price_texture").val(texture_val);
	
}


jQuery(document).ready(function() {	

jQuery( "#step5" ).hide();
jQuery( "#bread_crums_custom" ).hide();
localStorage.setItem("policyType_final","Private");
jQuery("#vehicle_val").prop('disabled', true);
jQuery("#car_value_details").hide();
jQuery("#model_year").prop('disabled', true);
jQuery("#vehicle_model_detail").prop('disabled', true);

jQuery( "#goto_table" ).click(function() {
jQuery( "#step4" ).show();
jQuery( "#step5" ).hide();
jQuery( ".smart-wrap" ).hide();
jQuery( ".form_desc" ).hide();
});
jQuery( "#goto_form" ).click(function() {
jQuery( "#step4" ).hide();
jQuery( ".smart-wrap" ).show();
jQuery( ".form_desc" ).show();
});

//Step 3 Texture Toggle
jQuery("#cs_select_texture").removeClass("cs-active");

jQuery( ".cs-placeholder" ).click(function() {
	jQuery("#cs_select_texture").toggleClass("cs-active");
});

//Step 4 Color
jQuery("#color_boxes_step4").removeClass("cs-active");

jQuery( "#color_boxes_trigger" ).click(function() {
	jQuery("#color_boxes_step4").toggleClass("cs-active");
});

jQuery('#texture_images_ul li').click(function(e) {
    jQuery(this).addClass('texture-selected').siblings().removeClass('texture-selected');
	var texture_val = jQuery(this).attr('data-price');
	console.log("texture_val..."+texture_val);
	show_price_texture(texture_val);
	
});

jQuery('#step4boxes_ul li').click(function(e) {
    e.preventDefault();
    jQuery('#step4boxes_ul li').removeClass('selected');
    jQuery(this).addClass('cs-selected');
});

// Dropzone

 var myDropzone = new Dropzone("#myDropzone",{ 
	url: '/',
	autoProcessQueue: false,
	uploadMultiple: true,
	parallelUploads: 5,
	maxFiles: 5,
	maxFilesize: 5,
	dictDefaultMessage: '',
	acceptedFiles: ".jpeg,.jpg,.png,.gif",
	addRemoveLinks: true,
	dictRemoveFile: '&#215;'
});




//Model Condition
jQuery( "#popup_dangermain" ).hide();
jQuery( "#popup_infomain" ).hide();
var ideal_date_21    = "01/1/1996"

//Hide Popup Danger on Ok Click	
jQuery( "#danger_ok" ).click(function() {
jQuery( "#popup_dangermain" ).hide();
});

jQuery( "#dangerPolicy_ok" ).click(function() {
jQuery( "#popup_dangerPolicyStartDate" ).hide();
});

jQuery( "#info_ok" ).click(function() {
	jQuery( "#popup_infomain" ).hide();
});

jQuery( "#info_ok_brand_new" ).click(function() {
	jQuery( "#popup_infomain_brand_new" ).hide();
});

jQuery( "#ins_expired_danger_ok" ).click(function() {
	jQuery( "#popup_current_ins_expired" ).hide();
});

jQuery( "#veh_notcommercial_ok" ).click(function() {
	jQuery( "#vehicle_notfor_commercialpurpose" ).hide();
});

jQuery( "#info_outsideuae_ok" ).click(function() {
	jQuery( "#popup_outsideuae_cover" ).hide();
});

jQuery( "#pa_coverDriver_ok" ).click(function() {
	jQuery( "#popup_pa_coverDriver" ).hide();
});
	
	var ideal_date_21    = "01/1/1992"

	jQuery( "#datepicker1" ).change(function() {

		var age_less_25 = jQuery('#datepicker1').val();
		//console.log("ideal_date_21.."+ideal_date_21);
		//console.log("age_less_25.."+age_less_25);

		if(new Date(age_less_25) > new Date(ideal_date_21)){		
		//console.log("age is less then 21 ...");
		jQuery( "#popup_dangermain" ).show();
		}else{
		jQuery( "#popup_dangermain" ).hide();
		}
	});
	
		var getCanvas; // global variable
		//jQuery("#rt_react").on('click', function () {
		function capture_precheckout(){
			console.log("Image render check");	
			html2canvas(document.body, { type: 'view' }).then(function(canvas) {
			getCanvas = canvas;
		  
		  var imgageData = getCanvas.toDataURL("image/png");
		   
		  });
		//});
		}
	  
	//When would you like the Policy to start..
	jQuery( "#vec_policy_date1" ).change(function() {		
		checkdaysPolicy();
	});
	jQuery( "#vec_policy_date2" ).change(function() {		
		checkdaysPolicy();
	});	
	jQuery( "#vec_policy_date3" ).change(function() {		
		checkdaysPolicy();
	});
	
	function checkdaysPolicy(){
		var vec_policy_date1 = jQuery('#vec_policy_date1 option:selected').val();
		var vec_policy_date2 = jQuery('#vec_policy_date2 option:selected').val();
		var vec_policy_date3 = jQuery('#vec_policy_date3 option:selected').val();
		
		var selected_Policydate = '"'+vec_policy_date2+'/'+vec_policy_date1+'/'+vec_policy_date3+'"';
		console.log("selected_Policydate.."+selected_Policydate);
		
		///
		var numDaysBetween = function(d1, d2) {
		  var diff = Math.abs(d1.getTime() - d2.getTime());
		  return diff / (1000 * 60 * 60 * 24);
		};

		var d1 = new Date(selected_Policydate); // Jan 1, 2011
		var d2 = new Date(); // Jan 2, 2011
		var difference = numDaysBetween(d1, d2); // => 1
		console.log("d1.."+d1);
		console.log("d2.."+d2);
		console.log("difference.."+difference);
		if(difference > 60){
			console.log("you have selected more then 60 days.");
			jQuery( "#popup_dangerPolicyStartDate" ).show();
		}else{
			console.log("you have selected Within 60 days.");
		}
	}	
		

function refreshPrice(){	
}      

jQuery(function () {
	jQuery('#reg_date').datepicker({
	format: 'mm/dd/yyyy',
	startDate: '-3d'					
	});
});

jQuery(function () {       
	jQuery('#polstart_date').datepicker({
	format: 'mm/dd/yyyy',
	startDate: '-3d'
	});
});

jQuery(function () {
	jQuery('#dob').datepicker({
	format: 'mm/dd/yyyy',
	startDate: '-3d'
	});
}); 

jQuery(".percentage").change(function(){ 

var veh_fullname_filled = jQuery('#firstname').val();
jQuery('#user_filled').html(veh_fullname_filled);

get_companies();
jQuery(this).addClass("changed");
var change_count = jQuery('.changed').length;
var currentval = parseFloat(change_count);
currentval1 = currentval * 2.94;
currentval11 = currentval1.toFixed(1);
jQuery("#percentage-text").html(currentval11+'%');

var circlefill = 'p'+currentval1;
	jQuery( "#progress:last" ).addClass( circlefill );
});

	jQuery('[data-toggle="tooltip"]').tooltip();   
	jQuery("#btn_step1").addClass("active_step");

jQuery("#btn_step1").click(function(){
	jQuery("#btn_step2").removeClass("active_step");
	jQuery("#btn_step1").addClass("active_step");
});		

jQuery("#btn_step2").click(function(){
	jQuery("#btn_step1").removeClass("active_step");
	jQuery("#btn_step2").addClass("active_step");
});		

jQuery("#btn_nextstep2inner").click(function(){
	jQuery("#btn_step1").removeClass("active_step");
	jQuery("#btn_step2").removeClass("active_step");
});  

var basePrice = 3.00;
var width;

//jQuery( "#ins_submodel" ).hide();
	jQuery( "#step2" ).hide();
	jQuery( "#step3" ).hide();
	jQuery( "#step4" ).hide();
	jQuery("#calCheckout_url").hide();

jQuery( "#btn_step2" ).click(function() {
	jQuery( "#step1" ).hide();
	jQuery( "#step2" ).show();
}); 	

jQuery( "#btn_nextstep1inner" ).click(function() {
	jQuery( "#step1" ).hide();
	jQuery( "#step2" ).show();
	jQuery("#btn_step1").removeClass("active_step");
	jQuery("#btn_step2").addClass("active_step");
});  	

jQuery( "#btn_step1" ).click(function() {
	jQuery( "#step2" ).hide();
	jQuery( "#step1" ).show();
}); 

jQuery("#model_year").change(function () {
	jQuery("#vehicle_model_detail").prop('disabled', false);
	var model_year_switch = jQuery('#model_year option:selected').val();
/* 	if(model_year_switch == "2017" || model_year_switch == "2018"){			
		jQuery("#fr3").prop('checked', true);
		jQuery('#dor_row').addClass( "hide_this" );	
	}	 */			
	show_models();
}); 

jQuery("#vehicle_reg_date3").change(function () {
	var vehicle_firstReg_dateYear = jQuery('#vehicle_reg_date3 option:selected').val();
	//console.log("vehicle_firstReg_dateYear"+vehicle_firstReg_dateYear);
	/* if(vehicle_firstReg_dateYear == "2018"){
		jQuery("#fr3").prop('checked', true);
		jQuery('#dor_row').addClass( "hide_this" );
	} */
});

jQuery("#vehicle_brand").change(function () {
	jQuery("#model_year").prop('disabled', false);
	show_models();
}); 

function show_models(){

var step_1datam = <?php echo $json_form_setVehicleInfo; ?>;
var selected_modelYear = jQuery('#model_year option:selected').val();
var vehicle_brand_filter = jQuery('#vehicle_brand option:selected').val();
var final_array = [];
var final_array2 =[];


jQuery.each( step_1datam, function( m, brand_filter ) {								
if(brand_filter[0] == vehicle_brand_filter && brand_filter[1] == selected_modelYear){
final_array2.push(brand_filter[2]);
}
});						 

//Set to option Make
jQuery('.modeloption').html('');
jQuery.each( final_array2, function( i, veh_model_set ) {
jQuery('.modeloption').append('<option class="options" value="'+veh_model_set+'">'+veh_model_set+'</option>');
});	
//show_minmax();
}

//Disable Car Value field on Model Changed

	jQuery("#vehicle_model_detail").change(function () {		
		jQuery("#vehicle_val").prop('disabled', false);
		jQuery("#car_value_details").show();
	//show_minmax();	
	});


//Default value for Personal Accident Cover Driver
localStorage.setItem("pa_cover_driver_check", "No");
localStorage.setItem("outside_uae_cov_value", "No");
localStorage.setItem("rent_car_check_val", "No");

function get_companies(){
	
//Get selected values

var vehicle_brand = jQuery('#vehicle_brand option:selected').val();
jQuery('#cars_brand').html(vehicle_brand);

//Year Selection Logic
var selected_modelYear = jQuery('#model_year option:selected').val();	
var currentYear2 = (new Date).getFullYear();	

jQuery('.veh_brand_new').click(function () {
	var veh_brandnewcheck = jQuery(this).val();

if(veh_brandnewcheck == "Yes"){
	var selected_modelYear = currentYear2;
}

else if(veh_brandnewcheck == "No"){
	var selected_modelYear = jQuery('#model_year option:selected').val();		
}
});

	var vehicle_model_detail = jQuery('#vehicle_model_detail option:selected').val();
	jQuery('#cars_model').html(vehicle_model_detail);
	var selected_vehicle_repair = jQuery('#per_agency_repair option:selected').val();
	var selected_vehicle_modified = jQuery('#vehicle_modified').is(':checked'); 	
	var selected_outside_uae = jQuery('#outside_uae').is(':checked'); 
	var selected_roadside_assistance = jQuery('#roadside_assistance').is(':checked'); 
	var selected_ins_rentACar = jQuery('#ins_rentACar').is(':checked'); 			
	var need_per_acc_cover_driver = "No";	
	var acc_cover_passangers = "Yes";	
	var rent_a_car_val;	
	var outside_uae_cov_val;	
	
	//need_per_acc_cover_driver = localStorage.setItem("pa_cover_driver_check","No");
	
	jQuery('#pa_cover_driver_id input').on('change', function() {
		var need_per_acc_cover_driver = jQuery('input[name=per_accidentcover_chk]:checked').val();
		localStorage.setItem("pa_cover_driver_check", need_per_acc_cover_driver);
		console.log("need_per_acc_cover_driver.."+need_per_acc_cover_driver);
	

		need_per_acc_cover_driver = localStorage.getItem("pa_cover_driver_check");
		console.log("need_per_acc_cover_driver.."+need_per_acc_cover_driver);	
	if(need_per_acc_cover_driver == "Yes"){
		var need_per_acc_cover_driver = need_per_acc_cover_driver;		
		jQuery( "#popup_pa_coverDriver" ).show();
	}else{
		var need_per_acc_cover_driver = "No";	
	}	
	});
	
	jQuery('#pa_cover_passangers_id input').on('change', function() {
		var need_per_acc_cover_pass = jQuery('input[name=per_accidentcover_chk]:checked').val();
		localStorage.setItem("pa_cover_pass_check", need_per_acc_cover_pass);
		console.log("need_per_acc_cover_pass..Check.."+need_per_acc_cover_pass);
	});

	jQuery('#rent_a_car_id input').on('change', function() {
		var rent_a_car_val = jQuery('input[name=veh_replacement_chk]:checked').val();
		localStorage.setItem("rent_car_check_val", rent_a_car_val);
	});	

	rent_car_val_get = localStorage.getItem("rent_car_check_val");	
	if(rent_car_val_get == "Yes"){
	var rent_car_check = rent_car_val_get;
	}else{
	var rent_car_check = "No";	
	}	

	jQuery('#outside_uae_coverage_id input').on('change', function() {
	var outside_uae_cov_val = jQuery('input[name=outsideuaeCover_chk]:checked').val();
	localStorage.setItem("outside_uae_cov_value", outside_uae_cov_val);	   
	////console.log("outside_uae_cov_val.."+outside_uae_cov_val);
	});

	outside_uae_cov_val = localStorage.getItem("outside_uae_cov_value");	
	if(outside_uae_cov_val == "Yes"){
	var outside_uae_cov_inner = outside_uae_cov_val;
	}else{
	var outside_uae_cov_inner = "No";	
	}

	//Private or Commercial Check
	var policyType_inner = "No";
	
	jQuery('#veh_PolicyType_id input').on('change', function() {
	var policy_typeCheck_val = jQuery('input[name=ptype_check]:checked').val();
	
	if(policy_typeCheck_val == "Yes"){		
		policyType_inner = "Yes";
		localStorage.setItem("policyType_final","Private");
	}
	else if(policy_typeCheck_val == "No"){		
		policyType_inner = "No";
		localStorage.setItem("policyType_final","Commercial");	
	}
	});	
	var policyType_result = localStorage.getItem("policyType_final");
	console.log("policyType_result.."+policyType_result);
	
var sel_cylenders = jQuery('#veh_cylenders option:selected').val();

var pa_cover_passangers = jQuery('#pa_cover_pass option:selected').val();
localStorage.setItem("pa_cover_pass_check",pa_cover_passangers);

var ideal_date    = "01/1/1992"
var ideal_date_21    = "01/1/1996"
var age_less_25 = jQuery('#datepicker1').val();

//Select Options End
var selected_category_tpl;

//Step1 and selected Data arrays
var step_1data = <?php echo $json_form_setVehicleInfo; ?>;
var step_dataTpl = <?php echo $json_coverage_dataTpl; ?>;

jQuery.each( step_1data, function( j, step_1Values ) {
	//Compare Step1 data to get category
	if(vehicle_brand == step_1Values[0] && selected_modelYear == step_1Values[1] && vehicle_model_detail == step_1Values[2]){
		selected_category_tpl = step_1Values[3];
		localStorage.setItem("selected_cat",selected_category_tpl);
		console.log("selected_category_tpl.."+selected_category_tpl);
	}
});

var rt_min_val = 0; 
var all_companies = [];
jQuery.each( step_dataTpl, function( j, valuecomp ) {

var get_selectedYear_agRepair = jQuery('#model_year option:selected').val();
var cat_selected = localStorage.getItem("selected_cat");
var csv_cylenders = valuecomp[6];

//Check Selected Cylenders
if(sel_cylenders == csv_cylenders && cat_selected == valuecomp[4] && policyType_result == valuecomp[5]){
	var tpl_price1 = valuecomp[7];
	var tpl_price = tpl_price1.replace(/\,/g, "");
	console.log("tpl_price..."+tpl_price);

	var cal_rate_tpl_unparsed = tpl_price;	
	var cal_rate_tpl = parseFloat(cal_rate_tpl_unparsed);
	
	//Per Acc Cover Driver	
	cal_personal_accident_c_driver = valuecomp[9];
	
	if(need_per_acc_cover_driver == "Yes"){
	cal_rate_tpl = cal_rate_tpl + parseFloat(cal_personal_accident_c_driver);			
	}else{
	cal_rate_tpl = cal_rate_tpl;
	}

	console.log("pa_cover_passangers..."+pa_cover_passangers);	
	
	//Per Acc Cover Passengers		
	if(pa_cover_passangers >= 1){										

	if(pa_cover_passangers == 1){
		cal_rate_tpl = cal_rate_tpl + parseFloat(valuecomp[10]);
	}
	else if(pa_cover_passangers == 2){
		var passangers2 = parseFloat(valuecomp[10]) + parseFloat(valuecomp[10]);
		cal_rate_tpl = parseFloat(cal_rate_tpl) + passangers2;
	}
	else if(pa_cover_passangers == 3){
		var passangers2 = parseFloat(valuecomp[10]) + parseFloat(valuecomp[10]) + parseFloat(valuecomp[10]);
		cal_rate_tpl = parseFloat(cal_rate_tpl) + passangers2;
	}
	else if(pa_cover_passangers == 4){
		var passangers2 = parseFloat(valuecomp[10]) + parseFloat(valuecomp[10]) + parseFloat(valuecomp[10]) + parseFloat(valuecomp[10]);
		cal_rate_tpl = parseFloat(cal_rate_tpl) + passangers2;
	}
	else if(pa_cover_passangers == 5){
		var passangers2 = parseFloat(valuecomp[10]) + parseFloat(valuecomp[10]) + parseFloat(valuecomp[10]) + parseFloat(valuecomp[10]) + parseFloat(valuecomp[10]);
		cal_rate_tpl = parseFloat(cal_rate_tpl) + passangers2;
	}
	else if(pa_cover_passangers == 6){
		var passangers2 = parseFloat(valuecomp[10]) + parseFloat(valuecomp[10]) + parseFloat(valuecomp[10]) + parseFloat(valuecomp[10]) + parseFloat(valuecomp[10]) + parseFloat(valuecomp[10]);
		cal_rate_tpl = parseFloat(cal_rate_tpl) + passangers2;
	}
	else if(pa_cover_passangers == 7){
		var passangers2 = parseFloat(valuecomp[10]) + parseFloat(valuecomp[10]) + parseFloat(valuecomp[10]) + parseFloat(valuecomp[10]) + parseFloat(valuecomp[10]) + parseFloat(valuecomp[10]) + parseFloat(valuecomp[10]);
		cal_rate_tpl = parseFloat(cal_rate_tpl) + passangers2;
	}
	else{
		cal_rate_tpl = cal_rate_tpl;
	}									
	} 
	
	//console.log("price_ Before disc.."+cal_rate_tpl);	

	// Apply Discounts	
 	var discount_apply = valuecomp[2].replace(/\%/g, "");
	var discount_apply2 = discount_apply / 100;
	var discount_amount = parseFloat(discount_apply2) * cal_rate_tpl;
	//console.log("discount_apply.."+discount_apply);
	//console.log("discount_apply2.."+discount_apply2);
	//console.log("discount_amount.."+discount_amount);
	var price_afterdeductB = cal_rate_tpl - parseFloat(discount_amount); 
	console.log("price_Before DeductB.."+price_afterdeductB);
	
	var apply_vatAmmount = valuecomp[8].replace(/\%/g, "");
	var percentage_toadd = (parseFloat(price_afterdeductB) / 100) * parseFloat(apply_vatAmmount);
	var price_afterdeductB = price_afterdeductB + parseFloat(percentage_toadd); 
	var price_afterdeduct = price_afterdeductB.toFixed(2); 
	
	console.log("price_after Deduct.."+price_afterdeduct);
	
	
all_companies.push({
compname: valuecomp[0],
tpl_class: valuecomp[1],
tpl_discounts: valuecomp[2],
exc_vehList: valuecomp[3],
tpl_vehCat: valuecomp[4],
tpl_policy: valuecomp[5],
tpl_cylender_cap: valuecomp[6],
tpl_price: cal_rate_tpl,
tpl_vat: valuecomp[8],
tpl_pac_driver: valuecomp[9],
tpl_pac_passanger: valuecomp[10],
tpl_tird_partyLimit: valuecomp[11],
tpl_tird_partydeath: valuecomp[12],
tpl_ambulance_cover: valuecomp[13],
tpl_road_assistance: valuecomp[14],
price_afterdisc1: price_afterdeduct,
price_beforedisc: cal_rate_tpl,

cart_btn: '<a value="'+price_afterdeduct+'" data1="'+price_afterdeduct+'#'+valuecomp[0]+'#'+valuecomp[1]+'#'+valuecomp[2]+'#'+valuecomp[3]+'#'+valuecomp[4]+'#'+valuecomp[5]+'#'+valuecomp[6]+'#'+valuecomp[7]+'#'+valuecomp[8]+'#'+valuecomp[9]+'#'+valuecomp[10]+'#'+valuecomp[11]+'#'+valuecomp[12]+'#'+valuecomp[13]+'#'+valuecomp[14]+'#'+valuecomp[15]+'" class="pricing-tables-widget-purchase-button add-tocartbtn button_1" href="#">Buy Now</a>'
});
}  
}); 

return all_companies;
}

jQuery(".percentage").change(function() {
jQuery('#priceingtable_tpl').html('');
all_companies = get_companies();

console.log(all_companies);

if(all_companies){

	var pa_cover_driver_check = localStorage.getItem("pa_cover_driver_check");
	var pa_cover_driver_show;
	var pa_cover_passangers_selected = localStorage.getItem("pa_cover_pass_check");
	var companies_count = all_companies.length;

	localStorage.setItem("comp_count",companies_count);
	jQuery('#cars_count').html(companies_count);
	//console.log("companies_count..."+companies_count);
	jQuery.each( all_companies, function(g, companies1) { 
	var pa_cover_pass_amount;
	//Checking PA Cover for Passangers 
	if(pa_cover_passangers_selected == 1){
	pa_cover_pass_amount = 30;
	}if(pa_cover_passangers_selected == 2){
	pa_cover_pass_amount = 60;
	}if(pa_cover_passangers_selected == 3){
	pa_cover_pass_amount = 90;
	}if(pa_cover_passangers_selected == 4){
	pa_cover_pass_amount = 120;
	}if(pa_cover_passangers_selected == 5){
	pa_cover_pass_amount = 150;
	}if(pa_cover_passangers_selected == 6){
	pa_cover_pass_amount = 180;
	}if(pa_cover_passangers_selected == 7){
	pa_cover_pass_amount = 210;
	}else if(pa_cover_passangers_selected == 0){
	pa_cover_pass_amount = "No";
	}	

	//Checking PA Cover for Driver
	var pa_cover_driver_show;
	var need_per_acc_cover_driver = localStorage.getItem("pa_cover_driver_check");	
	if(need_per_acc_cover_driver == "Yes"){
		pa_cover_driver_show = companies1.tpl_pac_driver;
	}else{
		pa_cover_driver_show = "No";
	}
	
	//Checking PA Cover for Passangers
	var pa_cover_passangers_show;
	var need_per_acc_cover_passangers = localStorage.getItem("pa_cover_pass_check");	
	if(need_per_acc_cover_passangers >= 1){
		pa_cover_passangers_show = pa_cover_pass_amount;
	}else{
		pa_cover_passangers_show = "No";
	} 
	
	console.log("pa_cover_passangers_show.."+pa_cover_passangers_show);
	
jQuery('#priceingtable_tpl').append('<div class="pricing-widget-wrapper mainrow"><div class="pricing-tables-widget pricing-tables-widget-default pricing-tables-widget-blue pricing-tables-widget-dark"><div class="grid-container"><div class="grid-row"><div class="grid-item item-lg-12 item-sm-6 item-xs-marginb30"><div class="grid-row grid-row-collapse"><div class="grid-item item-lg-3 item-xs-12 leftbar"><div class="pricing-tables-widget-header tablerighthead"><div id="companylogo"><p  id="companylogoinner"><span  class="'+companies1.compname.toString().toLowerCase()+'"></span></p></div><div class="pricing-tables-widget-purchase">'+companies1.compname+'</div><div id="compclass"><div class="pricing-tables-widget-purchase compclassname">'+companies1.tpl_class+'</div></div></div></div><div class="grid-item item-lg-6 item-xs-12"><div class="pricing-tables-widget-section"><div class="grid-row"><div class="grid-item item-lg-6 item-xs-12"><ul class="pricing-tables-widget-divider-res-section"><li><div class="pricing-tables-widget-tooltip pricing-tables-widget-top-left pricing-tables-widget-fixed" data-tooltip="Our Infrastructure is prepared for Instant Activation Services For a Better User Experience"></div><i class="icomoon-circle2 iconcircle"></i>Third Party Limit <span class="dataval"> '+companies1.tpl_tird_partyLimit+'</span></li><li id="chkeckthis11"><div class="pricing-tables-widget-tooltip pricing-tables-widget-top-left pricing-tables-widget-fixed" data-tooltip="With our Cloud Network you have Peace of Mind and can look at your business only"></div><i class="icomoon-circle2 iconcircle"></i>Ambulance Cover <span class="dataval"><i class="icomoon-checkmark-circle '+companies1.tpl_ambulance_cover.toString().toLowerCase()+'"></i></span></li><li><div class="pricing-tables-widget-tooltip pricing-tables-widget-top-left pricing-tables-widget-fixed" data-tooltip="One piece of Software amazing and Beautiful organized with user in Mind"></div><i class="icomoon-circle2 iconcircle"></i>Personal Accident Cover For Driver <span class="dataval"> '+pa_cover_driver_show+'</span></li><li></li></ul></div><div class="grid-item item-lg-6 item-xs-12"><ul><li><div class="pricing-tables-widget-tooltip pricing-tables-widget-top-left pricing-tables-widget-fixed" data-tooltip="Our Infrastructure is prepared for give good space in Services For a Better User Experience"></div><i class="icomoon-circle2 iconcircle"></i>Third Party Death / Body Injury <span class="dataval">'+companies1.tpl_tird_partydeath+'</span></li><li><div class="pricing-tables-widget-tooltip pricing-tables-widget-top-left pricing-tables-widget-fixed" data-tooltip="With our Cloud Network you have Peace of Mind and can look at your business only with this Bandwidth"></div><i class="icomoon-circle2 iconcircle"></i><span class="switchlabel">Road-Side Assistance <span class="dataval"><i class="icomoon-checkmark-circle '+companies1.tpl_road_assistance.toString().toLowerCase()+'"></i></span></li><li><div class="pricing-tables-widget-tooltip pricing-tables-widget-top-left pricing-tables-widget-fixed" data-tooltip="In this Package you have 100 Emails per Month"></div><i class="icomoon-circle2 iconcircle"></i>Personal Accident Cover For Passangers <span class="dataval">'+pa_cover_passangers_show+'</span></li><li></li></ul></div></div> </div></div><div class="grid-item item-lg-3 item-xs-12"><div class="pricing-tables-widget-footer ptablefooter"><p class="pricing-tables-widget-top-left rightdiscount">Save '+companies1.tpl_discounts+'</p><h3 class="item-xs-marginb0"><strike>'+companies1.tpl_price+'</strike></h3><p>Total Price Are</p><h1 class="item-xs-marginb0"><span class="dataval1">'+companies1.price_afterdisc1+'</span> AED</h1><div class="pricing-tables-widget-price">'+companies1.price_afterdisc1+' AED</div>'+companies1.cart_btn+'</div></div></div></div></div><div id="demo'+g+'" class="" style="display:none;"></div></div></div></div>');
});
}
}); 
});

</script>	
<!-- Price Table Excludes
<p class="pricing-tables-widget-purchase seemoredetails">see more details<i class="icomoon-menu-open2 seemoredetailsicon"></i></p>
<span class="pricing-tables-widget-bold">100GB</span>
<span class="pricing-tables-widget-badges">New</span> -->
<script>	

	//jQuery(document).ready(function() {
	jQuery(document).on('click', '.seemoretoggle', function () {
	var content_toggle= jQuery(this).attr('href');                    
	jQuery(content_toggle).toggle();
	});

	//Call to Capture PDF Function.
	//Canvas code
	
/* 	function capture_precheckout(){		   
		   //var uniqid = '<?php echo uniqid(); ?>';
			//jQuery("#btnSave").click(function() { 
		   //Canvas to show.
		   var imgageData;
		   console.log("Image render check");	
		   html2canvas(jQuery("#pre_chkout_policy"), {
				onrendered: function(canvas) {
					var imgageData = canvas.toDataURL("image/png");
					//jQuery("#img-out").append('<img src="'+imgageData+'">');
					//console.log(imgageData);
					localStorage.setItem("precheckout_image",imgageData);
				}
			});
			
		var pre_checkimage_code = localStorage.getItem("precheckout_image");
		//var pre_checkimage_code = imgageData;
		console.log("pre_checkimage_code..",pre_checkimage_code);	
		var data = {	
		'action': 			'my_ajax_rt_precheckout',
		'ins_pdf_image' : 	pre_checkimage_code };
		
		jQuery("#img-out").append('<img src="'+pre_checkimage_code+'">');
		var ajax_url_custom = "http://insurancetopup.com/wp-admin/admin-ajax.php";

		jQuery.post(ajax_url_custom, data, function(response) {
			console.log(response);					
		});				
	} */	

	
	
	jQuery(document).on('click', '.add-tocartbtn', function () {
	
	//capture_precheckout();
	//capture_precheckout();
	//setTimeout(capture_precheckout, 1000);
	
	//Set form name TPL
	localStorage.setItem("form_type",'tpl');
	
	// your function here
	var alldataclicked = jQuery(this).attr('data1');
	console.log("alldatacheck.."+alldataclicked);

	var dataSelected = alldataclicked.split('#');
	var price_final = dataSelected[0];

	//Getting Selected Company and let it go 

	var selected_compny_data = dataSelected;
	var price = 	dataSelected[0];
	var compname =  dataSelected[1];
	var gcc_spec =  dataSelected[2];
	var vehicle_cat = dataSelected[3];
	//var repair_cond = dataSelected[12];
	var minimum_val = dataSelected[5];
	var maximum_val = dataSelected[6];
	var rate = dataSelected[7];
	var minimum = dataSelected[8];
	var dl_less_than_year = dataSelected[9];
	var less_than_year = dataSelected[10];
	var os_coverage = dataSelected[11];
	var pa_cover_driver = dataSelected[13];
	var pa_cover_passanger = dataSelected[14];
	var rs_assistance = dataSelected[14];
	var rent_car = dataSelected[16];
	var excess_amount = dataSelected[16];	
	var third_prty_limit = dataSelected[21];	
	var em_med_expence2 = dataSelected[25];	
	var em_loss_of_per_belongings = dataSelected[28];	

	//User Details			
	var veh_email_address = jQuery('#email_address').val();	
	var veh_vehicle_brand = jQuery('#vehicle_brand').val();	
	var veh_model_year = jQuery('#model_year').val();	
	var veh_fullname = jQuery('#firstname').val();		
	var veh_dobirth = jQuery('#datepicker1').val();		
	var veh_nationality = jQuery('#nationality option:selected').val();
	var veh_country_first_driving_lic = jQuery('#country_firstdl').val();	
	var veh_brandnew = jQuery('#fr3').val();	
	var veh_manyear1 = jQuery('#dor_date').val();	
	var veh_manyear2 = jQuery('#dor_month').val();	
	var veh_manyear3 = jQuery('#dor_year').val();	
	var veh_manufactured_year = veh_manyear1+'/'+veh_manyear2+'/'+veh_manyear3;
	var veh_contact_num1 = jQuery('#contact_num1').val();	
	var veh_contact_num2 = jQuery('#contact_num2').val();	
	var veh_contact_numF = veh_contact_num1+veh_contact_num2;
	var veh_model_detail = jQuery('#vehicle_model_detail').val();
	var veh_reg_date1 = jQuery('#vehicle_reg_date1').val();
	var veh_reg_date2 = jQuery('#vehicle_reg_date2').val();
	var veh_reg_date3 = jQuery('#vehicle_reg_date3').val();
	var veh_reg_dateF = veh_reg_date1+'/'+veh_reg_date2+'/'+veh_reg_date3;
	var veh_city_ofreg = jQuery('#cityof_reg').val();
	var veh_car_val = price;
	var veh_policystart_date1 = jQuery('#vec_policy_date1').val();
	var veh_policystart_date2 = jQuery('#vec_policy_date2').val();
	var veh_policystart_date3 = jQuery('#vec_policy_date3').val();
	var veh_policy_sdateF = veh_policystart_date1+'/'+veh_policystart_date2+'/'+veh_policystart_date3;
	var veh_country_first_dl = jQuery('#country_firstdl').val();
	var tpl_third_party_death = dataSelected[13];
	var tpl_ambulance_cover = dataSelected[14];
	var tpl_roadSide_assistance = dataSelected[15];
	console.log("tpl_third_party_death.."+tpl_third_party_death);
	console.log("tpl_ambulance_cover"+tpl_ambulance_cover);
	console.log("tpl_roadSide_assistance"+tpl_roadSide_assistance);
	
	var years_uaedriving_lic = jQuery('#years_uaedrivinglicence option:selected').val();
	var years_exp_dl_home = jQuery('#home_country_drivingexp option:selected').val();
	var total_Drivingexperience = years_uaedriving_lic;
	var tpl = 'tpl';
	
	//Set data to stap 5

	jQuery('#comp_name').html(compname);
	jQuery('#ins_value').html(veh_car_val);
	jQuery('#sel_vehicleModel').html(veh_model_year);
	jQuery('#sel_vehicleBrand').html(veh_vehicle_brand);
	jQuery('#sel_vehicleModel_detail').html(veh_model_detail);
	jQuery('#driv_name').html(veh_fullname);
	jQuery('#driv_nationality').html(veh_nationality);
	jQuery('#driv_drivingExp').html(total_Drivingexperience);
	jQuery('#driv_FirstLicCountry').html(veh_country_first_driving_lic);
	jQuery('#driv_DOB').html(veh_dobirth);
	jQuery('#driv_email').html(veh_email_address);
	jQuery('#car_tpl').html(third_prty_limit);
	jQuery('#car_ExcessAmount').html(excess_amount);
	jQuery('#car_EmergencyMedical').html(em_med_expence2);
	jQuery('#rent_aCar_chkout').html(rent_car);
	jQuery('#loss_per_belong_chkout').html(em_loss_of_per_belongings);
	jQuery('#pa_cover_passangers').html(pa_cover_passanger);
	jQuery('#outside_uae_cover').html(os_coverage);
	jQuery('#ins_price').html(price);
	jQuery('#ins_price1').html(price);
	compname2 = compname.toLowerCase();
	localStorage.setItem("compname2",compname2);
	//jQuery('#company_logoname').addClass(compname2);
	jQuery('#policy_startFrom').html(veh_policy_sdateF);
	jQuery('#policy_end1').html(veh_policystart_date1);
	jQuery('#policy_end2').html(veh_policystart_date2);
	jQuery('#tpl_third_party_death_show').html(tpl_third_party_death);
	jQuery('#tpl_third_party_limit').html(third_prty_limit);
	
	var next_year = parseInt(veh_policystart_date3) + 1;
	jQuery('#policy_end3').html(next_year);
	var tpl_ambulance_coverLow = tpl_ambulance_cover.toLowerCase();
	jQuery('#tpl_amb_cover').addClass(tpl_ambulance_coverLow);
	var tpl_roadSide_assistanceLow = tpl_roadSide_assistance.toLowerCase();
	jQuery('#tpl_road_assistance').addClass(tpl_roadSide_assistanceLow);


	//jQuery('#ins_discount').html(discount_applied);
		
	//Apply Vat on Price	
	var disc_price = parseFloat(price) * 0.05;
	var vat_added_amount = parseFloat(price) + disc_price;
	console.log("vat_added_amount.."+vat_added_amount); 	
	
	localStorage.setItem("final_price_cart1",vat_added_amount);
	jQuery('#ins_total_after_vat').html(vat_added_amount);

	what_included();
		
var data = {	

'action': 			'my_ajax_rt',
'ins_price' : 	price,
'ins_compname' :  compname,
'ins_discount' :  gcc_spec,
'ins_vehicle_cat' : vehicle_cat,
//'ins_repair_cond' : repair_cond,
'ins_minimum_val' : minimum_val,
'ins_maximum_val' : maximum_val,
'ins_rate' : rate,
'ins_minimum' : minimum,
'ins_dl_less_than_year' : dl_less_than_year,
'ins_less_than_year' : less_than_year,
'ins_os_coverage' : os_coverage,
'ins_pa_cover_driver' : pa_cover_driver,
'ins_cover_passanger' : pa_cover_passanger,
'ins_rent_car' : rent_car,
'ins_excess_amount' : excess_amount,
'ins_third_prty_limit' : third_prty_limit,
'ins_emergency_med_exp' : em_med_expence2,
'ins_loss_of_per_belongings' : em_loss_of_per_belongings,
'tpl_ambulanceCover' : tpl_ambulance_cover,
'tpl_roadSide_assistance' : tpl_roadSide_assistance,
'tpl_third_party_death' : tpl_third_party_death,

'ins_total_drivexp' : total_Drivingexperience,									
'ins_fullname' : veh_fullname,									
'ins_vehicle_brand' : veh_vehicle_brand,									
'ins_veh_model_year' : veh_model_year,									
'ins_contact_num' : veh_contact_numF,									
'ins_veh_man_year' : veh_manufactured_year,									
'ins_emailaddress' : veh_email_address,									
'ins_nationality_sel' : veh_nationality,									
'ins_brand_new' : veh_brandnew,									
'ins_model_detail' : veh_model_detail,									
'ins_vehiclereg_date' : veh_reg_dateF,									
'ins_cityof_reg' : veh_city_ofreg,									
'ins_car_value' : veh_car_val,									
'ins_policy_startdate' : veh_policy_sdateF,								
'ins_country_first_dl' : veh_country_first_dl,								
'ins_cntry_fir_driv_lic' : veh_country_first_driving_lic,									
'ins_dob_date' : veh_dobirth,									
'ins_selected_compny_data' : selected_compny_data,									
'form_type' : tpl									

};

	var ajax_url_custom = "http://insurancetopup.com/wp-admin/admin-ajax.php";

	jQuery.post(ajax_url_custom, data,   function(response) {
		console.log(response);					
	});

	

    
	
showEstimatedFareHtml(price_final);
document.getElementById("stern_taxi_fare_estimated_fare").value =  price_final;
localStorage.setItem("finalPrice", price_final);

//Show pre checkot
jQuery( "#step5" ).show();
jQuery( "#bread_crums_custom" ).show();
jQuery( "#step4" ).hide();
jQuery( ".smart-wrap" ).hide();
jQuery( ".form_desc" ).hide();
});
		 // global variable
		//jQuery("#rt_react").on('click', function () {
		 var imgageData;
		 function capture_precheckout(){
			var getCanvas;
			console.log("Image render check");	
			/* html2canvas(document.querySelector("#pre_chkout_policy")).then(function(canvas) {
				getCanvas = canvas;
			  
				var imgageData = getCanvas.toDataURL("image/png");
				// jQuery("#img-out").append('<img src="'+imgageData+'">');
				localStorage.setItem("precheckout_image",imgageData); 
			}); */
			//});
			//var imgageData1 = localStorage.getItem("precheckout_image");
			
			var html_email = jQuery( "#pre_chkout_policy" ).html();
			
			var data = {	
			'action': 			'my_ajax_rt_precheckout',
			'ins_pdf_image' : 	html_email };
			
			//jQuery("#img-out1").append(html_email);
			var ajax_url_custom = "http://insurancetopup.com/wp-admin/admin-ajax.php";

			jQuery.post(ajax_url_custom, data, function(response) {
				console.log(response);					
			});
							
		}
jQuery(document).on('click', '#proceed_checkout', function () {
	capture_precheckout();
	checkout_url_function();
	//setTimeout(capture_precheckout, 1000);
	//

});
	
//checkout_url_function(); 
//});
</script>			

<!-- Old Calculater -->

<form  id="stern_taxi_fare_div" method="post">
<div  style="@display:none;">
<input type="hidden"  name="stern_taxi_fare_estimated_fare" id="stern_taxi_fare_estimated_fare" value=""/><div class="row">
</div>								
</div>											

</form>
</div>

</div>

<script type="text/javascript" src="http://doptiq.com/smart-forms/demos/samples/elegant/js/jquery.formShowHide.min.js"></script> 
<script type="text/javascript">
jQuery(document).ready(function(jQuery){

/* jQuery(function(){ 
jQuery('.smartfm-ctrl').formShowHide(); 
}); */

/* @normal masking rules */

jQuery.mask.definitions['f'] = "[A-Fa-f0-9]"; 	

jQuery("#contact_num2").mask('9999999', {placeholder:'X'});
//jQuery("#datepicker1").mask('99-99-9999', {placeholder:'_'});			
//jQuery("#firstname").mask('Mustafa Jamal', {placeholder:'X'});			
jQuery('#firstname').bind('keyup blur',function(){ 
    var node = jQuery(this);
    node.val(node.val().replace(/^[0-9]*$/,'') ); 
	}); 
//jQuery("#firstname").mask('aaaaaa-aaaaaa', {placeholder:'X'});

//Showpopups on Select.
jQuery('#rent_a_car_id input').on('change', function() {
	var rent_a_car_val = jQuery('input[name=veh_replacement_chk]:checked').val();
	if(rent_a_car_val == 'Yes' ){
	jQuery( "#popup_infomain" ).show();
	//alert(" You Have choosed rental car option. So... 200 AED will be added ");		   
	////console.log("Rent Car Yes");
	}
});

//Show Brand New Popup
jQuery('#veh_brand_new_id input').on('change', function() {
	var brand_newchk = jQuery('input[name=new_check]:checked').val();
	if(brand_newchk == 'Yes' ){
		console.log("Brand New Yes");
		jQuery( "#popup_infomain_brand_new" ).show();
	}
});

//Current Insurence Not Expired alert
jQuery('#my_current_ins_not_exp input').on('change', function() {
	var current_ins_notexp_val = jQuery('input[name=mycurrentins_chk]:checked').val();
	if(current_ins_notexp_val == 'No' ){		
		jQuery( "#popup_current_ins_expired" ).show();
	}
});	

//My Vehicle is not for Commercial Purpose
jQuery('#veh_not_forcommercial input').on('change', function() {
	var veh_not_forcommercial = jQuery('input[name=vehiclecommercial_chk]:checked').val();
	if(veh_not_forcommercial == 'No' ){		
		jQuery( "#vehicle_notfor_commercialpurpose" ).show();
	}
});	

//Outside UAE Coverage
jQuery('#outside_uae_coverage_id input').on('change', function() {
	var outside_uae_coverage = jQuery('input[name=outsideuaeCover_chk]:checked').val();
	if(outside_uae_coverage == 'Yes' ){		
		jQuery( "#popup_outsideuae_cover" ).show();
	}
});	
		  	

jQuery('#pa_cover_driver_chekcbox0 input').on('change', function() {
var pa_cover_driver_chekcbox_val = jQuery('input[name=pac_driver0]:checked').val();
//console.log("pa_cover_driver_chekcbox_val.."+pa_cover_driver_chekcbox_val);
});
});

</script>

<style>


/* mPopup box style */

.mpopup {
display: none;
position: fixed;
z-index: 1;
padding-top: 100px;
left: 0;
top: 0;
width: 100%;
height: 100%;
overflow: auto;
background-color: rgb(0,0,0);
background-color: rgba(0,0,0,0.4);
}

.mpopup-content {
position: relative;
background-color: #fefefe;
margin: auto;
padding: 0;
width: 60%;
box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2),0 6px 20px 0 rgba(0,0,0,0.19);
-webkit-animation-name: animatetop;
-webkit-animation-duration: 0.4s;
animation-name: animatetop;
animation-duration: 0.4s
}

.mpopup-head {
padding: 2px 16px;
background-color: #ff0000;
color: white;
}

.mpopup-main {padding: 2px 16px;}
.mpopup-foot {
padding: 2px 16px;
background-color: #ff0000;
color: #ffffff;
}

/* add animation effects */
@-webkit-keyframes animatetop {
from {top:-300px; opacity:0}
to {top:0; opacity:1}
}

@keyframes animatetop {
from {top:-300px; opacity:0}
to {top:0; opacity:1}
}

/* close button style */
.close {
color: white;
float: right;
font-size: 28px;
font-weight: bold;
}
.close:hover, .close:focus {
color: #000;
text-decoration: none;
cursor: pointer;
}
</style>	

<!-- Model Start -->

<div id="popup_dangermain" style="display:none;">
<input class="bunta-switcher" id="bunta-danger" type="checkbox">
<div id="mpopupBox" class="bunta" data-bunta-style="dos" data-bunta-theme="danger">
<label class="bunta-close" for="bunta-danger">&times;</label>
<label class="bunta-overlay" for="bunta-danger" data-bunta-opacity="40"></label>
<div class="bunta-container" id="banta_cont">
<div class="bunta-center">
<h3 class="bunta-heading-icon"><i class="fa fa-id-card"></i></h3>
<h1 class="bunta-heading text1">Oops!</h1>
<h2 class="bunta-heading text1">Your Age is Less then 25 Years</h2>
<p class="text1 text2">Dear Valued Customer, Your age is less then 25 years, so please call us and talk to one of our Agents.
We'll be able to pull up your quote and help you get the coverage that's right for you.<br>
It doesn’t matter if you are 18 or 80, there are always ways for you to save on auto insurance.</p>
<h3 class="bunta-heading text1"><i class="fa fa-phone"></i> Call Now @ +971 40 123456</h3>
<label class="bunta-button bunta-circle" id="danger_ok" for="bunta-danger" data-bunta-theme="white"> - OK - </label>
</div>
</div>
</div>						
</div>

<div id="popup_dangerPolicyStartDate" style="display:none;">
<input class="bunta-switcher" id="bunta-danger" type="checkbox">
<div id="mpopupBox" class="bunta" data-bunta-style="dos" data-bunta-theme="danger">
<label class="bunta-close" for="bunta-danger">&times;</label>
<label class="bunta-overlay" for="bunta-danger" data-bunta-opacity="40"></label>
<div class="bunta-container" id="banta_cont">
<div class="bunta-center">
<h3 class="bunta-heading-icon"><i class="fa fa-id-card"></i></h3>
<h1 class="bunta-heading text1">Oops!</h1>
<h2 class="bunta-heading text1">You have selected Policy start date more then 60 days.</h2>
<p class="text1 text2">Dear Valued Customer, Your age is less then 25 years, so please call us and talk to one of our Agents.
We'll be able to pull up your quote and help you get the coverage that's right for you.<br>
It doesn’t matter if you are 18 or 80, there are always ways for you to save on auto insurance.</p>
<h3 class="bunta-heading text1"><i class="fa fa-phone"></i> Call Now @ +971 40 123456</h3>
<label class="bunta-button bunta-circle" id="dangerPolicy_ok" for="bunta-danger" data-bunta-theme="white"> - OK - </label>
</div>
</div>
</div>						
</div>

<div id="popup_current_ins_expired" style="display:none;">
<input class="bunta-switcher" id="bunta-danger" type="checkbox">
<div id="mpopupBox" class="bunta" data-bunta-style="dos" data-bunta-theme="danger">
<label class="bunta-close" for="bunta-danger">&times;</label>
<label class="bunta-overlay" for="bunta-danger" data-bunta-opacity="40"></label>
<div class="bunta-container" id="banta_cont">
<div class="bunta-center">
<h3 class="bunta-heading-icon"><i class="fa fa-id-card"></i></h3>
<h1 class="bunta-heading text1">Oops!</h1>
<h2 class="bunta-heading text1">Your Age is Less then 21 Years</h2>
<p class="text1 text2">Dear Valued Customer, Your age is less then 21 years, so please call us and talk to one of our Agents.
We'll be able to pull up your quote and help you get the coverage that's right for you.<br>
It doesn’t matter if you are 18 or 80, there are always ways for you to save on auto insurance.</p>
<h3 class="bunta-heading text1"><i class="fa fa-phone"></i> Call Now @ +971 40 123456</h3>
<label class="bunta-button bunta-circle" id="ins_expired_danger_ok" for="bunta-danger" data-bunta-theme="white"> - OK - </label>
</div>
</div>
</div>						
</div>

<div id="vehicle_notfor_commercialpurpose" style="display:none;">
<input class="bunta-switcher" id="bunta-danger" type="checkbox">
<div id="mpopupBox" class="bunta" data-bunta-style="dos" data-bunta-theme="danger">
<label class="bunta-close" for="bunta-danger">&times;</label>
<label class="bunta-overlay" for="bunta-danger" data-bunta-opacity="40"></label>
<div class="bunta-container" id="banta_cont">
<div class="bunta-center">
<h3 class="bunta-heading-icon"><i class="fa fa-id-card"></i></h3>
<h1 class="bunta-heading text1">Oops!</h1>
<h2 class="bunta-heading text1">Your Age is Less then 21 Years</h2>
<p class="text1 text2">Dear Valued Customer, Your age is less then 21 years, so please call us and talk to one of our Agents.
We'll be able to pull up your quote and help you get the coverage that's right for you.<br>
It doesn’t matter if you are 18 or 80, there are always ways for you to save on auto insurance.</p>
<h3 class="bunta-heading text1"><i class="fa fa-phone"></i> Call Now @ +971 40 123456</h3>
<label class="bunta-button bunta-circle" id="veh_notcommercial_ok" for="bunta-danger" data-bunta-theme="white"> - OK - </label>
</div>
</div>
</div>						
</div>						

<div id="popup_infomain" style="display:none;">
<input class="bunta-switcher" id="bunta-success" type="checkbox">
<div class="bunta" data-bunta-style="dos" data-bunta-theme="success">
<label class="bunta-close" for="bunta-success">&times;</label>
<label class="bunta-overlay" for="bunta-success" data-bunta-opacity="40"></label>
<div class="bunta-container">
<div class="bunta-center">
<h3 class="bunta-heading-icon"><i class="fa fa-info-circle"></i></h3>
<h2 class="bunta-heading text1">That's Good job!<i class="fa fa-thumbs-up"></i></h2>
<h4 class="bunta-heading text1"><p></p>
You have chosen Rental Car Option. So Amount of 200 AED will be added in Quote</h4>
<p></p>
<label class="bunta-button bunta-circle" id="info_ok" for="bunta-success" data-bunta-theme="white"> - OK - </label>
</div>
</div>
</div>
</div>

<div id="popup_outsideuae_cover" style="display:none;">
<input class="bunta-switcher" id="bunta-success" type="checkbox">
<div class="bunta" data-bunta-style="dos" data-bunta-theme="success">
<label class="bunta-close" for="bunta-success">&times;</label>
<label class="bunta-overlay" for="bunta-success" data-bunta-opacity="40"></label>
<div class="bunta-container">
<div class="bunta-center">
<h3 class="bunta-heading-icon"><i class="fa fa-info-circle"></i></h3>
<h2 class="bunta-heading text1">That's Good job!<i class="fa fa-thumbs-up"></i></h2>
<h4 class="bunta-heading text1"><p></p>
You have chosen Outside UAE Coverage Option. So Amount  will be added in Quote</h4>
<p></p>
<label class="bunta-button bunta-circle" id="info_outsideuae_ok" for="bunta-success" data-bunta-theme="white"> - OK - </label>
</div>
</div>
</div>
</div>

<div id="popup_pa_coverDriver" style="display:none;">
<input class="bunta-switcher" id="bunta-success" type="checkbox">
<div class="bunta" data-bunta-style="dos" data-bunta-theme="success">
<label class="bunta-close" for="bunta-success">&times;</label>
<label class="bunta-overlay" for="bunta-success" data-bunta-opacity="40"></label>
<div class="bunta-container">
<div class="bunta-center">
<h3 class="bunta-heading-icon"><i class="fa fa-info-circle"></i></h3>
<h2 class="bunta-heading text1">That's Good job!<i class="fa fa-thumbs-up"></i></h2>
<h4 class="bunta-heading text1"><p></p>
I Need Personal Accident Cover for Driver</h4>
<p></p>
<label class="bunta-button bunta-circle" id="pa_coverDriver_ok" for="bunta-success" data-bunta-theme="white"> - OK - </label>
</div>
</div>
</div>
</div>

<div id="popup_infomain_brand_new" style="display:none;">
<input class="bunta-switcher" id="bunta-success" type="checkbox">
<div class="bunta" data-bunta-style="dos" data-bunta-theme="success">
<label class="bunta-close" for="bunta-success">&times;</label>
<label class="bunta-overlay" for="bunta-success" data-bunta-opacity="40"></label>
<div class="bunta-container">
<div class="bunta-center">
<h3 class="bunta-heading-icon"><i class="fa fa-info-circle"></i></h3>
<h2 class="bunta-heading text1">That's Good job!<i class="fa fa-thumbs-up"></i></h2>
<h4 class="bunta-heading text1"><p></p>
Wow! your Car is Brand New</h4>
<p></p>
<label class="bunta-button bunta-circle" id="info_ok_brand_new" for="bunta-success" data-bunta-theme="white"> - OK - </label>
</div>
</div>
</div>
</div>

<!-- Model End -->
<!-- Pricing Table -->

<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Open+Sans:400,400italic,600,600italic,700,700italic,800,800italic|Source+Sans+Pro:400,300,300italic,400italic,600,600italic,700,700italic,900,900italic|Alegreya+Sans:400,300,300italic,400italic,500,500italic,700,700italic,800,800italic,900,900italic|Poiret+One|Dosis:400,500,300,600,700,800|Lobster+Two:400,400italic,700,700italic|Raleway:400,300,300italic,400italic,500,500italic,600,600italic,700,700italic,800,800italic,900,900italic|Roboto+Condensed:400,300italic,300,400italic,700,700italic|Roboto:400,300,300italic,400italic,500,500italic,700,700italic,900,900italic|Courgette">

<?php
}