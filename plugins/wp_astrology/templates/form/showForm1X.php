<?php
function showForm1($atts) {

$args = array(
'post_type' => 'stern_taxi_car_type',
'nopaging' => true,
'order'     => 'ASC',
'orderby' => 'meta_value',
'meta_key' => '_stern_taxi_car_type_organizedBy'
);
$allTypeCars = get_posts( $args );	

$args = array(
'post_type' => 'stern_listAddress',
'nopaging' => true,
'meta_query' => array(
array(
'key'     => 'typeListAddress',
'value'   => 'destination',
'compare' => '=',
),				
array(
'key'     => 'isActive',
'value'   => 'true',
'compare' => '=',
),
),				
);
$allListAddressesDestination = get_posts( $args );			

$backgroundColor= get_option('stern_taxi_fare_bcolor');
if($backgroundColor!="")
{
$backgroundColor='background-color:'.stern_taxi_fare_hex2rgba($backgroundColor,get_option('stern_taxi_fare_bTransparency'));
}
global $woocommerce;

$currency_symbol = get_woocommerce_currency_symbol();

if (isBootstrapSelectEnabale()=="true")
{
$class = "selectpicker show-tick";
} else {
$class = "form-control";
}


$class_full_row12 = (get_option('stern_taxi_fare_full_row') != "true") ? "class='col-lg-12 col-md-12 col-sm-12 col-xs-12'" : "class='col-lg-12 col-md-12 col-sm-12 col-xs-12'";
$class_full_row6 = (get_option('stern_taxi_fare_full_row') != "true") ? "class='col-lg-6 col-md-6 col-sm-12 col-xs-12'" : "class='col-lg-12 col-md-12 col-sm-12 col-xs-12'";
$class_full_row3 = (get_option('stern_taxi_fare_full_row') != "true") ? "class='col-lg-3 col-md-3 col-sm-12 col-xs-12'" : "class='col-lg-12 col-md-12 col-sm-12 col-xs-12'";
$classMain = (get_option('stern_taxi_fare_form_full_row') != "true") ? "class='col-xs-12 col-sm-6 col-lg-6'" : "class='col-xs-12 col-sm-12 col-lg-12'";

//showDemoSettings($backgroundColor);
?>

<div class="container1 row">
<div class="col col-lg-9 stern-taxi-fare">
<!-- <div class="row"> -->
<!-- <div  <?php //echo $classMain; ?> id="main1" style="<?php //echo $backgroundColor; ?>;padding-bottom: 5px" > -->
<?php
$form_dataset = get_option( 'frontend_dataVinfo' );
//$data_setfd = json_decode($form_dataset, true); 
$form_dataComp = get_option( 'frontend_dataComp' );
//$data_setComp = json_decode($form_dataComp, true); 
foreach ( $form_dataComp as $data_dataComp ) {
$veh_cat_2[] = $data_dataComp[2]; 
$repair_con[] = $data_dataComp[12]; 		
} 
foreach ( $form_dataset as $data_vbrand ) {
$name_arr[] = $data_vbrand[0]; 
$medel_year[] = $data_vbrand[1]; 
$modelno_submodel[] = $data_vbrand[2]; 
$vehicle_cat[] = $data_vbrand[5]; 
//$sub_model[]= $data_vbrand[4]; 			
} 
$vehicle_brand = array_unique($name_arr);
$veh_medel_year = array_unique($medel_year);
$veh_modelno = array_unique($modelno_submodel);
$veh_vehicle_cat = array_unique($vehicle_cat);
//$veh_sub_model = array_unique($sub_model);
$vehicle_cat_2 = array_unique($veh_cat_2);
$repair_condition = array_unique($repair_con);
?>

<div class="smart-wrap">
<div id="XXstep1" class="smart-forms smart-container wrap-1">

<div class="form-body smart-steps steps-progress stp-three">
		<form  id="smart-form" method="post">
					<h2>vehicle information</h2>
                 <fieldset>
                <br/>
                <br/>
                <br/>
			   <!-- STEP 1 HTML -->
				<div class="frm-row">
					<div class="section colm colm6">
						<p class="step1_labels">Your Vehicle Brand is</p>
					</div>
					<div class="section colm colm6">
						<label class="field select">
						<select class="calculate1 percentage" id="vehicle_brand" name="vehicle_brand" required>
						<option class="options" value="0" >-- Select --</option>
						<?php
						foreach ( $vehicle_brand as $data_vbrand ) { ?>
						<option class="options" value="<?php echo $data_vbrand;?>"><?php echo $data_vbrand;?></option>
						<?php } ?>
						</select><i class="arrow double"></i><b class="tooltip tip-right-top"><em> Hey buddy! iam a top left tooltip.</em></b>
						</label>						
					</div>
				</div>
				
				
				<div class="frm-row">
					
					<div class="section colm colm6">
						<p class="step1_labels">Is Your Vehicle Brand New?</p>
					</div>
					
					<div class="section colm colm3 percentage">			
					<label class="switch switch-round block option brand-new-yes">
					  <input type="radio" class="veh_brand_new" name="new_check" id="fr3" value="Yes" >
					  <span class="switch-label" data-on="YES" data-off="YES"></span>
					  <span>  </span>
					</label>			
					</div>
					
					<div class="section colm colm3 percentage">	
					<label class="switch switch-round block option brand-new-no">
					  <input type="radio" class="veh_brand_new" name="new_check" id="fr4" value="No" checked>
					  <span class="switch-label" data-on="NO" data-off="NO"></span>
					  <span>  </span>
					</label>                        			   
					</div>
					
				</div>
				
				<div id="dor_row" class="frm-row form_row-space">
				
				<div class="section colm colm6">
					<p class="step1_labels">Manufactured Year of Vehicle is</p>
				</div>
				
				<div class="section colm colm6"><div class="form-group">
				<label class="field select">
				<select class="calculate1 percentage" id="model_year" name="model_year" >
				<option data-price="0.00" value="" > --- Model Year --- </option>
				<?php 
				foreach ( $veh_medel_year as $data_medel_year ) { ?>
				<option data-price="5.00" value="<?php echo $data_medel_year;?>"><?php echo $data_medel_year;?></option>
				<?php } ?>		
				</select><i class="arrow"></i><b class="tooltip tip-right-top"><em> Hey buddy! iam a top left tooltip.</em></b>
				</label>
				</div>
				</div>
				</div>

				<div class="frm-row form_row-space">
				<div class="section colm colm6"><p class="step1_labels">Your Vehicle Model Details</p></div>
				<div class="section colm colm6"><div class="form-group">
				<label class="field select">
				<select class="calculate percentage modeloption" id="vehicle_model_detail" name="vec_model_num" required>
				<?php
				//$data_unique_mno = array_unique($data_setfd);
				//foreach ( $veh_modelno as $data_modelNo ) { ?>
				<!-- <option data-price="5.00" value="<?php// echo $data_modelNo;?>"><?php //echo $data_modelNo;?></option> -->
				<?php // } ?>
				</select><i class="arrow"></i><b class="tooltip tip-right-top"><em> Hey buddy! iam a top left tooltip.</em></b>
				</label>
				</div>
				</div>
				</div>

				<div id="dor_row" class="frm-row form_row-space">						
				<div class="section colm colm6">
				<p class="step2_labels">Vehicle First Registration Date is</p>
				</div>
				
				<div class="section colm colm6"><div class="form-group">
				<span id="phone1">
				<label class="field select">
				<select class="calculate percentage ignore" id="vehicle_reg_date1" name="vehicle_reg_date" required>
				<option value="" >-Select-</option>
				<option value="1">1</option>
				<option value="2">2</option>
				<option value="3">3</option>
				<option value="4">4</option>
				<option value="5">5</option>
				<option value="6">6</option>
				<option value="7">7</option>
				<option value="8">8</option>
				<option value="9">9</option>
				<option value="10">10</option>
				<option value="11">11</option>
				<option value="12">12</option>
				<option value="13">13</option>
				<option value="14">14</option>
				<option value="15">15</option>
				<option value="16">16</option>
				<option value="17">17</option>
				<option value="18">18</option>
				<option value="19">19</option>
				<option value="20">20</option>
				<option value="21">21</option>
				<option value="22">22</option>
				<option value="23">23</option>
				<option value="24">24</option>
				<option value="25">25</option>
				<option value="26">26</option>
				<option value="27">27</option>
				<option value="28">28</option>
				<option value="29">29</option>
				<option value="30">30</option>
				<option value="31">31</option>												
				</select><i class="arrow"></i><b class="tooltip tip-right-top"><em> Hey buddy! iam a top left tooltip.</em></b>
				</label>
				</span>
				<span id="month">
				<label class="field select">
				<select class="calculate percentage ignore" id="vehicle_reg_date1=2" name="dor_month" required>
				<option value="" >-Select-</option>
				<option value="1">January</option>
				<option value="2">February</option>
				<option value="3">March</option>
				<option value="4">April</option>
				<option value="5">May</option>
				<option value="6">June</option>
				<option value="7">July</option>
				<option value="8">August</option>
				<option value="9">September</option>
				<option value="10" >October</option>
				<option value="11">November</option>
				<option value="12">December</option>										
				</select><i class="arrow"></i><b class="tooltip tip-right-top"><em> Hey buddy! iam a top left tooltip.</em></b>
				</label>
				</span>
				<span id="phone1">
				<label class="field select">
				<select class="calculate percentage ignore" id="vehicle_reg_date3" name="dor_year" required>
				<option value="" >-Select-</option>
				<option value="2018">2018</option>
				<option value="2017" >2017</option>
				<option value="2016">2016</option>
				<option value="2015">2015</option>
				<option value="2014">2014</option>
				<option value="2013">2013</option>
				<option value="2012">2012</option>
				<option value="2011">2011</option>
				<option value="2010">2010</option>
				<option value="2009">2009</option>
				<option value="2008">2008</option>											
				</select><i class="arrow"></i><b class="tooltip tip-right-top"><em> Hey buddy! iam a top left tooltip.</em></b>
				</label>
				</span>
				</div>
				</div>
				</div>
				
				<div class="frm-row form_row-space">
				<div class="section colm colm6">
					<p class="step2_labels">Your City fo Registration is</p>
				</div>
				<div class="section colm colm6">
					<label class="field select">
					<select class="calculate percentage" id="cityof_reg" name="cityof_reg" required>
					<option class="option" value="" >-Select-</option>
					<option class="option" value="abu-dahbi" selected>Abu Dahbi</option>
					<option class="option" value="abudahbi">Dubai</option>
					<option class="option" value="jada">Sharjah</option>
					<option class="option" value="jada">Al Ain</option>
					<option class="option" value="jada">Ajman</option>
					<option class="option" value="jada">Ras Al Khaimah</option>
					<option class="option" value="jada">Fujairah</option>
					<option class="option" value="jada">Umm al-Quwain</option>					
					</select><i class="arrow"></i>  			
					</label>
				</div>
				</div>
				
				<div class="frm-row form_row-space">
				<div class="section colm colm6">
					<p class="step1_labels">Your Car Value is</p>
				</div>
				<div class="section colm colm6">
				<label for="home_phone" class="field prepend-icon">
					<input type="text" placeholder="Car Value" value="" class="gui-input percentage" id="vehicle_val" name="vehicle_val"  required>
					<b class="tooltip tip-right-top"><em> Hey buddy! iam a top left tooltip.</em></b>
					<span class="field-icon"><i class="fa fa-car"></i></span>  
					<div style="@display:none;">
					<p class="car_value_box">Your estimated car value is between <span id="item_minval">0.00</span> AED to <span id="item_maxval">0.00</span> AED and must be approved by the insurance company</p> 
					</div>				
				</label>
				</div>
				</div>

				<div class="frm-row form_row-space">						
				<div class="section colm colm6"><p class="step2_labels">When would you like the policy to start?</p></div>
				<div class="section colm colm6"><div class="form-group">
				<span id="phone1">
				
				<label class="field select">
				<select class="calculate percentage" id="vec_policy_date1" name="vec_policy_date" required>
				<option value="">-Select-</option>
				<option value="1" >1</option>
				<option value="2">2</option>
				<option value="3">3</option>
				<option value="4">4</option>
				<option value="5">5</option>
				<option value="6">6</option>
				<option value="7">7</option>
				<option value="8">8</option>
				<option value="9">9</option>
				<option value="10">10</option>
				<option value="11">11</option>
				<option value="12">12</option>
				<option value="13">13</option>
				<option value="14">14</option>
				<option value="15">15</option>
				<option value="16">16</option>
				<option value="17">17</option>
				<option value="18">18</option>
				<option value="19">19</option>
				<option value="20">20</option>
				<option value="21">21</option>
				<option value="22">22</option>
				<option value="23">23</option>
				<option value="24">24</option>
				<option value="25">25</option>
				<option value="26">26</option>
				<option value="27">27</option>
				<option value="28">28</option>
				<option value="29">29</option>
				<option value="30">30</option>
				<option value="31">31</option>												
				</select>
				</label><i class="arrow"></i><b class="tooltip tip-right-top"><em> Hey buddy! iam a top left tooltip.</em></b>
				
				</span>
				<span id="month">
				
				<label class="field select">
				<select class="calculate percentage" id="vec_policy_date2" name="vec_policy_month2" required>
				<option value="">-Select-</option>
				<option value="1" >January</option>
				<option value="2">February</option>
				<option value="3">March</option>
				<option value="4">April</option>
				<option value="5">May</option>
				<option value="6">June</option>
				<option value="7">July</option>
				<option value="8">August</option>
				<option value="9">September</option>
				<option value="10">October</option>
				<option value="11">November</option>
				<option value="12">December</option>										
				</select><i class="arrow"></i><b class="tooltip tip-right-top"><em> Hey buddy! iam a top left tooltip.</em></b>
				</label>
				
				</span>
				<span id="phone1">
				
				<label class="field select">
				<select class="calculate percentage" id="vec_policy_date3" name="vec_policy_date2" required>
				<option value="" >-Select-</option>
				<option value="2018">2017</option>
				<option value="2017" >2018</option>
				<option value="2016">2019</option>										
				</select><i class="arrow"></i><b class="tooltip tip-right-top"><em> Hey buddy! iam a top left tooltip.</em></b>
				</label>
				
				</span>
				</div>
				</div>
				</div>

				<div class="frm-row">
				<div class="section colm colm6">
					<p class="step1_labels">Your Email Address is</p>
				</div>
				<div class="section colm colm6">
					<label for="email_address" class="field prepend-icon">
					<input type="email" placeholder="Email Address" class="gui-input required percentage" id="email_address" name="email_address" />
					<b class="tooltip tip-right-top"><em> Hey buddy! iam a top left tooltip. You notice me now huh?</em></b>
					<span class="field-icon"><i class="fa fa-envelope"></i></span>  
					</label>
				</div>					
				</div> 

                </fieldset>
				
				<h2>Cover Information</h2>
                <fieldset>
				<!-- STEP 2 HTML MARKUP GOES HERE-->                                           
                
				
				<div class="frm-row" id="my_current_ins_not_exp">					
					<div class="section colm colm6">
						<p class="step1_labels">My Current Insurance is not Expired</p>						
					</div>
					
					<div class="section colm colm3  percentage">			
					<label class="switch switch-round block option">
					  <input type="radio" class="current_ins_exp" name="mycurrentins_chk" id="fr5" value="Yes" >					  
					  <span class="switch-label" data-on="YES" data-off="YES"></span>
					  <span>  </span>
					<b class="tooltip"><em> This is a radio tooltip </em></b>					  
					</label>			
					</div>
					
					<div class="section colm colm3">	
					<label class="switch switch-round block option">
					  <input type="radio" class="current_ins_exp" name="mycurrentins_chk" id="fr6" value="No" checked>
					  <span class="switch-label" data-on="NO" data-off="NO"></span>
					  <span>  </span>
					  <b class="tooltip"><em> This is a radio tooltip </em></b>
					</label>                        			   
					</div>					
				</div>				
				
				<div class="frm-row">					
					<div class="section colm colm6">
						<p class="step1_labels">My Current Policy is Comprehensive Insurance</p>
					</div>
					
					<div class="section colm colm3 percentage">			
					<label class="switch switch-round block option">
					  <input type="radio" class="current_policy_comp_ins" name="mycurrentpolicy_chk" id="fr7" value="Yes" checked>
					  <span class="switch-label" data-on="YES" data-off="YES"></span>
					  <span>  </span>
					  <b class="tooltip"><em> This is a radio tooltip </em></b>
					</label>			
					</div>
					
					<div class="section colm colm3">	
					<label class="switch switch-round block option">
					  <input type="radio" class="current_policy_comp_ins" name="mycurrentpolicy_chk" id="fr8" value="No">
					  <span class="switch-label" data-on="NO" data-off="NO"></span>
					  <span>  </span>
					  <b class="tooltip"><em> This is a radio tooltip </em></b>
					</label>                        			   
					</div>					
				</div>	
				
				<div class="frm-row">					
					<div class="section colm colm6">
						<p class="step1_labels">In My Current Policy! Agency Repair is Include</p>
					</div>
					
					<div class="section colm colm3 percentage">			
					<label class="switch switch-round block option">
					  <input type="radio" class="cur_policy_agency_repair" name="currentpolicyagency_chk" id="fr9" value="Yes" checked>
					  <span class="switch-label" data-on="YES" data-off="YES"></span>
					  <span>  </span>
					  <b class="tooltip"><em> This is a radio tooltip </em></b>
					</label>			
					</div>
					
					<div class="section colm colm3">	
					<label class="switch switch-round block option">
					  <input type="radio" class="cur_policy_agency_repair" name="currentpolicyagency_chk" id="fr10" value="No">
					  <span class="switch-label" data-on="NO" data-off="NO"></span>
					  <span>  </span>
					  <b class="tooltip"><em> This is a radio tooltip </em></b>
					</label>                        			   
					</div>					
				</div>
				
				<div class="frm-row">					
					<div class="section colm colm6">
						<p class="step1_labels">My Vehicle is GCC Specification & not Modified</p>
					</div>
					
					<div class="section colm colm3 percentage">			
					<label class="switch switch-round block option">
					  <input type="radio" class="vehicle_is_gcc_spec" name="vehicle_isgcc_chk" id="fr11" value="Yes" checked>
					  <span class="switch-label" data-on="YES" data-off="YES"></span>
					  <span>  </span>
					  <b class="tooltip"><em> This is a radio tooltip </em></b>
					</label>			
					</div>
					
					<div class="section colm colm3">	
					<label class="switch switch-round block option">
					  <input type="radio" class="vehicle_is_gcc_spec" name="vehicle_isgcc_chk" id="fr12" value="No">
					  <span class="switch-label" data-on="NO" data-off="NO"></span>
					  <span>  </span>
					  <b class="tooltip"><em> This is a radio tooltip </em></b>
					</label>                        			   
					</div>					
				</div>									
				
				<div class="frm-row">					
					<div class="section colm colm6">
						<p class="step1_labels">My Vehicle is not for Commercial Purpose</p>
					</div>
					
					<div class="section colm colm3 percentage">			
					<label class="switch switch-round block option">
					  <input type="radio" class="not_for_commercial_purpose" name="vehiclecommercial_chk" id="fr13" value="Yes" checked>
					  <span class="switch-label" data-on="YES" data-off="YES"></span>
					  <span>  </span>
					  <b class="tooltip"><em> This is a radio tooltip </em></b>
					</label>			
					</div>
					
					<div class="section colm colm3">	
					<label class="switch switch-round block option ">
					  <input type="radio" class="not_for_commercial_purpose" name="vehiclecommercial_chk" id="fr14" value="No">
					  <span class="switch-label" data-on="NO" data-off="NO"></span>
					  <span>  </span>
					  <b class="tooltip"><em> This is a radio tooltip </em></b>
					</label>                        			   
					</div>					
				</div>									
				
				<div class="frm-row">					
					<div class="section colm colm6">
						<p class="step1_labels">My Vehicle is Registered under My Name</p>
					</div>
					
					<div class="section colm colm3 percentage">			
					<label class="switch switch-round block option">
					  <input type="radio" class="reg_under_myname" name="vehicleregisteredname_chk" id="fr15" value="Yes" checked>
					  <span class="switch-label" data-on="YES" data-off="YES"></span>
					  <span>  </span>
					  <b class="tooltip"><em> This is a radio tooltip </em></b>
					</label>			
					</div>
					
					<div class="section colm colm3">	
					<label class="switch switch-round block option">
					  <input type="radio" class="reg_under_myname" name="vehicleregisteredname_chk" id="fr16" value="No">
					  <span class="switch-label" data-on="NO" data-off="NO"></span>
					  <span>  </span>
					  <b class="tooltip"><em> This is a radio tooltip </em></b>
					</label>                        			   
					</div>					
				</div>									
				
				<div class="frm-row">					
					<div class="section colm colm6">
						<p class="step1_labels">Did you claim in the last 12 Months?</p>
					</div>
					
					<div class="section colm colm3 percentage">			
					<label class="switch switch-round block option">
					  <input type="radio" class="claim_in_last12months" name="last12monthsclaim_chk" id="fr17" value="Yes" checked>
					  <span class="switch-label" data-on="YES" data-off="YES"></span>
					  <span>  </span>
					  <b class="tooltip"><em> This is a radio tooltip </em></b>
					</label>			
					</div>
					
					<div class="section colm colm3">	
					<label class="switch switch-round block option">
					  <input type="radio" class="claim_in_last12months" name="last12monthsclaim_chk" id="fr18" value="No">
					  <span class="switch-label" data-on="NO" data-off="NO"></span>
					  <span>  </span>
					  <b class="tooltip"><em> This is a radio tooltip </em></b>
					</label>                        			   
					</div>					
				</div>									
				
				<div class="frm-row form_row-space">				
				<div class="section colm colm6">
					<p class="step2_labels">Do you have NCD Certificate from your insurance?</p>
				</div>
				
				<div class="section colm colm6">
					<label class="field select">				
					<select class="calculate percentage" id="years_dltotal" name="long_uaedrivinglicence" required>
					<option class="option" value="0" >-- Select --</option>
					<option class="option" value="no" >No, I have No Certificate</option>
					<option class="option" value="0-6m">0 to 6 months</option>
					<option class="option" value="6-12m">6 to 12 months</option>
					<option class="option" value="1 year">1 year</option>
					<option class="option" value="2 years">2 years</option>
					<option class="option" value="3 years">3 years</option>
					<option class="option" value="4 years">4 years</option>
					<option class="option" value="4+ years">4+ years</option>
					</select><i class="arrow"></i>
					</label>
				</div>
				</div>
				
				<div class="spacer-b30">
					<div class="tagline"><span> Additional Cover for Your peace of Mind </span></div><!-- .tagline -->
				</div>
				
				<div class="frm-row">					
					<div class="section colm colm6">
						<p class="step1_labels">Excess Amount i will Pay</p>
					</div>
					
					<div class="section colm colm3 percentage">			
					<label class="switch switch-round block option">
					  <input type="radio" class="excess_amountpay" name="excess_ammount_chk" id="fr19" value="Yes" checked>
					  <span class="switch-label" data-on="YES" data-off="YES"></span>
					  <span>  </span>
					  <b class="tooltip"><em> This is a radio tooltip </em></b>
					</label>			
					</div>
					
					<div class="section colm colm3">	
					<label class="switch switch-round block option">
					  <input type="radio" class="excess_amountpay" name="excess_ammount_chk" id="fr20" value="No">
					  <span class="switch-label" data-on="NO" data-off="NO"></span>
					  <span>  </span>
					  <b class="tooltip"><em> This is a radio tooltip </em></b>
					</label>                        			   
					</div>					
				</div>				
				
				<div class="frm-row" id="rent_a_car_id">					
					<div class="section colm colm6">
						<p class="step1_labels">I Need My Vehicle Replacement / Rental Car</p>
					</div>
					
					<div class="section colm colm3 percentage">			
					<label class="switch switch-round block option">
					  <input type="radio" class="need_veh_replacement" name="veh_replacement_chk" id="fr21" value="Yes" >
					  <span class="switch-label" data-on="YES" data-off="YES"></span>
					  <span>  </span>
					  <b class="tooltip"><em> This is a radio tooltip </em></b>
					</label>			
					</div>
					
					<div class="section colm colm3">	
					<label class="switch switch-round block option">
					  <input type="radio" class="need_veh_replacement" name="veh_replacement_chk" id="fr22" value="No" checked>
					  <span class="switch-label" data-on="NO" data-off="NO"></span>
					  <span>  </span>
					  <b class="tooltip"><em> This is a radio tooltip </em></b>
					</label>                        			   
					</div>					
				</div>				
				
				<div class="frm-row" id="pa_cover_driver_id">					
					<div class="section colm colm6">
						<p class="step1_labels">I Need Personal Accident Cover for Driver</p>
					</div>
					
					<div class="section colm colm3 percentage">			
					<label class="switch switch-round block option">
					  <input type="radio" class="need_per_acc_cover" name="per_accidentcover_chk" id="fr23" value="Yes" checked>
					  <span class="switch-label" data-on="YES" data-off="YES"></span>
					  <span>  </span>
					  <b class="tooltip"><em> This is a radio tooltip </em></b>
					</label>			
					</div>
					
					<div class="section colm colm3">	
					<label class="switch switch-round block option">
					  <input type="radio" class="need_per_acc_cover" name="per_accidentcover_chk" id="fr24" value="No">
					  <span class="switch-label" data-on="NO" data-off="NO"></span>
					  <span>  </span>
					  <b class="tooltip"><em> This is a radio tooltip </em></b>
					</label>                        			   
					</div>					
				</div>

				<div class="frm-row form_row-space" id="pa_cover_passangers_id">				
				<div class="section colm colm6">
					<p class="step2_labels">I Need Personal Accident Cover for Passengers</p>
				</div>				
				<div class="section colm colm6">
					<label class="field select">				
					<select class="calculate percentage" id="pa_cover_pass" name="long_uaedrivinglicence" required>
					<option class="option" value="0" >-Select-</option>
					<option class="option" value="1" >For 1 Person</option>
					<option class="option" value="2" >For 2 Persons</option>
					<option class="option" value="3" >For 3 Persons</option>
					<option class="option" value="4" >For 4 Persons</option>
					<option class="option" value="5" >For 5 Persons</option>
					<option class="option" value="6" >For 6 Persons</option>
					<option class="option" value="7" >For 7 Persons</option>
					</select><i class="arrow"></i>
					</label>
				</div>
				</div>				
				
				<div class="frm-row form_row-space">				
				<div class="section colm colm6">
					<p class="step2_labels">I Need Personal Agency Repair From</p>
				</div>				
				<div class="section colm colm6">
					<label class="field select">				
					<select class="calculate percentage" id="per_agency_repair" name="per_agency_repair" required>
					<option class="option" value="" > ----- Select -----</option>
					<option class="option" value="Garrage Repairs" >Garrage Repairs</option>
					<option class="option" value="Premier Workshop">Premier Workshop</option>
					<option class="option" value="Inside Agency">Inside Agency</option>
					</select><i class="arrow"></i>
					</label>
				</div>
				</div>
				
				<div class="frm-row" id="outside_uae_coverage_id">					
					<div class="section colm colm6">
						<p class="step1_labels">I Need Outside UAE Coverage</p>
					</div>
					
					<div class="section colm colm3 percentage">			
					<label class="switch switch-round block option">
					  <input type="radio" class="need_out_uae_cover" name="outsideuaeCover_chk" id="fr25" value="Yes">
					  <span class="switch-label" data-on="YES" data-off="YES"></span>
					  <span>  </span>
					  <b class="tooltip"><em> This is a radio tooltip </em></b>
					</label>			
					</div>
					
					<div class="section colm colm3">	
					<label class="switch switch-round block option">
					  <input type="radio" class="need_out_uae_cover" name="outsideuaeCover_chk" id="fr26" value="No" checked>
					  <span class="switch-label" data-on="NO" data-off="NO"></span>
					  <span>  </span>
					  <b class="tooltip"><em> This is a radio tooltip </em></b>
					</label>                        			   
					</div>					
				</div>
									
                </fieldset> 
				
				<h2>Driver Information</h2>
                <fieldset>
				<!-- STEP 3 HTML MARKUP GOES HERE-->                                           
                
				<div class="frm-row ">						
					<div class="section colm colm6">
					<p class="step2_labels">Your Full name</p>
				</div>
				<div class="section colm colm6">
					<label for="firstName" class="field prepend-icon">
					<input type="text" name="firstName" placeholder="First Name" class="gui-input percentage" id="firstname" value="" required> 
					<span class="field-icon"><i class="fa fa-user"></i></span>  					
					</label>
				</div>
				</div>

				<div class="frm-row">
				<div class="section colm colm6">
					<p class="step2_labels">Your contact number</p>
				</div>
				<div class="section colm colm2">
				<label class="field select">				
				<select class="calculate percentage" id="contact_num1" name="contact_num1" required>
				<option  value="Code" selected> Code </option>
				<option  value="050" > 050 </option>
				<option  value="052" > 052 </option>
				<option  value="053" > 053 </option>
				<option  value="054" > 054 </option>
				<option  value="055" > 055 </option>
				<option  value="056" > 056 </option>
				<option  value="058" > 058 </option>												
				</select><i class="arrow"></i>				
				</label>
				</div>
				
				
				<div class="section colm colm3">				
					<label class="field prepend-icon">
					<input type="text" name="contact_num2" placeholder="4012345" class="percentage" id="contact_num2" value="" required>
					<span class="field-icon"><i class="fa fa-phone-square"></i></span>
				    </label>
				</div>
				</div>
				
				
				<div class="frm-row">						
				<div class="section colm colm6">
					<p class="step2_labels">Nationality</p>
				</div>
				
				<div class="section colm colm6">
				<label class="field select">
				<select class="calculate percentage" id="nationality" name="packaging" required>
				<option value="0">- Please Select -</option>
				<option value="164">Afghan</option>
				<option value="2">Albanian</option>
				<option value="3">Algerian</option>
				<option value="166">American</option>
				<option value="4">Andorran</option>
				<option value="5">Angolan</option>
				<option value="6">Argentinian</option>
				<option value="7">Armenian</option>
				<option value="8">Australian</option>
				<option value="9">Austrian</option>
				<option value="10">Azerbaijani</option>
				<option value="11">Bahamian</option>
				<option value="12">Bahraini</option>
				<option value="13">Bangladeshi</option>
				<option value="14">Barbadian</option>
				<option value="16">Belgian</option>
				<option value="17">Belizian</option>
				<option value="15">Belorussian</option>
				<option value="18">Beninese</option>
				<option value="19">Bhutanese</option>
				<option value="20">Bolivian</option>
				<option value="21">Bosnian</option>
				<option value="22">Botswanan</option>
				<option value="23">Brazilian</option>
				<option value="165">British</option>
				<option value="25">Bruneian</option>
				<option value="26">Bulgarian</option>
				<option value="27">Burkinese</option>
				<option value="28">Burmese</option>
				<option value="29">Burundian</option>
				<option value="30">Cambodian</option>
				<option value="31">Cameroonian</option>
				<option value="32">Canadian</option>
				<option value="33">Cape Verdean</option>
				<option value="34">Chadian</option>
				<option value="35">Chilean</option>
				<option value="36">Chinese</option>
				<option value="37">Colombian</option>
				<option value="183">Comorian</option>
				<option value="38">Congolese</option>
				<option value="39">Costa Rican</option>
				<option value="40">Croatian</option>
				<option value="41">Cuban</option>
				<option value="42">Cypriot</option>
				<option value="43">Czech</option>
				<option value="44">Danish</option>
				<option value="45">Djiboutian</option>
				<option value="46">Dominican</option>
				<option value="47">Dominican</option>
				<option value="111">Dutch</option>
				<option value="48">Ecuadorean</option>
				<option value="49">Egyptian</option>
				<option value="1">Emirati</option>
				<option value="52">Eritrean</option>
				<option value="53">Estonian</option>
				<option value="54">Ethiopian</option>
				<option value="55">Fijian</option>
				<option value="56">Finnish</option>
				<option value="57">French</option>
				<option value="58">Gabonese</option>
				<option value="59">Gambian</option>
				<option value="60">Georgian</option>
				<option value="61">German</option>
				<option value="62">Ghanaian</option>
				<option value="63">Greek</option>
				<option value="182">Greenland</option>
				<option value="64">Grenadian</option>
				<option value="65">Guatemalan</option>
				<option value="66">Guinean</option>
				<option value="67">Guyanese</option>
				<option value="68">Haitian</option>
				<option value="69">Honduran</option>
				<option value="181">Hong Kong</option>
				<option value="70">Hungarian</option>
				<option value="71">Icelandic</option>
				<option value="72">Indian</option>
				<option value="73">Indonesian</option>
				<option value="74">Iranian</option>
				<option value="75">Iraqi</option>
				<option value="76">Irish</option>
				<option value="78">Italian</option>
				<option value="79">Jamaican</option>
				<option value="80">Japanese</option>
				<option value="81">Jordanian</option>
				<option value="82">Kazakh</option>
				<option value="83">Kenyan</option>
				<option value="185">Kittitians and Nevisians</option>
				<option value="84">Kuwaiti</option>
				<option value="184">Kyrgyz</option>
				<option value="85">Laotian</option>
				<option value="86">Latvian</option>
				<option value="87">Lebanese</option>
				<option value="88">Liberian</option>
				<option value="89">Libyan</option>
				<option value="90">Liechtenstein</option>
				<option value="91">Lithuanian</option>
				<option value="92">Luxembourg</option>
				<option value="93">Macedonian</option>
				<option value="94">Madagascan</option>
				<option value="95">Malawian</option>
				<option value="96">Malaysian</option>
				<option value="97">Maldivian</option>
				<option value="98">Malian</option>
				<option value="99">Maltese</option>
				<option value="100">Mauritanian</option>
				<option value="101">Mauritian</option>
				<option value="102">Mexican</option>
				<option value="103">Moldovan</option>
				<option value="104">Monégasque or Monacan</option>
				<option value="105">Mongolian</option>
				<option value="106">Montenegrin</option>
				<option value="107">Moroccan</option>
				<option value="108">Mozambican</option>
				<option value="109">Namibian</option>
				<option value="110">Nepalese</option>
				<option value="112">New Zealand</option>
				<option value="113">Nicaraguan</option>
				<option value="115">Nigerian</option>
				<option value="114">Nigerien</option>
				<option value="116">North Korean</option>
				<option value="117">Norwegian</option>
				<option value="118">Omani</option>
				<option value="119">Pakistani</option>
				<option value="77">Palestini</option>
				<option value="120">Panamanian</option>
				<option value="121">Papua New Guinean</option>
				<option value="122">Paraguayan</option>
				<option value="123">Peruvian</option>
				<option value="124">Philippine</option>
				<option value="125">Polish</option>
				<option value="126">Portuguese</option>
				<option value="127">Qatari</option>
				<option value="128">Romanian</option>
				<option value="129">Russian</option>
				<option value="130">Rwandan</option>
				<option value="50">Salvadorean</option>
				<option value="180">San Marino</option>
				<option value="131">Saudi Arabian</option>
				<option value="132">Scottish</option>
				<option value="133">Senegalese</option>
				<option value="134">Serbian</option>
				<option value="135">Seychellois</option>
				<option value="136">Sierra Leonian</option>
				<option value="137">Singaporean</option>
				<option value="138">Slovak</option>
				<option value="139">Slovenian</option>
				<option value="140">Solomon Islands</option>
				<option value="141">Somali</option>
				<option value="142">South African</option>
				<option value="143">South Korean</option>
				<option value="144">Spanish</option>
				<option value="145">Sri Lankan</option>
				<option value="146">Sudanese</option>
				<option value="147">Surinamese</option>
				<option value="148">Swazi</option>
				<option value="149">Swedish</option>
				<option value="150">Swiss</option>
				<option value="151">Syrian</option>
				<option value="152">Taiwanese</option>
				<option value="153">Tajik</option>
				<option value="154">Tanzanian</option>
				<option value="155">Thai</option>
				<option value="156">Togolese</option>
				<option value="157">Trinidadian/Tobagonian</option>
				<option value="158">Tunisian</option>
				<option value="159">Turkish</option>
				<option value="160">Turkmen</option>
				<option value="161">Tuvaluan</option>
				<option value="162">Ugandan</option>
				<option value="163">Ukrainian</option>
				<option value="167">Uruguayan</option>
				<option value="168">Uzbek</option>
				<option value="169">Vanuatuan</option>
				<option value="170">Vatican City</option>
				<option value="171">Venezuelan</option>
				<option value="172">Vietnamese</option>
				<option value="174">Western Samoan</option>
				<option value="175">Yemeni</option>
				<option value="176">Yugoslav</option>
				<option value="177">Zairean</option>
				<option value="178">Zambian</option>
				<option value="179">Zimbabwean</option>
				</select><i class="arrow"></i>
				</label>
				</div>
				</div>
				
				<div class="frm-row">
				<div class="section colm colm6">
					<p class="step2_labels">Your Date of Birth</p>
				</div>
				<div class="section colm colm6  percentage">
					<label class="field prepend-icon">
						<input type="text" name="datepicker1" placeholder="20-5-1990" id="datepicker1" class="gui-input" required>
						<span class="field-icon"><i class="fa fa-calendar"></i></span> 						
					</label>
				</div>
			
				<script type="text/javascript">
	
					jQuery(function() {
						   
						jQuery("#datepicker1").datepicker({
							numberOfMonths: 1,					
							prevText: '<i class="fa fa-chevron-left"></i>',
							nextText: '<i class="fa fa-chevron-right"></i>',			
							showButtonPanel: false
						});       
								
					});				
					
				</script>								
				</div>
				
				<div class="frm-row" id="year_uae_driving_licence">
				<div class="section colm colm6">
					<p class="step2_labels">Your Years of UAE Driving Experience</p>
				</div>
				<div class="section colm colm6">
					<label class="field select">
					<select class="calculate percentage" id="years_uaedrivinglicence" name="years_uaedriving" required>
					<option class="option" value="" >-Select-</option>
					<option value="l1year" > Less then one Year </option>
					<option value="1year" > 1 Year </option>
					<option value="2-3years" > 2 to 3 Years </option>
					<option value="morethn-4years"> More then 4 Years </option>
					</select><i class="arrow"></i>
					</label>
				</div>
				</div>

				<div class="frm-row">									
				<div class="section colm colm6">
					<p class="step2_labels">Country of your first driver's license</p>
				</div>
				<div class="section colm colm6">
				<label class="field select">
				<select class="calculate percentage" id="country_firstdl" name="packaging" required>
				<option class="option" value="" >-Select-</option>
				<option class="option" value="Country of your first driver's license" >United Arab Emirates</option>
				<option class="option" value="Albania">Albania</option>
				<option class="option" value="Algeria">Algeria</option>
				<option class="option" value="American Samoa">American Samoa</option>
				<option class="option" value="Andorra">Andorra</option>
				<option class="option" value="Angola">Angola</option>
				<option class="option" value="Anguilla">Anguilla</option>
				<option class="option" value="Antarctica">Antarctica</option>
				<option class="option" value="Antigua and Barbuda">Antigua and Barbuda</option>
				<option class="option" value="Argentina">Argentina</option>
				<option class="option" value="Armenia">Armenia</option>
				<option class="option" value="Aruba">Aruba</option>
				<option class="option" value="Australia">Australia</option>
				<option class="option" value="Austria">Austria</option>
				<option class="option" value="Azerbaijan">Azerbaijan</option>
				<option class="option" value="Bahamas">Bahamas</option>
				<option class="option" value="Bahrain">Bahrain</option>
				<option class="option" value="Bangladesh">Bangladesh</option>
				<option class="option" value="Barbados">Barbados</option>
				<option class="option" value="Belarus">Belarus</option>
				<option class="option" value="Belgium">Belgium</option>
				<option class="option" value="Belize">Belize</option>
				<option class="option" value="Benin">Benin</option>
				<option class="option" value="Bermuda">Bermuda</option>
				<option class="option" value="Bhutan">Bhutan</option>
				<option class="option" value="Bolivia">Bolivia</option>
				<option class="option" value="Bosnia and Herzegovina">Bosnia and Herzegovina</option>
				<option class="option" value="Botswana">Botswana</option>
				<option class="option" value="Bouvet Island">Bouvet Island</option>
				<option class="option" value="Brazil">Brazil</option>
				<option class="option" value="Brunei Darussalam">Brunei Darussalam</option>
				<option class="option" value="Bulgaria">Bulgaria</option>
				<option class="option" value="Burkina Faso">Burkina Faso</option>
				<option class="option" value="Burundi">Burundi</option>
				<option class="option" value="Cambodia">Cambodia</option>
				<option class="option" value="Cameroon">Cameroon</option>
				<option class="option" value="Canada">Canada</option>
				<option class="option" value="Cape Verde">Cape Verde</option>
				<option class="option" value="Cayman Islands">Cayman Islands</option>
				<option class="option" value="Central African Republic">Central African Republic</option>
				<option class="option" value="Chad">Chad</option>
				<option class="option" value="Chile">Chile</option>
				<option class="option" value="China">China</option>
				<option class="option" value="Christmas Island">Christmas Island</option>
				<option class="option" value="Cocos (Keeling) Islands">Cocos (Keeling) Islands</option>
				<option class="option" value="Colombia">Colombia</option>
				<option class="option" value="Comoros">Comoros</option>
				<option class="option" value="Congo">Congo</option>
				<option class="option" value="Cook Islands">Cook Islands</option>
				<option class="option" value="Costa Rica">Costa Rica</option>
				<option class="option" value="Cote optionoire">Cote optionoire</option>
				<option class="option" value="Croatia">Croatia</option>
				<option class="option" value="Cuba">Cuba</option>
				<option class="option" value="Cyprus">Cyprus</option>
				<option class="option" value="Czech Republic">Czech Republic</option>
				<option class="option" value="Denmark">Denmark</option>
				<option class="option" value="Djibouti">Djibouti</option>
				<option class="option" value="Dominica">Dominica</option>
				<option class="option" value="Dominican Republic">Dominican Republic</option>
				<option class="option" value="East Timor">East Timor</option>
				<option class="option" value="Ecuador">Ecuador</option>
				<option class="option" value="Egypt">Egypt</option>
				<option class="option" value="El Salvador">El Salvador</option>
				<option class="option" value="Equatorial Guinea">Equatorial Guinea</option>
				<option class="option" value="Eritrea">Eritrea</option>
				<option class="option" value="Estonia">Estonia</option>
				<option class="option" value="Ethiopia">Ethiopia</option>
				<option class="option" value="Faroe Islands">Faroe Islands</option>
				<option class="option" value="Fiji">Fiji</option>
				<option class="option" value="Finland">Finland</option>
				<option class="option" value="France">France</option>
				<option class="option" value="France Metropolitan">France Metropolitan</option>
				<option class="option" value="French Guiana">French Guiana</option>
				<option class="option" value="French Polynesia">French Polynesia</option>
				<option class="option" value="Gabon">Gabon</option>
				<option class="option" value="Gambia">Gambia</option>
				<option class="option" value="Georgia">Georgia</option>
				<option class="option" value="Germany">Germany</option>
				<option class="option" value="Ghana">Ghana</option>
				<option class="option" value="Gibraltar">Gibraltar</option>
				<option class="option" value="Greece">Greece</option>
				<option class="option" value="Greenland">Greenland</option>
				<option class="option" value="Grenada">Grenada</option>
				<option class="option" value="Guadeloupe">Guadeloupe</option>
				<option class="option" value="Guam">Guam</option>
				<option class="option" value="Guatemala">Guatemala</option>
				<option class="option" value="Guinea">Guinea</option>
				<option class="option" value="Guinea-bissau">Guinea-bissau</option>
				<option class="option" value="Guyana">Guyana</option>
				<option class="option" value="Haiti">Haiti</option>
				<option class="option" value="Honduras">Honduras</option>
				<option class="option" value="Hong Kong">Hong Kong</option>
				<option class="option" value="Hungary">Hungary</option>
				<option class="option" value="Iceland">Iceland</option>
				<option class="option" value="India">India</option>
				<option class="option" value="Indonesia">Indonesia</option>
				<option class="option" value="Iran (Islamic Republic of)">Iran (Islamic Republic of)</option>
				<option class="option" value="Iraq">Iraq</option>
				<option class="option" value="Ireland">Ireland</option>
				<option class="option" value="Italy">Italy</option>
				<option class="option" value="Jamaica">Jamaica</option>
				<option class="option" value="Japan">Japan</option>
				<option class="option" value="Jordan">Jordan</option>
				<option class="option" value="Kazakhstan">Kazakhstan</option>
				<option class="option" value="Kenya">Kenya</option>
				<option class="option" value="Kiribati">Kiribati</option>
				<option class="option" value="Korea">Korea</option>
				<option class="option" value="Kuwait">Kuwait</option>
				<option class="option" value="Kyrgyzstan">Kyrgyzstan</option>
				<option class="option" value="Latvia">Latvia</option>
				<option class="option" value="Lebanon">Lebanon</option>
				<option class="option" value="Lesotho">Lesotho</option>
				<option class="option" value="Liberia">Liberia</option>
				<option class="option" value="Libya">Libya</option>
				<option class="option" value="Liechtenstein">Liechtenstein</option>
				<option class="option" value="Lithuania">Lithuania</option>
				<option class="option" value="Luxembourg">Luxembourg</option>
				<option class="option" value="Macau">Macau</option>
				<option class="option" value="Macedonia">Macedonia</option>
				<option class="option" value="Madagascar">Madagascar</option>
				<option class="option" value="Malawi">Malawi</option>
				<option class="option" value="Malaysia">Malaysia</option>
				<option class="option" value="Maloptiones">Maloptiones</option>
				<option class="option" value="Mali">Mali</option>
				<option class="option" value="Malta">Malta</option>
				<option class="option" value="Martinique">Martinique</option>
				<option class="option" value="Mauritania">Mauritania</option>
				<option class="option" value="Mauritius">Mauritius</option>
				<option class="option" value="Mayotte">Mayotte</option>
				<option class="option" value="Mexico">Mexico</option>
				<option class="option" value="Monaco">Monaco</option>
				<option class="option" value="Mongolia">Mongolia</option>
				<option class="option" value="Montserrat">Montserrat</option>
				<option class="option" value="Morocco">Morocco</option>
				<option class="option" value="Mozambique">Mozambique</option>
				<option class="option" value="Myanmar">Myanmar</option>
				<option class="option" value="Namibia">Namibia</option>
				<option class="option" value="Nauru">Nauru</option>
				<option class="option" value="Nepal">Nepal</option>
				<option class="option" value="Netherlands">Netherlands</option>
				<option class="option" value="New Caledonia">New Caledonia</option>
				<option class="option" value="New Zealand">New Zealand</option>
				<option class="option" value="Nicaragua">Nicaragua</option>
				<option class="option" value="Niger">Niger</option>
				<option class="option" value="Nigeria">Nigeria</option>
				<option class="option" value="Niue">Niue</option>
				<option class="option" value="Norfolk Island">Norfolk Island</option>
				<option class="option" value="Norway">Norway</option>
				<option class="option" value="Oman">Oman</option>
				<option class="option" value="Pakistan">Pakistan</option>
				<option class="option" value="Palau">Palau</option>
				<option class="option" value="Palestine">Palestine</option>
				<option class="option" value="Panama">Panama</option>
				<option class="option" value="Papua New Guinea">Papua New Guinea</option>
				<option class="option" value="Paraguay">Paraguay</option>
				<option class="option" value="Peru">Peru</option>
				<option class="option" value="Philippines">Philippines</option>
				<option class="option" value="Pitcairn">Pitcairn</option>
				<option class="option" value="Poland">Poland</option>
				<option class="option" value="Portugal">Portugal</option>
				<option class="option" value="Puerto Rico">Puerto Rico</option>
				<option class="option" value="Qatar">Qatar</option>
				<option class="option" value="Romania">Romania</option>
				<option class="option" value="Russia">Russia</option>
				<option class="option" value="Rwanda">Rwanda</option>
				<option class="option" value="Saint Kitts and Nevis">Saint Kitts and Nevis</option>
				<option class="option" value="Saint Lucia">Saint Lucia</option>
				<option class="option" value="Samoa">Samoa</option>
				<option class="option" value="San Marino">San Marino</option>
				<option class="option" value="Sao Tome and Principe">Sao Tome and Principe</option>
				<option class="option" value="Saudi Arabia">Saudi Arabia</option>
				<option class="option" value="Serbia">Serbia</option>
				<option class="option" value="Senegal">Senegal</option>
				<option class="option" value="Seychelles">Seychelles</option>
				<option class="option" value="Sierra Leone">Sierra Leone</option>
				<option class="option" value="Singapore">Singapore</option>
				<option class="option" value="Slovakia (Slovak Republic)">Slovakia (Slovak Republic)</option>
				<option class="option" value="Slovenia">Slovenia</option>
				<option class="option" value="Solomon Islands">Solomon Islands</option>
				<option class="option" value="Somalia">Somalia</option>
				<option class="option" value="South Africa">South Africa</option>
				<option class="option" value="Spain">Spain</option>
				<option class="option" value="Sri Lanka">Sri Lanka</option>
				<option class="option" value="St. Helena">St. Helena</option>
				<option class="option" value="Sudan">Sudan</option>
				<option class="option" value="Suriname">Suriname</option>
				<option class="option" value="Swaziland">Swaziland</option>
				<option class="option" value="Sweden">Sweden</option>
				<option class="option" value="Switzerland">Switzerland</option>
				<option class="option" value="Syrian Arab Republic">Syrian Arab Republic</option>
				<option class="option" value="Taiwan">Taiwan</option>
				<option class="option" value="Tajikistan">Tajikistan</option>
				<option class="option" value="Thailand">Thailand</option>
				<option class="option" value="Togo">Togo</option>
				<option class="option" value="Tokelau">Tokelau</option>
				<option class="option" value="Tonga">Tonga</option>
				<option class="option" value="Trinidad and Tobago">Trinidad and Tobago</option>
				<option class="option" value="Tunisia">Tunisia</option>
				<option class="option" value="Turkey">Turkey</option>
				<option class="option" value="Turkmenistan">Turkmenistan</option>
				<option class="option" value="Tuvalu">Tuvalu</option>
				<option class="option" value="Uganda">Uganda</option>
				<option class="option" value="Ukraine">Ukraine</option>
				<option class="option" value="United Arab Emirates">United Arab Emirates</option>
				<option class="option" value="United Kingdom">United Kingdom</option>
				<option class="option" value="United States">United States</option>
				<option class="option" value="Uruguay">Uruguay</option>
				<option class="option" value="Uzbekistan">Uzbekistan</option>
				<option class="option" value="Vanuatu">Vanuatu</option>
				<option class="option" value="Venezuela">Venezuela</option>
				<option class="option" value="Viet Nam">Viet Nam</option>
				<option class="option" value="Virgin Islands (British)">Virgin Islands (British)</option>
				<option class="option" value="Virgin Islands (U.S.)">Virgin Islands (U.S.)</option>
				<option class="option" value="Wallis and Futuna Islands">Wallis and Futuna Islands</option>
				<option class="option" value="Western Sahara">Western Sahara</option>
				<option class="option" value="Yemen">Yemen</option>
				<option class="option" value="Yugoslavia">Yugoslavia</option>
				<option class="option" value="Zambia">Zambia</option>
				<option class="option" value="Zimbabwe">Zimbabwe</option>
				</option>	
				</select><i class="arrow double"></i> 			
				</label>
				</div>												
				</div>

		
				<div class="frm-row">
				<div class="section colm colm6">
					<p class="step2_labels">Your Home Country Driving Experience</p>
				</div>
				<div class="section colm colm6">
					<label class="field select">
					<select class="calculate percentage" id="home_country_drivingexp" name="home_country_drivingexp" required>
					<option value="" > ---- Select ---- </option>										
					<option value="l1year" > ---- Select ---- </option>										
					<option value="l1year" > Less then one year </option>										
					<option value="1year" > 1 year </option>										
					<option value="2-3year" > 2 to 3 years </option>										
					<option value="mthn4years" > More then 4 Years </option>										
					</select><i class="arrow"></i>
					</label>
				</div>
				</div>				
				
				<div class="frm-row">
				<div class="section colm colm6">
					<p class="step2_labels">Years of Claim-Free Driving in UAE</p>
				</div>
				<div class="section colm colm6">
					<label class="field select">
					<select class="calculate percentage" id="years_claimfreeUAE" name="years_claimfreeUAE" required>
					<option value="" > ----- Select ----- </option>						
					<option value="self-declaration" > Self-Declaration </option>						
					<option value="1-year" > 1 Year (with proof) </option>						
					<option value="2-year" > 2 Year (with proof) </option>						
					<option value="3-year" > 3 Year (with proof) </option>						
					<option value="4-year" > 4 Year (with proof) </option>						
					<option value="5-plus" > More then 5 years </option>						
					</select><i class="arrow"></i>
					</label>
				</div>
				</div>
				
                </fieldset> 
	
</form>
</div>
</div>
</div>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
<!-- Jquery  Code -->
<?php  
$json_form_dataset = json_encode($form_dataset); 
$json_form_dataComp = json_encode($form_dataComp); 
?>    
<script>
jQuery(document).ready(function() {	

	//Model Condition
	var ideal_date_21    = "01/1/1996"
		
	jQuery( "#datepicker1" ).change(function() {
		var age_less_25 = jQuery('#datepicker1').val();

		console.log("ideal_date_21.."+ideal_date_21);
		console.log("age_less_25.."+age_less_25);

	if(new Date(age_less_25) > new Date(ideal_date_21)){
		
		console.log("age is less then 21 ...");
	}
	});
	
	
	jQuery("#pa_cover_driver_chekcbox0").change(function () {
		if(jQuery("#pa_cover_driver_chekcbox0").prop("checked")){
			console.log("Checked");
		}else{			
			console.log("Un Checked");		
		}
	});
	jQuery('#checkAll').change(function () {
		//console.log("clicked");
		if(jQuery("#checkAll").prop("checked")){
			console.log("Checked");
		}else{
			console.log("Un Checked");		
		}
	});

						
	function refreshPrice(){	
	}      
	
	//Hide and Remove Date of Registration

	
	
	
	jQuery('#dor_row').removeClass( "hide_this" ); 
	
	jQuery('.brand-new-yes').click(function() {
		jQuery('#dor_row').addClass( "hide_this" );			
	});
	jQuery('.brand-new-no').click(function() {
		jQuery('#dor_row').removeClass( "hide_this" ); 
	});
	
	jQuery(function () {
	jQuery('#reg_date').datepicker({
	format: 'mm/dd/yyyy',
	startDate: '-3d'					
	});
	});
	jQuery(function () {       
	jQuery('#polstart_date').datepicker({
	format: 'mm/dd/yyyy',
	startDate: '-3d'
	});
	});
	jQuery(function () {
	jQuery('#dob').datepicker({
	format: 'mm/dd/yyyy',
	startDate: '-3d'
	});
	}); 



jQuery(".percentage").change(function(){ 
jQuery(this).addClass("changed");
var change_count = jQuery('.changed').length;
var currentval = parseFloat(change_count);
currentval1 = currentval * 2.94;
currentval11 = currentval1.toFixed(1);
jQuery("#percentage-text").html(currentval11+'%');

var circlefill = 'p'+currentval1;
//////console.log("circlefill..."+circlefill);
jQuery( "#progress:last" ).addClass( circlefill );
});

jQuery('[data-toggle="tooltip"]').tooltip();   
jQuery("#btn_step1").addClass("active_step");
 
jQuery("#btn_step1").click(function(){
jQuery("#btn_step2").removeClass("active_step");
jQuery("#btn_step1").addClass("active_step");
});		
jQuery("#btn_step2").click(function(){
jQuery("#btn_step1").removeClass("active_step");
jQuery("#btn_step2").addClass("active_step");
});		
jQuery("#btn_nextstep2inner").click(function(){
jQuery("#btn_step1").removeClass("active_step");
jQuery("#btn_step2").removeClass("active_step");
});  

var basePrice = 3.00;
var width;

//jQuery( "#ins_submodel" ).hide();
jQuery( "#step2" ).hide();
jQuery( "#step3" ).hide();
jQuery( "#step4" ).hide();
jQuery("#calCheckout_url").hide();

jQuery( "#btn_step2" ).click(function() {
jQuery( "#step1" ).hide();
jQuery( "#step2" ).show();
}); 	

jQuery( "#btn_nextstep1inner" ).click(function() {

jQuery( "#step1" ).hide();
jQuery( "#step2" ).show();
jQuery("#btn_step1").removeClass("active_step");
jQuery("#btn_step2").addClass("active_step");
//var vehicle_brand = jQuery('#vehicle_brand option:selected').val();
//var selected_modelYear = jQuery('#model_year option:selected').val();
//var vehicle_model_detail = jQuery('#vehicle_model_detail option:selected').val();
//var selected_vehCat = jQuery('#vehicle_cat option:selected').val();	 
});  	

jQuery( "#btn_step1" ).click(function() {
jQuery( "#step2" ).hide();
jQuery( "#step1" ).show();
}); 

jQuery(".calculate").change(function () {
show_minmax();
});

jQuery("#model_year").change(function () {
	show_models();
});
jQuery("#vehicle_brand").change(function () {
	show_models();
}); 

function show_models(){
	var step_1datam = <?php echo $json_form_dataset; ?>;
	var selected_modelYear = jQuery('#model_year option:selected').val();
	var vehicle_brand_filter = jQuery('#vehicle_brand option:selected').val();
	var final_array = [];
	var final_array2 =[];
	
	jQuery.each( step_1datam, function( m, brand_filter ) {								
				if(brand_filter[0] == vehicle_brand_filter && brand_filter[1] == selected_modelYear){
					final_array2.push(brand_filter[2]);
				}
	});						 
	//console.log(final_array2);
	
	//Set to option Make
	jQuery('.modeloption').html('');
	jQuery.each( final_array2, function( i, veh_model_set ) {
		jQuery('.modeloption').append('<option class="options" value="'+veh_model_set+'">'+veh_model_set+'</option>');
	});	
	show_minmax();
}


function show_minmax(){
	//Step1 and selected Data arrays
	var step_1datam = <?php echo $json_form_dataset; ?>;

	//Get selected values
	var vehicle_brand = jQuery('#vehicle_brand option:selected').val();
	var selected_modelYear = jQuery('#model_year option:selected').val();
	
	//Show Model	
/* 	var selected_modelYear = jQuery('#model_year option:selected').val();
	var vehicle_brand_filter = jQuery('#vehicle_brand option:selected').val();
	var final_array = [];
	var final_array2 =[];
	
	jQuery.each( step_1datam, function( m, brand_filter ) {								
				if(brand_filter[0] == vehicle_brand_filter && brand_filter[1] == selected_modelYear){
					final_array2.push(brand_filter[2]);
				}
	});						 
	//console.log(final_array2);
	
	//Set to option Make
	jQuery('.modeloption').html('');
	jQuery.each( final_array2, function( i, veh_model_set ) {
		jQuery('.modeloption').append('<option class="options" value="'+veh_model_set+'">'+veh_model_set+'</option>');
	});
 */		
	//Show Model Option End
	
	//Year Selection Logic
	var selected_modelYear = jQuery('#model_year option:selected').val();	
	var currentYear = (new Date).getFullYear();	
	jQuery('.veh_brand_new').click(function () {
		var veh_brandnewcheck = jQuery(this).val();
		
		if(veh_brandnewcheck == "Yes"){
			//console.log("Checked");
			selected_modelYear = currentYear;
			//console.log("selected_modelYear..."+selected_modelYear);
		}
		else if(veh_brandnewcheck == "No"){
			//console.log("Un Checked");
			selected_modelYear = jQuery('#model_year option:selected').val();		
		}
	});

var vehicle_model_detail = jQuery('#vehicle_model_detail option:selected').val();
var selected_valarraym = [vehicle_brand, selected_modelYear, vehicle_model_detail];
var ins_selected_valarraym = vehicle_brand+selected_modelYear+vehicle_model_detail;

jQuery.each( step_1datam, function( i, valuem ) {
value_min_max1 = valuem[0];
value_min_max2 = valuem[1];
value_min_max3 = valuem[2];
value_min_max4 = valuem[3];
value_min_max5 = valuem[4];

rt_valuem = value_min_max1+value_min_max2+value_min_max3;

if(ins_selected_valarraym == rt_valuem){
var rt_min_valm = valuem[3];
var rt_max_valm = valuem[4];
//////console.log("rt_min_valm..."+rt_min_valm);

//Display Min and Max Values.
document.getElementById("item_minval").innerHTML = rt_min_valm;
document.getElementById("item_maxval").innerHTML = rt_max_valm;
//document.getElementById("vehicle_submodel").innerHTML = rt_min_valm;
jQuery( "#vehicle_val" ).val( rt_min_valm );					
}
});

}	

//Limit Value to Min Max
jQuery("#vehicle_val").change(function () {

var updatedcarval1 = jQuery( "#vehicle_val" ).val();
var rt_min_get1 = document.getElementById("item_minval").innerHTML;
var rt_max_get1 = document.getElementById("item_maxval").innerHTML;

var updatedcarval = parseInt(updatedcarval1);
var rt_min_get = parseInt(rt_min_get1);
var rt_max_get = parseInt(rt_max_get1);

if(updatedcarval > rt_max_get){
updatedcarval = rt_max_get;						
jQuery( "#vehicle_val" ).val( rt_max_get );
//////console.log("Changed valNNNN..."+updatedcarval);
}
else if(updatedcarval < rt_min_get){
updatedcarval = rt_min_get;
jQuery( "#vehicle_val" ).val( rt_min_get );
//////console.log("Changed valNNNN..."+updatedcarval);
}

}); 
	
	
function get_companies(){

	//Get selected values
	var vehicle_brand = jQuery('#vehicle_brand option:selected').val();
	//var selected_modelYear = jQuery('#model_year option:selected').val();
	
	//Year Selection Logic
	var selected_modelYear = jQuery('#model_year option:selected').val();	
	var currentYear2 = (new Date).getFullYear();	
	jQuery('.veh_brand_new').click(function () {
		var veh_brandnewcheck = jQuery(this).val();
		
		if(veh_brandnewcheck == "Yes"){
			var selected_modelYear = currentYear2;
			//console.log("selected_modelYear2..."+selected_modelYear);
		}
		else if(veh_brandnewcheck == "No"){
			//console.log("Un Checked");
			var selected_modelYear = jQuery('#model_year option:selected').val();		
		}
	});

	var vehicle_model_detail = jQuery('#vehicle_model_detail option:selected').val();
	var selected_vehicle_repair = jQuery('#per_agency_repair option:selected').val();
	var selected_vehicle_modified = jQuery('#vehicle_modified').is(':checked'); 	
	var selected_outside_uae = jQuery('#outside_uae').is(':checked'); 
	var selected_roadside_assistance = jQuery('#roadside_assistance').is(':checked'); 
	var selected_ins_rentACar = jQuery('#ins_rentACar').is(':checked'); 			
	
	//Select Options
	/* 	jQuery('.veh_brand_new').click(function () {
		var veh_brandnewcheck = jQuery(this).val();		
		var current_ins_exp = jQuery(this).val();		
		var current_policy_comp_ins = jQuery(this).val();		
		var cur_policy_agency_repair = jQuery(this).val();		
		var vehicle_is_gcc_spec = jQuery(this).val();		
		var not_for_commercial_purpose = jQuery(this).val();		
		var reg_under_myname = jQuery(this).val();		
		var claim_in_last12months = jQuery(this).val();		
		var excess_amountpay = jQuery(this).val();		
		var need_veh_replacement = jQuery(this).val();		
		var need_per_acc_cover = jQuery(this).val();		
		var need_out_uae_cover = jQuery(this).val();		
	}); */

	var need_per_acc_cover_driver = "Yes";	
	var acc_cover_passangers = "Yes";	
	var rent_a_car_val = "Yes";	
	var outside_uae_cov_val = "Yes";	
	
	jQuery('#pa_cover_driver_id input').on('change', function() {
	   var need_per_acc_cover_driver = jQuery('input[name=per_accidentcover_chk]:checked').val();
	   console.log("need_per_acc_cover_driver.."+need_per_acc_cover_driver);
	});

	jQuery('#rent_a_car_id input').on('change', function() {
	   var rent_a_car_val = jQuery('input[name=veh_replacement_chk]:checked').val();
	   console.log("rent_a_car_val.."+rent_a_car_val);
	   /* if(rent_a_car_val == 'Yes' ){
		   //alert(" You have Selected Rent A Car Service.");
		   console.log("Rent Car Yes");
	   } */
	});	
	
	jQuery('#outside_uae_coverage_id input').on('change', function() {
	   var outside_uae_cov_val = jQuery('input[name=outsideuaeCover_chk]:checked').val();
	   console.log("outside_uae_cov_val.."+outside_uae_cov_val);
	});
		
		var pa_cover_passangers = jQuery('#pa_cover_pass option:selected').val();
		console.log("pa_cover_passangers.."+pa_cover_passangers);
	
		var years_uaedriving_lic = jQuery('#years_uaedrivinglicence option:selected').val();
		console.log("years_uaedriving_lic.."+years_uaedriving_lic);
		
		var ideal_date    = "01/1/1992"
		var ideal_date_21    = "01/1/1996"
		var age_less_25 = jQuery('#datepicker1').val();
		//console.log("age_less_25.."+age_less_25);

	//Select Options End
	
	// does not have a value selected Submodel
	var selected_valarray = [vehicle_brand, selected_modelYear, vehicle_model_detail];

	//Step1 and selected Data arrays
	var step_1data = <?php echo $json_form_dataset; ?>;
	var step_dataComp = <?php echo $json_form_dataComp; ?>;
	var rt_min_val = 0;
	jQuery.each( step_1data, function( i, value ) {
	value0 = value[0];
	value1 = value[1];
	value2 = value[2];
	value3 = value[3];
	value4 = value[4];
	value5 = value[5];
	value6 = value[6];
	
	selected_valarray0 = selected_valarray[0];
	selected_valarray1 = selected_valarray[1];
	selected_valarray2 = selected_valarray[2];

	//Selected Options array	
	rt_selected_valarray = selected_valarray0+selected_valarray1+selected_valarray2;	
	rt_value = value0+value1+value2;
	
	//console.log("rt_selected_valarray.."+rt_selected_valarray);	
	///console.log("rt_value.."+rt_value);	
	
		if(rt_selected_valarray == rt_value){
		////console.log("rt_selected_valarray.."+rt_selected_valarray);	
		////console.log("rt_value.."+rt_value);	

		//Getting value from user Entered Field.
		var updatedcarval1 = jQuery( "#vehicle_val" ).val();

		if(updatedcarval1){						
			rt_min_val = updatedcarval1;						
		}
		//rt_min_val = value[5];
			rt_max_val = value[4];
						
		}		  
	}); 
	var all_companies = [];
	jQuery.each( step_dataComp, function( j, valuecomp ) {

	var min_valcomp = valuecomp[6];
	var max_valcomp = valuecomp[7];
	//console.log("min_valcomp..."+min_valcomp);	
	//console.log("max_valcomp..."+max_valcomp);	

	if(min_valcomp){
		min_valcomp1 = parseFloat(min_valcomp.replace(/\,/g,''));
	}
	if(min_valcomp){
		max_valcomp1 = parseFloat(max_valcomp.replace(/\,/g,''));
	}

	rt_min_val1 = parseFloat(rt_min_val);
	//console.log("rt_min_val1..."+rt_min_val1);

	if( rt_min_val1>=min_valcomp1&&rt_min_val1<=max_valcomp1){
	//if(valuecomp[2] == selected_vehCat2){
	//console.log("selected_vehicle_repair..."+selected_vehicle_repair);
	//console.log("valuecomp[12]..."+valuecomp[12]);
	
	if(valuecomp[12] == selected_vehicle_repair){
	
	valuecomp_min_val1 = parseFloat(valuecomp[6]);
	valuecomp_rate = parseFloat(valuecomp[8]);		
	cal_rate = valuecomp_min_val1 * valuecomp_rate;
	
	if(cal_rate < valuecomp[6]){
		cal_rate = valuecomp[6]
	}
	
	//Per Acc Cover Driver	
	cal_personal_accident = valuecomp[13];
	
	if(need_per_acc_cover_driver == "Yes"){
		cal_rate = parseInt(cal_rate) + parseInt(cal_personal_accident);
		console.log("cal_rate_driver..."+cal_rate);
	}else{
		cal_rate = cal_rate;
	}
	
	//Rent A Car	
	cal_rent_a_car = valuecomp[15];	
	if(rent_a_car_val == "Yes"){
		cal_rate = parseInt(cal_rate) + parseInt(cal_rent_a_car);
		console.log("rent_a_car..."+cal_rate);
	}else{
		cal_rate = cal_rate;
	}	
	
	//Outside UAE Coverage	
	out_cov_val = valuecomp[17];	
	if(outside_uae_cov_val == "Yes"){
		cal_rate = parseInt(cal_rate) + parseInt(out_cov_val);
		console.log("outside_uae_cov..."+cal_rate);
	}else{
		cal_rate = cal_rate;
	}	
	
	//Years UAE Driv Licence age	
	if(years_uaedriving_lic == "l1year"){
		//console.log(valuecomp[8]);
		//console.log(cal_rate);										
		year_uae_driving_lic = valuecomp[18].replace ( /[^\d.]/g, '' );
		if(jQuery.isNumeric(year_uae_driving_lic)){
		year_uae_driving_licence_var = (cal_rate/100)*year_uae_driving_lic;
		cal_rate = parseInt(cal_rate) + parseInt(year_uae_driving_licence_var);
		console.log("cal_rate years_uaedriving_lic ..."+cal_rate);
		}else{
		cal_rate = cal_rate;
		}									
	}

	//if age less then 25 years	
	if(new Date(age_less_25) > new Date(ideal_date)){
		//console.log(valuecomp[8]);
		//console.log(cal_rate);										
		age_less_then25 = valuecomp[19].replace ( /[^\d.]/g, '' );
		if(jQuery.isNumeric(age_less_then25)){
		age_less_then25_var = (cal_rate/100)*age_less_then25;
		cal_rate = parseInt(cal_rate) + parseInt(age_less_then25_var);
		//console.log("age_less_then25 ..."+cal_rate);
		}else{
		cal_rate = cal_rate;
		}									
	}
									

	//Per Acc Cover Passengers		
	if(pa_cover_passangers >= 1){										
	
		if(pa_cover_passangers == 1){
			cal_rate = parseInt(cal_rate) + valuecomp[14];
			console.log("cal_rate_passanger1..."+cal_rate);
		}else if(pa_cover_passangers == 2){
			var passangers2 = valuecomp[14] + valuecomp[14];
			cal_rate = parseInt(cal_rate) + passangers2;
			console.log("cal_rate_passanger2..."+cal_rate);
		}else if(pa_cover_passangers == 3){
			var passangers2 = valuecomp[14] + valuecomp[14] + valuecomp[14];
			cal_rate = parseInt(cal_rate) + passangers2;
			console.log("cal_rate_passanger3..."+cal_rate);
		}else if(pa_cover_passangers == 4){
			var passangers2 = valuecomp[14] + valuecomp[14] + valuecomp[14] + valuecomp[14];
			cal_rate = parseInt(cal_rate) + passangers2;
			console.log("cal_rate_passanger4..."+cal_rate);
		}else if(pa_cover_passangers == 5){
			var passangers2 = valuecomp[14] + valuecomp[14] + valuecomp[14] + valuecomp[14] + valuecomp[14];
			cal_rate = parseInt(cal_rate) + passangers2;
			console.log("cal_rate_passanger5..."+cal_rate);
		}else if(pa_cover_passangers == 6){
			var passangers2 = valuecomp[14] + valuecomp[14] + valuecomp[14] + valuecomp[14] + valuecomp[14] + valuecomp[14];
			cal_rate = parseInt(cal_rate) + passangers2;
			console.log("cal_rate_passanger6..."+cal_rate);
		}else if(pa_cover_passangers == 7){
			var passangers2 = valuecomp[14] + valuecomp[14] + valuecomp[14] + valuecomp[14] + valuecomp[14] + valuecomp[14] + valuecomp[14];
			cal_rate = parseInt(cal_rate) + passangers2;
			console.log("cal_rate_passanger7..."+cal_rate);
		}else{
			cal_rate = cal_rate;
		}									
	}
	
	
	
/*	if(selected_driving_licence == true){
		//console.log(valuecomp[8]);
		//console.log(cal_rate);										
		cal_driving = valuecomp[18].replace ( /[^\d.]/g, '' );
		if(jQuery.isNumeric(cal_driving)){
		cal_rate_driving = (cal_rate/100)*cal_driving;
		cal_rate = parseInt(cal_rate) + parseInt(cal_rate_driving);
		//console.log("cal_rate..."+cal_rate);
		}else{
		cal_rate = cal_rate;
		}									
	}
	if(selected_outside_uae == true){
		//console.log(valuecomp[17]);
		//console.log(cal_rate);										
		cal_outside_uae = valuecomp[17].replace ( /[^\d.]/g, '' );
		if(jQuery.isNumeric(cal_outside_uae)){
		cal_rate = parseInt(cal_rate) + parseInt(cal_outside_uae);
		}else{
		cal_rate = cal_rate;
		}					
	}	
	if(selected_roadside_assistance == true){
		//console.log(valuecomp[12]);
		//console.log(cal_rate);										
		cal_roadside_assistance = valuecomp[20].replace ( /[^\d.]/g, '' );
		if(jQuery.isNumeric(cal_roadside_assistance)){
		cal_rate = parseInt(cal_rate) + parseInt(cal_roadside_assistance);
		}else{
		cal_rate = cal_rate;
		}									
	}
	if(selected_ins_rentACar == true){
		//console.log(valuecomp[15]);
		cal_ins_rentACar = valuecomp[15].replace ( /[^\d.]/g, '' );
		if(jQuery.isNumeric(cal_ins_rentACar)){
		cal_rate = parseInt(cal_rate) + parseInt(cal_ins_rentACar);
		}else{
		cal_rate = cal_rate;
		}									
	}
*/	
	var discount_apply = valuecomp[2].replace(/\%/g, "");
	
	/* 	var regexp = new RegExp(valuecomp[2], 'g');
	var discount_apply = jQuery('#example_id').replace(regexp,'');
    */	
	////console.log("discount_apply..."+discount_apply);
	var discount_apply2 = discount_apply/100;
	var discount_amount = discount_apply2 * cal_rate;
	var price_afterdeduct = cal_rate - discount_amount; 
	
	console.log("cal_rate bef disc.."+cal_rate);
	console.log("price_afterdeduct.."+price_afterdeduct);
	
	var len = valuecomp[6].length;
		all_companies.push({
		compname: valuecomp[0],
		discounts: valuecomp[2],
		ondamage: valuecomp[16],
		personal_accident_CFD: valuecomp[13],
		loss_of_pb: valuecomp[28],
		third_partylimit: valuecomp[21],
		accident_cforpassangers: valuecomp[14],
		offroad_desrec: valuecomp[24],
		windscreen_dmage: valuecomp[22],
		em_med_expence: valuecomp[25],
		amb_cover: valuecomp[26],
		rep_of_locks: valuecomp[31],
		v_parking_theft: valuecomp[30],
		car_reg_renewal: valuecomp[33],
		third_p_limit: valuecomp[21],
		death_bodyinjury: valuecomp[27],
		natural_clamity_cover: valuecomp[32],
		fire_and_theft: valuecomp[29],
		repair_dent: valuecomp[23],
		compclass: valuecomp[1],
		price_afterdisc: price_afterdeduct,
		price_beforedisc: cal_rate,
		gcc_spec: valuecomp[34],
		vehicle_cat: valuecomp[5],
		repair_cond: valuecomp[12],
		minimum_val: valuecomp[6],
		maximum_val: valuecomp[7],
		rate: valuecomp[8],       
		minimum: valuecomp[9],
		dl_less_than_year: valuecomp[18],
		less_than_year: valuecomp[19],
		outside_UAE_coverage: valuecomp[17],
		pa_driver: valuecomp[13],
		rs_assistance: valuecomp[20],
		rent_car: valuecomp[15],
		cart_btn: '<a value="'+cal_rate+'" data1="'+cal_rate+'#'+valuecomp[0]+'#'+valuecomp[1]+'#'+valuecomp[2]+'#'+valuecomp[3]+'#'+valuecomp[4]+'#'+valuecomp[5]+'#'+valuecomp[6]+'#'+valuecomp[7]+'#'+valuecomp[8]+'#'+valuecomp[9]+'#'+valuecomp[10]+'#'+valuecomp[11]+'#'+valuecomp[12]+'#'+valuecomp[13]+'#'+valuecomp[14]+'#'+valuecomp[15]+'#'+valuecomp[16]+'#'+valuecomp[17]+'#'+valuecomp[18]+'#'+valuecomp[19]+'#'+valuecomp[20]+'#'+valuecomp[21]+'#'+valuecomp[22]+'#'+valuecomp[23]+'#'+valuecomp[24]+'#'+valuecomp[25]+'#'+valuecomp[26]+'#'+valuecomp[27]+'#'+valuecomp[28]+'#'+valuecomp[29]+'#'+valuecomp[30]+'#'+valuecomp[31]+'#'+valuecomp[32]+'#'+valuecomp[33]+'#'+valuecomp[34]+'" class="pricing-tables-widget-purchase-button add-tocartbtn button_1" href="#">Buy Now</a>'
		});
	}
} 	  
}); 
return all_companies;
}

jQuery(".percentage").change(function() {
	
	jQuery('#priceingtable').html('');
	all_companies = get_companies();
	//console.log("all_companies..."+all_companies);

if(all_companies){
jQuery.each( all_companies, function(g, companies1) { 
	
	jQuery('#priceingtable').append('<div class="pricing-widget-wrapper mainrow"><div class="pricing-tables-widget pricing-tables-widget-default pricing-tables-widget-blue pricing-tables-widget-dark"><div class="grid-container"><div class="grid-row"><div class="grid-item item-lg-12 item-sm-6 item-xs-marginb30"><div class="grid-row grid-row-collapse"><div class="grid-item item-lg-3 item-xs-12 leftbar"><div class="pricing-tables-widget-header tablerighthead"><div id="companylogo"><p  id="companylogoinner"><span  class="'+companies1.compname.toString().toLowerCase()+'"></span></p></div><div class="pricing-tables-widget-purchase">'+companies1.compname+'</div><div id="compclass"><div class="pricing-tables-widget-purchase compclassname">'+companies1.compclass+'</div></div></div></div><div class="grid-item item-lg-6 item-xs-12"><div class="pricing-tables-widget-section"><div class="grid-row"><div class="grid-item item-lg-6 item-xs-12"><ul class="pricing-tables-widget-divider-res-section"><li><div class="pricing-tables-widget-tooltip pricing-tables-widget-top-left pricing-tables-widget-fixed" data-tooltip="Our Infrastructure is prepared for Instant Activation Services For a Better User Experience"></div><i class="icomoon-circle2 iconcircle"></i>Excess/Own-Damage Cover <span class="dataval"> '+companies1.ondamage+'</span></li><li><div class="pricing-tables-widget-tooltip pricing-tables-widget-top-left pricing-tables-widget-fixed" data-tooltip="With our Cloud Network you have Peace of Mind and can look at your business only"></div><i class="icomoon-circle2 iconcircle"></i><span class="switchlabel">Personal Accident Cover to Driver</span> <span class="datavalswitch">'+companies1.personal_accident_CFD+'</span><div class="smart-forms accoverswitch"><form method="post" action="" id="form-ui"><div class="form-body"><div class="frm-row"><div class="section colm colm4"><label class="switch block"><input type="checkbox" name="pac_driver'+g+'" id="pa_cover_driver_chekcbox'+g+'" value="javascript" /><span class="switch-label" data-on="ON" data-off="OFF"></span><span></span></label></div></div></div></form></div></li><li><div class="pricing-tables-widget-tooltip pricing-tables-widget-top-left pricing-tables-widget-fixed" data-tooltip="One piece of Software amazing and Beautiful organized with user in Mind"></div><i class="icomoon-circle2 iconcircle"></i>Car Repair in <span class="dataval"> '+companies1.repair_cond+'</span></li><li><div class="pricing-tables-widget-tooltip pricing-tables-widget-top-left pricing-tables-widget-fixed" data-tooltip="Our team is a group of nerds ready to give their best to assist you"></div><i class="icomoon-circle2 iconcircle"></i>Loss of Personal Belongings <span class="dataval"><i class="icomoon-checkmark-circle '+companies1.loss_of_pb.toString().toLowerCase()+'"></i></span></li></ul></div><div class="grid-item item-lg-6 item-xs-12"><ul><li><div class="pricing-tables-widget-tooltip pricing-tables-widget-top-left pricing-tables-widget-fixed" data-tooltip="Our Infrastructure is prepared for give good space in Services For a Better User Experience"></div><i class="icomoon-circle2 iconcircle"></i>Third_partylimit <span class="dataval">'+companies1.third_partylimit+'</span></li><li><div class="pricing-tables-widget-tooltip pricing-tables-widget-top-left pricing-tables-widget-fixed" data-tooltip="With our Cloud Network you have Peace of Mind and can look at your business only with this Bandwidth"></div><i class="icomoon-circle2 iconcircle"></i><span class="switchlabel">Accident Cover for Passengers</span><span class="datavalswitch"> '+companies1.accident_cforpassangers+'</span><div class="smart-forms accoverswitch"><form method="post" action="" id="form-ui"><div class="form-body"><div class=""><div><label class="switch block"><input type="checkbox" name="features" id="f1" value="javascript" checked><span class="switch-label" data-on="ON" data-off="OFF"></span></label></div></div></div></form></div></li><li><div class="pricing-tables-widget-tooltip pricing-tables-widget-top-left pricing-tables-widget-fixed" data-tooltip="In this Package you have 100 Emails per Month"></div><i class="icomoon-circle2 iconcircle"></i>Out Side UAE Coverage <span class="dataval">'+companies1.outside_UAE_coverage+'</span></li><li><div class="pricing-tables-widget-tooltip pricing-tables-widget-top-left pricing-tables-widget-fixed" data-tooltip="In this Package you have 100 Mysql Databases per Month"></div><i class="icomoon-circle2 iconcircle"></i>Off-Road & Desert Recovery <span class="dataval"><i class="icomoon-checkmark-circle '+companies1.offroad_desrec.toString().toLowerCase()+'"></i></span></li></ul></div></div><p class="pricing-tables-widget-purchase seemoredetails"><a href="#demo'+g+'" class="pricing-tables-widget-purchase seemoretoggle">see more details</a><i class="icomoon-menu-open2 seemoredetailsicon"></i></p> </div></div><div class="grid-item item-lg-3 item-xs-12"><div class="pricing-tables-widget-footer ptablefooter"><p class="pricing-tables-widget-top-left rightdiscount">Save '+companies1.discounts+'</p><h3 class="item-xs-marginb0"><strike>'+companies1.price_beforedisc+'</strike></h3><p>Total Price Are</p><h1 class="item-xs-marginb0"><span class="dataval1">'+companies1.price_afterdisc+'</span> AED</h1><div class="pricing-tables-widget-price">'+companies1.price_afterdisc+' AED</div>'+companies1.cart_btn+'</div></div></div></div></div><div id="demo'+g+'" class="" style="display:none;"><div class="pricing-tables-widget-section moredetail"><div class="grid-row"><div class="grid-item item-sm-4"><ul class="pricing-tables-widget-divider-res-section"><li><div class="pricing-tables-widget-tooltip pricing-tables-widget-top-left pricing-tables-widget-fixed" data-tooltip="Our Infrastructure is prepared for Instant Activation Services For a Better User Experience"></div><i class="icomoon-circle2 iconcircle"></i>Road Side Assistance <span class="dataval"><i class="icomoon-checkmark-circle '+companies1.rs_assistance.toString().toLowerCase()+'"></i></span></li><li><div class="pricing-tables-widget-tooltip pricing-tables-widget-top-left pricing-tables-widget-fixed" data-tooltip="With our Cloud Network you have Peace of Mind and can look at your business only"></div><i class="icomoon-circle2 iconcircle"></i>Windscreen Damage <span class="dataval">'+companies1.windscreen_dmage+'</span></li><li><div class="pricing-tables-widget-tooltip pricing-tables-widget-top-left pricing-tables-widget-fixed" data-tooltip="One piece of Software amazing and Beautiful organized with user in Mind"></div><i class="icomoon-circle2 iconcircle"></i>Dent Repair <span class="dataval"><i class="icomoon-checkmark-circle '+companies1.repair_dent.toString().toLowerCase()+'"></i></span></li><li><div class="pricing-tables-widget-tooltip pricing-tables-widget-top-left pricing-tables-widget-fixed" data-tooltip="Our team is a group of nerds ready to give their best to assist you"></div><i class="icomoon-circle2 iconcircle"></i>Loss of Personal Belongings <span class="dataval"><i class="icomoon-checkmark-circle '+companies1.loss_of_pb.toString().toLowerCase()+'"></i></span></li></ul></div><div class="grid-item item-sm-4"><ul><li><div class="pricing-tables-widget-tooltip pricing-tables-widget-top-left pricing-tables-widget-fixed" data-tooltip="Our Infrastructure is prepared for give good space in Services For a Better User Experience"></div><i class="icomoon-circle2 iconcircle"></i>Emergency Medical Expenses <span class="dataval">'+companies1.em_med_expence+'</span></li><li><div class="pricing-tables-widget-tooltip pricing-tables-widget-top-left pricing-tables-widget-fixed" data-tooltip="With our Cloud Network you have Peace of Mind and can look at your business only with this Bandwidth"></div><i class="icomoon-circle2 iconcircle"></i>Ambulance Cover <span class="dataval"><i class="icomoon-checkmark-circle '+companies1.amb_cover.toString().toLowerCase()+'"></i></span></li><li><div class="pricing-tables-widget-tooltip pricing-tables-widget-top-left pricing-tables-widget-fixed" data-tooltip="In this Package you have 100 Emails per Month"></div><i class="icomoon-circle2 iconcircle"></i>Death or Bodily Injury <span class="dataval"><i class="icomoon-checkmark-circle '+companies1.death_bodyinjury.toString().toLowerCase()+'"></i></span></li><li><div class="pricing-tables-widget-tooltip pricing-tables-widget-top-left pricing-tables-widget-fixed" data-tooltip="In this Package you have 100 Mysql Databases per Month"></div><i class="icomoon-circle2 iconcircle"></i>Car Registration / Renewal <span class="dataval"><i class="icomoon-checkmark-circle '+companies1.car_reg_renewal.toString().toLowerCase()+'"></i></span></li></ul></div><div class="grid-item item-sm-4"><ul><li><div class="pricing-tables-widget-tooltip pricing-tables-widget-top-left pricing-tables-widget-fixed" data-tooltip="Our Infrastructure is prepared for give good space in Services For a Better User Experience"></div><i class="icomoon-circle2 iconcircle"></i>Natural Calamity Cover <span class="dataval"><i class="icomoon-checkmark-circle '+companies1.natural_clamity_cover.toString().toLowerCase()+'"></i></span></li><li><div class="pricing-tables-widget-tooltip pricing-tables-widget-top-left pricing-tables-widget-fixed" data-tooltip="With our Cloud Network you have Peace of Mind and can look at your business only with this Bandwidth"></div><i class="icomoon-circle2 iconcircle"></i>Fire and Theft Cover <span class="dataval"><i class="icomoon-checkmark-circle '+companies1.fire_and_theft.toString().toLowerCase()+'"></i></span></li><li><div class="pricing-tables-widget-tooltip pricing-tables-widget-top-left pricing-tables-widget-fixed" data-tooltip="In this Package you have 100 Emails per Month"></div><i class="icomoon-circle2 iconcircle"></i>Valet Parking Theft <span class="dataval"><i class="icomoon-checkmark-circle '+companies1.v_parking_theft.toString().toLowerCase()+'"></i></span> </li><li><div class="pricing-tables-widget-tooltip pricing-tables-widget-top-left pricing-tables-widget-fixed" data-tooltip="In this Package you have 100 Mysql Databases per Month"></div><i class="icomoon-circle2 iconcircle"></i>Replacment of locks <span class="dataval"><i class="icomoon-checkmark-circle '+companies1.rep_of_locks.toString().toLowerCase()+'"></i></span> </li></ul></div></div> </div></div></div></div></div>');
});
}
});
});
</script>	
		<!-- 
		Price Table Excludes
		<p class="pricing-tables-widget-purchase seemoredetails">see more details<i class="icomoon-menu-open2 seemoredetailsicon"></i></p>
		<span class="pricing-tables-widget-bold">100GB</span>
		<span class="pricing-tables-widget-badges">New</span>
		-->
<script>	
//jQuery(document).ready(function() {

jQuery(document).on('click', '.seemoretoggle', function () {
			var content_toggle= jQuery(this).attr('href');                    
			jQuery(content_toggle).toggle();
});
		

				
jQuery(document).on('click', '.add-tocartbtn', function () {
// your function here
var alldataclicked = jQuery(this).attr('data1');
//console.log("alldatacheck.."+alldataclicked);

var dataSelected = alldataclicked.split('#');
var price_final = dataSelected[0];
//console.log("price_final.."+price_final);

//Getting Selected Company and let it go 

var selected_compny_data = dataSelected;
var price = 	dataSelected[0];
var compname =  dataSelected[1];
var gcc_spec =  dataSelected[2];
var vehicle_cat = dataSelected[3];
var repair_cond = dataSelected[12];
var minimum_val = dataSelected[5];
var maximum_val = dataSelected[6];
var rate = dataSelected[7];
var minimum = dataSelected[8];
var dl_less_than_year = dataSelected[9];
var less_than_year = dataSelected[10];
var os_coverage = dataSelected[11];
var pa_cover_driver = dataSelected[13];
var pa_cover_passanger = dataSelected[14];
var rs_assistance = dataSelected[14];
var rent_car = dataSelected[15];
var excess_amount = dataSelected[16];	
var third_prty_limit = dataSelected[21];	
var em_med_expence2 = dataSelected[25];	
var em_loss_of_per_belongings = dataSelected[28];	
//console.log("pa_cover_driver.."+pa_cover_driver);

//User Details			
var veh_email_address = jQuery('#email_address').val();	
var veh_vehicle_brand = jQuery('#vehicle_brand').val();	
var veh_model_year = jQuery('#model_year').val();	
var veh_fullname = jQuery('#firstname').val();		
var veh_dobirth = jQuery('#datepicker1').val();		
var veh_city_registered = jQuery('#nationality').val();	
var veh_country_first_driving_lic = jQuery('#country_firstdl').val();	
var veh_brandnew = jQuery('#fr3').val();	
var veh_manyear1 = jQuery('#dor_date').val();	
var veh_manyear2 = jQuery('#dor_month').val();	
var veh_manyear3 = jQuery('#dor_year').val();	
var veh_manufactured_year = veh_manyear1+'/'+veh_manyear2+'/'+veh_manyear3;
var veh_contact_num1 = jQuery('#contact_num1').val();	
var veh_contact_num2 = jQuery('#contact_num2').val();	
var veh_contact_numF = veh_contact_num1+veh_contact_num2;
var veh_model_detail = jQuery('#vehicle_model_detail').val();
var veh_reg_date1 = jQuery('#vehicle_reg_date1').val();
var veh_reg_date2 = jQuery('#vehicle_reg_date2').val();
var veh_reg_date3 = jQuery('#vehicle_reg_date3').val();
var veh_reg_dateF = veh_reg_date1+'/'+veh_reg_date2+'/'+veh_reg_date3;
var veh_city_ofreg = jQuery('#cityof_reg').val();
var veh_car_val = jQuery('#vehicle_val').val();
var veh_policystart_date1 = jQuery('#vec_policy_date1').val();
var veh_policystart_date2 = jQuery('#vec_policy_date2').val();
var veh_policystart_date3 = jQuery('#vec_policy_date3').val();
var veh_policy_sdateF = veh_policystart_date1+'/'+veh_policystart_date2+'/'+veh_policystart_date3;
var veh_country_first_dl = jQuery('#country_firstdl').val();
				
	//Select Options
/* 	jQuery('.veh_brand_new').click(function () {
		var veh_brandnewcheck = jQuery(this).val();		
		var current_ins_exp = jQuery(this).val();		
		var current_policy_comp_ins = jQuery(this).val();		
		var cur_policy_agency_repair = jQuery(this).val();		
		var vehicle_is_gcc_spec = jQuery(this).val();		
		var not_for_commercial_purpose = jQuery(this).val();		
		var reg_under_myname = jQuery(this).val();		
		var claim_in_last12months = jQuery(this).val();		
		var excess_amountpay = jQuery(this).val();		
		var need_veh_replacement = jQuery(this).val();		
		var need_per_acc_cover = jQuery(this).val();		
		var need_out_uae_cover = jQuery(this).val();		
	}); */
				
	

var data = {	
'action': 			'my_ajax_rt',
'ins_price' : 	price,
'ins_compname' :  compname,
'ins_discount' :  gcc_spec,
'ins_vehicle_cat' : vehicle_cat,
'ins_repair_cond' : repair_cond,
'ins_minimum_val' : minimum_val,
'ins_maximum_val' : maximum_val,
'ins_rate' : rate,
'ins_minimum' : minimum,
'ins_dl_less_than_year' : dl_less_than_year,
'ins_less_than_year' : less_than_year,
'ins_os_coverage' : os_coverage,
'ins_pa_cover_driver' : pa_cover_driver,
'ins_cover_passanger' : pa_cover_passanger,
'ins_rs_assistance' : rs_assistance,
'ins_rent_car' : rent_car,
'ins_excess_amount' : excess_amount,
'ins_third_prty_limit' : third_prty_limit,
'ins_emergency_med_exp' : em_med_expence2,
'ins_loss_of_per_belongings' : em_loss_of_per_belongings,

'ins_fullname' : veh_fullname,									
'ins_vehicle_brand' : veh_vehicle_brand,									
'ins_veh_model_year' : veh_model_year,									
'ins_contact_num' : veh_contact_numF,									
'ins_veh_man_year' : veh_manufactured_year,									
'ins_emailaddress' : veh_email_address,									
'ins_regcity' : veh_city_registered,									
'ins_brand_new' : veh_brandnew,									
'ins_model_detail' : veh_model_detail,									
'ins_vehiclereg_date' : veh_reg_dateF,									
'ins_cityof_reg' : veh_city_ofreg,									
'ins_car_value' : veh_car_val,									
'ins_policy_startdate' : veh_policy_sdateF,								
'ins_country_first_dl' : veh_country_first_dl,								
'ins_cntry_fir_driv_lic' : veh_country_first_driving_lic,									
'ins_dob_date' : veh_dobirth,									
'ins_selected_compny_data' : selected_compny_data									

/* 'veh_brandnewcheck' : veh_brandnewcheck,									
'current_ins_exp' : current_ins_exp,									
'current_policy_comp_ins' : current_policy_comp_ins,									
'cur_policy_agency_repair' : cur_policy_agency_repair,									
'vehicle_is_gcc_spec' : vehicle_is_gcc_spec,									
'not_for_commercial_purpose' : not_for_commercial_purpose,									
'reg_under_myname' : reg_under_myname,									
'claim_in_last12months' : claim_in_last12months,									
'excess_amountpay' : excess_amountpay,									
'need_veh_replacement' : need_veh_replacement,									
'need_per_acc_cover' : need_per_acc_cover,									
'need_out_uae_cover' : need_out_uae_cover */									
																							
};
var ajax_url_custom = "http://insurancetopup.com/wp-admin/admin-ajax.php";
jQuery.post(ajax_url_custom, data,   function(response) {
console.log(response);					
});



showEstimatedFareHtml(price_final);
document.getElementById("stern_taxi_fare_estimated_fare").value =  price_final;
localStorage.setItem("finalPrice", price_final);
checkout_url_function(); 
});
		
</script>			

<!-- Old Calculater -->

<form  id="stern_taxi_fare_div" method="post">
<div  style="@display:none;">
<input type="hidden"  name="stern_taxi_fare_estimated_fare" id="stern_taxi_fare_estimated_fare" value=""/><div class="row">
</div>								
</div>											
</form>
</div>

<div class="col-lg-3 form_desc" >

<div class="desc_icon">
<img class="form_descicon" src="http://insurancetopup.com//xio_outww2/insurance-quote-icon.png">
</div>

<p class="form_descright">Can't find what you need? Not sure about something? Please Contact us at on:</p>

<p class="form_descright"><a href="mailto:sales@insurancetopup.net">sales@insurancetopup.net</a>, or Call Us at<a href="+97155123456">+97155123456</a> <br>We'll be able to pull up your quote and help you get the coverage that's right for you</p>

<div class="progress_indicator">
<div class="big c100 " id="progress">
<span id="percentage-text">0%</span>
<span id="complete">Info Complete</span>
<div class="slice">
<div class="bar"></div>
<div class="fill"></div>
</div>
<div class="whitecircle"></div>
</div>
</div>				
</div>
</div>
<script type="text/javascript" src="http://doptiq.com/smart-forms/demos/samples/elegant/js/jquery.formShowHide.min.js"></script> 

 <script type="text/javascript">
	
	jQuery(document).ready(function($){
	jQuery(function(){ 
		jQuery('.smartfm-ctrl').formShowHide(); 
	});
		
		
	/* @normal masking rules 
	---------------------------------------------------------- */
	$.mask.definitions['f'] = "[A-Fa-f0-9]"; 	
	$("#contact_num2").mask('9999999', {placeholder:'X'});			

	//Showpopups on Select.
	jQuery('#rent_a_car_id input').on('change', function() {
	   var rent_a_car_val = jQuery('input[name=veh_replacement_chk]:checked').val();
	   console.log("rent_a_car_val.."+rent_a_car_val);
	   if(rent_a_car_val == 'Yes' ){
		   alert(" You Have choosed rental car option. So... 200 AED will be added ");
		   //console.log("Rent Car Yes");
	   }
	});	
	jQuery('#my_current_ins_not_exp input').on('change', function() {
	   var current_ins_notexp_val = jQuery('input[name=mycurrentins_chk]:checked').val();
	   if(current_ins_notexp_val == 'Yes' ){
		   alert(" You need to provide us Expired Documents.");
		   //console.log("Rent Car Yes");
	   }
	});		  	
	
	//Model
 	//.ui-state-default
/* 	jQuery('[data-smartmodal-close]').on('click', function(e)  {
				e.preventDefault();
				var smartInactiveModal = jQuery(this).attr('data-smartmodal-close');
				jQuery(smartInactiveModal).removeClass('smartforms-modal-visible');
				jQuery('body').removeClass('smartforms-modal-scroll');
			});	
	});  */
	
	
    </script>

	
	
<!-- Model Start -->
	<div class="smartforms-px smart-forms">
    	<a href="#" data-smart-modal="#smart-modal1" class="smartforms-modal-trigger">Basic Modal</a>
    </div>
    
    <div id="smart-modal1" class="smartforms-modal" role="alert">
        <div class="smartforms-modal-container">
            <div class="smartforms-modal-header">
                <h3>Contact Form</h3>
                <a href="#" class="smartforms-modal-close">&times;</a>            
            </div><!-- .smartforms-modal-header -->
            <div class="smartforms-modal-body">
                <div class="smart-wrap">
                    <div class="smart-forms smart-container wrap-full">
                        <div class="form-body">
                            <form method="post" action="" id="smart-form">
                                <div class="frm-row">
                                    <div class="section colm colm6">
                                        <label class="field prepend-icon">
                                            <input type="text" name="firstname" id="firstname" class="gui-input" placeholder="First name">
                                            <span class="field-icon"><i class="fa fa-user"></i></span>  
                                        </label>
                                    </div><!-- end section -->
                                    <div class="section colm colm6">
                                        <label class="field prepend-icon">
                                            <input type="text" name="lastname" id="lastname" class="gui-input" placeholder="Last name">
                                            <span class="field-icon"><i class="fa fa-user"></i></span>  
                                        </label>
                                    </div><!-- end section -->
                                </div><!-- end .frm-row section -->
                                <div class="frm-row">
                                    <div class="section colm colm6">
                                        <label class="field prepend-icon">
                                            <input type="email" name="emailaddress" id="emailaddress" class="gui-input" placeholder="Email address">
                                            <span class="field-icon"><i class="fa fa-envelope"></i></span>  
                                        </label>
                                    </div><!-- end section -->
                                    <div class="section colm colm6">
                                        <label class="field prepend-icon">
                                            <input type="tel" name="telephone" id="telephone" class="gui-input" placeholder="Telephone number">
                                            <span class="field-icon"><i class="fa fa-phone-square"></i></span>  
                                        </label>
                                    </div><!-- end section -->
                                </div><!-- end .frm-row section -->
                                <div class="frm-row">
                                    <div class="section colm colm6">
                                        <label class="field prepend-icon">
                                            <input type="text" name="address" id="address" class="gui-input" placeholder="Physical address">
                                            <span class="field-icon"><i class="fa fa-map-marker"></i></span>  
                                        </label>
                                    </div><!-- end section -->
                                    <div class="section colm colm6">
                                        <label class="field select">
                                            <select name="departments" id="departments">
                                                <option value="">Select Department</option>
                                                <option value="Sales">Sales and Marketing</option>
                                                <option value="Techical">Techical</option>
                                                <option value="Management">Management</option>
                                                <option value="ICT">ICT</option>
                                            </select>
                                            <i class="arrow double"></i>
                                        </label>
                                    </div><!-- end section -->
                                </div><!-- end .frm-row section -->
                                <div class="section">
                                    <label class="field prepend-icon">
                                        <textarea class="gui-textarea" id="comment" name="comment" placeholder="Your comment"></textarea>
                                        <span class="field-icon"><i class="fa fa-comments"></i></span>
                                        <span class="input-hint"> 
                                            <strong>DO NOT:</strong> Be negative or off topic, we expect a great comment... 
                                        </span>   
                                    </label>
                                </div><!-- end section -->
                                <div class="smartforms-modal-footer">
                                    <button type="submit" class="button btn-primary"> Send Form </button>
                                    <button type="button" data-smartmodal-close="#smart-modal1" class="button"> Cancel </button>
                                </div><!-- end .form-footer section -->                                                          
                            </form>                                                                                   
                        </div><!-- end .form-body section -->
                    </div><!-- end .smart-forms section -->
                </div><!-- end .smart-wrap section -->            
            </div><!-- .smartforms-modal-body -->
        </div><!-- .smartforms-modal-container -->
    </div><!-- .smartforms-modal 1 -->


<!-- Model End -->


<!-- Pricing Table -->

		<!-- FONT LINK -->
        <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Open+Sans:400,400italic,600,600italic,700,700italic,800,800italic|Source+Sans+Pro:400,300,300italic,400italic,600,600italic,700,700italic,900,900italic|Alegreya+Sans:400,300,300italic,400italic,500,500italic,700,700italic,800,800italic,900,900italic|Poiret+One|Dosis:400,500,300,600,700,800|Lobster+Two:400,400italic,700,700italic|Raleway:400,300,300italic,400italic,500,500italic,600,600italic,700,700italic,800,800italic,900,900italic|Roboto+Condensed:400,300italic,300,400italic,700,700italic|Roboto:400,300,300italic,400italic,500,500italic,700,700italic,900,900italic|Courgette">
	
	<div id="step4">		
		<div id="priceingtable">
		</div>
		<div class="smart-forms accoverswitch" style="display:none;"><form method="post" action="" id="form-ui"><div class="form-body"><div class="frm-row"><div class="section colm colm4"><label class="switch block"><input type="checkbox" id="checkAll" name="pac_driver'+g+'" value="javascript"><span class="switch-label" data-on="ON" data-off="OFF"></span><span>J</span></label></div></div></div></form></div>
	</div>
	
<?php

}
