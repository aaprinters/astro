<?php

if ( ! defined( 'ABSPATH' ) )
	exit;

/**
 * Event admin
 */
class stern_taxi_fare_events_Admin {

	/**
	 * Constructor
	 */
	public function __construct() {

		add_action('admin_menu', array( $this,'register_stern_taxi_fare') );		
		//add_action('admin_menu', array( $this,'register_submenu_design') );		
		add_action('admin_menu', array( $this,'register_submenu_listAddress') );		
	}
	
	
/* 	public function create_post_type_car($cartype,$carfare,$carseat,$suitcases){
		$userID = 1;
		if(get_current_user_id()){
			$userID = get_current_user_id();
		}
		$post = array(
			'post_author' => $userID,
			'post_content' => '',
			'post_status' => 'publish',
			'post_title' => 'stern_taxi_car_type',
			'post_type' => 'stern_taxi_car_type',
		);

		$post_id = wp_insert_post($post);  
		update_post_meta($post_id, '_stern_taxi_car_type_cartype', $cartype);
		update_post_meta($post_id, '_stern_taxi_car_type_carfare', $carfare);
		update_post_meta($post_id, '_stern_taxi_car_type_carseat', $carseat);
		update_post_meta($post_id, '_stern_taxi_car_type_suitcases', $suitcases);
		
		
		return $post_id;
	}	 */

	public function register_stern_taxi_fare(){
		add_menu_page( 'Insurance Quote', 'Hair Wig', 'manage_options', 'WigPage', array( $this,'menu_page_stern_taxi_fare'), plugins_url("img/", dirname(__FILE__)).'stern_taxi_fare.png', 6 ); 
	}
	
	
	function register_submenu_listAddress() {
		add_submenu_page( 'WigPage', __('Wig Rates', 'stern_taxi_fare'),  __('Wig Rates', 'stern_taxi_fare'), 'manage_options', 'wig-rates', array( $this,'my_custom_submenu_page_full_vehicle_info')  );
		//add_submenu_page( 'InsurancePage', __('Coverage Data', 'stern_taxi_fare'),  __('Coverage Data', 'stern_taxi_fare'), 'manage_options', 'full-Coverage-data', array( $this,'my_custom_companies_page_frontend_data')  );
		//add_submenu_page( 'InsurancePage', __('TPL Vehicle Info', 'stern_taxi_fare'),  __('TPL Vehicle Info', 'stern_taxi_fare'), 'manage_options', 'tpl-vehicle-data', array( $this,'my_custom_tpl_page_frontend_data')  );
		//add_submenu_page( 'InsurancePage', __('TPL Coverage', 'stern_taxi_fare'),  __('TPL Coverage', 'stern_taxi_fare'), 'manage_options', 'tpl-coverage', array( $this,'my_custom_tpl_page_coverage_data')  );
	}	
	
	
	public function my_custom_submenu_page_callback_calendar(){
		if (($_SERVER['REQUEST_METHOD'] == 'POST') && isset($_POST['SternSaveSettingsCalendarTableSubmit']) ) {			
			updateOptionsSternTaxiFare();
		}
		
		if (($_SERVER['REQUEST_METHOD'] == 'POST') && isset($_POST['calendarSubmit']) ) {
			if( $_POST['dateTimeBegin']!=null && $_POST['dateTimeEnd']!=null  && $_POST['typeCalendar']!=null ) {
				
				$oCalendar = new calendar();
				$oCalendar->settypeIdCar(sanitize_text_field($_POST['typeIdCar']));
				$oCalendar->settypeCalendar(sanitize_text_field($_POST['typeCalendar']));
				$oCalendar->setisRepeat(sanitize_text_field($_POST['isRepeat']));

				
			//	$oCalendar->setdateEnd(sanitize_text_field($_POST['dateEnd']));
			//	$oCalendar->setdateBegin(sanitize_text_field($_POST['dateBegin']));
			
				$date1=date_create($_POST['dateBegin'] . " " . $_POST['dateTimeBegin']);
				$date1=date_format($date1,"Y/m/d g:i A");			
				$oCalendar->setdateTimeBegin(sanitize_text_field($date1));
				
				$date2=date_create($_POST['dateEnd'] . " " . $_POST['dateTimeEnd']);
				$date2=date_format($date2,"Y/m/d g:i A");				
				$oCalendar->setdateTimeEnd(sanitize_text_field($date2));
				
				
				if($date1<$date2) {
					$oCalendar->save();	
				} else {
					echo 'Date Begin is  greater than date End!';
				}
					
				
				
				
			}

			// Delete
			$args = array(
				'post_type' => 'stern_taxi_calendar',
				'posts_per_page' => 200,
			);

			$allPosts = get_posts( $args );			
			foreach ( $allPosts as $post ) {
			setup_postdata( $post );			
				if (isset($_POST['remove'.$post->ID])) {						
					if ($_POST['remove'.$post->ID] =='yes') {
						$oCalendar = new calendar($post->ID);
						$oCalendar->delete();
					}				
				}
			}
		}
		new templateCalendar("600px","300px",true);
	}	

	
		public function my_custom_submenu_page_full_vehicle_info(){
		
		global $redux_demo;  // This is your opt_name.
		?><pre><?php print_r ($redux_demo); ?> </pre><?php
			
		}		
				
		
	public function my_custom_submenu_page_callback_list_addresses(){
		if (($_SERVER['REQUEST_METHOD'] == 'POST') && isset($_POST['SettingsTemplateListAddressSubmit']) ) {
			updateOptionsSternTaxiFare();			
		}			
		if (($_SERVER['REQUEST_METHOD'] == 'POST') && isset($_POST['listAddressSubmit']) ) {
			if( $_POST['typeListAddress']!=null && $_POST['address']!=null  ) {
				$oListAddress = new listAddress();
				$oListAddress->setisActive(sanitize_text_field($_POST['isActive']));
				$oListAddress->settypeListAddress(sanitize_text_field($_POST['typeListAddress']));				
				$oListAddress->setaddress(sanitize_text_field($_POST['address']));
				$oListAddress->save();	
			} else {
				$args = array(
				'post_type' => 'stern_listAddress',
				'nopaging' => true,
				);

				$allPosts = get_posts( $args );			
				foreach ( $allPosts as $post ) {
					setup_postdata( $post );
					$oListAddress = new listAddress($post->ID);			
					if (isset($_POST['remove'.$post->ID])) {						
						$oListAddress->delete();
					}
					if (isset($_POST['isActive'.$post->ID])) {									
						$oListAddress->setisActive("true");
						$oListAddress->save();
					} else {
						$oListAddress->setisActive("false");
						$oListAddress->save();
					}		
				
				}
			}
			
		}		
		new templateListAddress("600px","300px",true);
	}
		
	public function my_custom_submenu_page_callback_rule(){
		
		if (($_SERVER['REQUEST_METHOD'] == 'POST') && isset($_POST['SettingsPricingRulesSubmit']) ) {
			updateOptionsSternTaxiFare();			
		}
	
		
		if (($_SERVER['REQUEST_METHOD'] == 'POST') && isset($_POST['ruleSubmit']) ) {
			if( $_POST['price']!=null && $_POST['nameRule']!=null  ) {				
				$oRule = new rule();	
				$oRule->setisActive(sanitize_text_field($_POST['isActive']));
				$oRule->setnameRule(sanitize_text_field($_POST['nameRule']));				
				$oRule->settypeSource(sanitize_text_field($_POST['typeSource']));
				$oRule->settypeSourceValue(sanitize_text_field($_POST['typeSourceValue']));
				$oRule->settypeDestination(sanitize_text_field($_POST['typeDestination']));
				$oRule->settypeDestinationValue(sanitize_text_field($_POST['typeDestinationValue']));
				$oRule->settypeIdCar(sanitize_text_field($_POST['typeIdCar']));
				$oRule->setprice(sanitize_text_field($_POST['price']));	
				$oRule->save();				
			}

			// Delete
			
		if(isset($_GET["paged"])) {
			$paged = $_GET["paged"];
		} else {
			$paged = 1;
		}
			//$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
			
		if(get_option('stern_taxi_fare_nb_post_to_show')=="") {
			$posts_per_page = 10;
		} else {
			$posts_per_page = get_option('stern_taxi_fare_nb_post_to_show');
		}
		$query = getQueryRule($paged, $posts_per_page );			
			
			while ( $query->have_posts() ) : $query->the_post(); 
				$oRule = new rule(get_the_ID());
				if (isset($_POST['remove'.get_the_ID()])) {				
					$oRule->delete();
					
				} 
				
				if (isset($_POST['isActive'.get_the_ID()])) {						
					$oRule->setisActive("true");
					$oRule->save();
					
				} else {
					$oRule->setisActive("false");
					$oRule->save();
					
				}
			endwhile;
						
			
			/*
			$args = array(
			'post_type' => 'stern_taxi_rule',
			'nopaging' => true,
			);

			$allPosts = get_posts( $args );			
			foreach ( $allPosts as $post ) {
				setup_postdata( $post );
				$oRule = new rule($post->ID);			
				if (isset($_POST['remove'.$post->ID])) {						
					$oRule->delete();
					
				} 
				
				if (isset($_POST['isActive'.$post->ID])) {						
					$oRule->setisActive("true");
					$oRule->save();
					
				} else {
					$oRule->setisActive("false");
					$oRule->save();
					
				}
				
				
			
			}
			*/
		}
		if (($_SERVER['REQUEST_METHOD'] == 'POST') && isset($_POST['deleteAllRules']) ) {
			
			$args = array(
			'post_type' => 'stern_taxi_rule',
			'nopaging' => true,
			);

			$allPosts = get_posts( $args );			
			foreach ( $allPosts as $post ) {
				setup_postdata( $post );
				$oRule = new rule($post->ID);			
										
				$oRule->delete();			
			}			
			
		}
	//	var_dump( $bulkData);
	//	var_dump($parts);
		new templateRule("600px","300px",true);
	}

	//true	lille-marseille2	city	Lille, France	city	Marseille, France	All	9
			
	public function my_custom_submenu_page_callback_design(){
		if (($_SERVER['REQUEST_METHOD'] == 'POST') && isset($_POST['SternSaveSettings']) ) 
        {		
			updateOptionsSternTaxiFare();
		}
		
		new design("600px","300px",true);
	}
	

	public function menu_page_stern_taxi_fare(){
		
		
		?>
		
		<div id="content">
		 <h2>Enter this shortcode to view form [insurance-qoute-system]</h2>
		 <h2>Enter this shortcode to view TPL form [insurance-qoute-system-tpl]</h2>		
		</div>
		
		<?php
		
		if (($_SERVER['REQUEST_METHOD'] == 'POST') && isset($_POST['SternSaveSettings']) ) 
        {		
			updateOptionsSternTaxiFare();
			saveVersion();
			sendInfosDebug();

		}
		
		if (($_SERVER['REQUEST_METHOD'] == 'POST') && isset($_POST['initVal']) ) 
        {

			
			$beginNameOption = "stern_taxi_fare%";
			global $wpdb;
			$wpdb->query($wpdb->prepare("DELETE FROM $wpdb->options WHERE option_name like %s",$beginNameOption));
		
						
					
		}
		if (($_SERVER['REQUEST_METHOD'] == 'POST') && isset($_POST['createProduct']) ) 
        {
			createProductAndSaveId();			
			
		}		
		
		
		new settings("600px","300px",true);
	}




}

new stern_taxi_fare_events_Admin();