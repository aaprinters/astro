<?php

/*
 Plugin Name: Wp Astrology
 Description: Astrology System
 Version: 1.0.0
 Plugin URI: http://logicsbuffer.com/
 Author: https://www.fiverr.com/wpright
*/

add_action('wp_enqueue_scripts', 'stern_taxi_fares_script_front_css');
add_action('wp_enqueue_scripts', 'stern_taxi_fares_script_front_js');
add_action('admin_enqueue_scripts', 'stern_taxi_fares_script_back_css');
add_filter( 'plugin_action_links_' . plugin_basename( __FILE__ ), 'ts_add_plugin_action_links' );
add_action('init', 'wp_astro_init');
include 'templates/admin/settings.php';
require_once( dirname( __FILE__ ) . '/ReduxFramework/sample/wig-config.php' );
function wp_astro_init() {
   add_shortcode('show_astology_form', 'wp_astology_form');
}
function get_zodiac_by_dob($day, $month)
{
/* $month = 04;
$day =12; */
/* Capricorn
December 22 –January 19
Aquarius
January 20 –February 18
Pisces
February 19 –March 20
Aries
March 21 – April 19
Taurus
April 20 – May 20
Gemini
May 21 – June 20
Cancer
June 21 – July 22
Leo
July 23 – August 22
Virgo
August 23 – September 22
Libra
September 23 – October 22
Scorpio
October 23 –November 21
Sagittarius
November 22 –December 21 */
//$month = '09';
//$day =12;
$zod_signs = array('Capricorn','Aquarius', 'Pisces', 'Aries', 'Taurus', 'Gemini', 'Cancer', 'Leo', 'Virgo', 'Libra', 'Scorpio', 'Sagittarius', 'Capricorn');
switch($month)
{
	case '01': {//January
			 if($day < 20)
		 		$zodiacSign = $zod_signs[0]; // Capricorn
			 else
		 		$zodiacSign = $zod_signs[1]; // Aquarius
		    }break;
	case '02': {//February
			 if($day < 19)
		 		$zodiacSign = $zod_signs[1]; // Aquarius
			 else
		 		$zodiacSign = $zod_signs[2]; // Pisces
			}break;
	case '03': {//March
			 if($day < 21)
			 	$zodiacSign = $zod_signs[2]; // Pisces
			 else
			 	$zodiacSign = $zod_signs[3]; // Aries
			}break;
	case '04': {//April
			 if($day < 20)
		 		$zodiacSign = $zod_signs[3]; // Aries
			 else
		 		$zodiacSign = $zod_signs[4]; // Taurus
			}break;
	case '05': {//May
			 if($day < 19)
		 		$zodiacSign = $zod_signs[4]; // Taurus
			 else
		 		$zodiacSign = $zod_signs[5]; //Gemini
			}break;
	case '06': {//June
			 if($day < 21)
		 		$zodiacSign = $zod_signs[5]; //Gemini
			 else
		 		$zodiacSign = $zod_signs[6]; //Cancer
			}break;
	case '07': {//July
			 if($day < 23)
		 		$zodiacSign = $zod_signs[6]; //Cancer
			 else
		 		$zodiacSign = $zod_signs[7]; //Leo
			}break;
 	case '08': {//August
			 if($day < 23)
		 		$zodiacSign = $zod_signs[7]; //Leo
			 else
		 		$zodiacSign = $zod_signs[8]; //Virgo
			}break;
	case '09':{//September
		if($day < 23)
		$zodiacSign = $zod_signs[8]; //Virgo
		else
		$zodiacSign = $zod_signs[9]; //Libra
			}break;
	case '10': {//October
			 if($day < 23)
		 		$zodiacSign = $zod_signs[9]; //Libra
			 else
		 		$zodiacSign = $zod_signs[10]; //Scorpio
			}break;
	case '11': {//November
			 if($day < 22)
		 		$zodiacSign = $zod_signs[10]; //Scorpio
			 else
		 		$zodiacSign = $zod_signs[11]; //Sagittarius
			}break;
	case '12': {//December
			 if($day < 22)
		 		$zodiacSign = $zod_signs[11]; //Sagittarius
			 else
		 		$zodiacSign = $zod_signs[12]; //
			}break; 
 }
$zod_signs_file = plugins_url('img/astrology/',__FILE__);
$zod_signs_url = $zod_signs_file.$zodiacSign.'.png';
//print_r($zod_signs_url);

 $zodiacSign_info = array ( 
					"zodiacSign" => $zodiacSign,
				   "discription" => "Example",
				   "zodic_icon" => $zod_signs_url
				   );

 return $zodiacSign_info;
}

function ConverToRoman($num){ 
    $n = intval($num); 
    $res = ''; 

    //array of roman numbers
    $romanNumber_Array = array( 
        'M'  => 1000, 
        'CM' => 900, 
        'D'  => 500, 
        'CD' => 400, 
        'C'  => 100, 
        'XC' => 90, 
        'L'  => 50, 
        'XL' => 40, 
        'X'  => 10, 
        'IX' => 9, 
        'V'  => 5, 
        'IV' => 4, 
        'I'  => 1); 

    foreach ($romanNumber_Array as $roman => $number){ 
        //divide to get  matches
        $matches = intval($n / $number); 

        //assign the roman char * $matches
        $res .= str_repeat($roman, $matches); 

        //substract from the number
        $n = $n % $number; 
    } 

    // return the result
    return $res; 
} 

// this switches the date from USA to AU date format but do not need year
function get_planet_by_life_number($life_number) {
$life_number_arr = str_split($life_number);
		foreach($life_number_arr as $life_number){
			$planet += $life_number;
		}
		$palent_arr = array("none","sun","mercury","venus","earth","moon","mars","jupiter","saturn","uranus","neptune","pluto","gods","satans");
		foreach($palent_arr as $palent_number=>$planet_name){
			if($planet == $palent_number){
				$user_palent_number = $palent_number;
				$user_planet = $planet_name;
			}
		}
$planets_file = plugins_url('img/planets/',__FILE__);
$planet_url = $planets_file.$user_planet.'.png';
//print_r($planets_file_url);

 $planets_info = array ( "planet_name" => $user_planet,
 "palent_number" => $user_palent_number,
				   "discription" => "Test Example",
				   "planet_icon" => $planet_url
				   );

 return $planets_info;
}

function get_life_number_by_dob($user_dob_arr) {
foreach($user_dob_arr as $user_dob){
	$life_number += $user_dob;
}
//print_r($life_number);
$roman_number = ConverToRoman($life_number);
$life_number_file = plugins_url('img/planets/',__FILE__);

$life_icon = $life_number_file.$life_number.'.png';
$life_number_info = array ( 
				"life_number" => $life_number,
				"life_number_roman" => $roman_number,
			   "discription" => "Test Example",
			   "life_icon" => $life_icon
			   );

 return $life_number_info;
}

function get_element_by_planet_number($planet_number) {
/* 1	Hydrogen
2	Helium
3	Lithium	
4	Beryllium	
5	Boron
6	Carbon	
7	Nitrogen
8	Oxygen
9	Fluorine
10	Neon	
11	Sodium	
12	Magnesium	
13	Aluminum	
14	Silicon	
15	Phosphorus	
16	Sulfur */
//$life_number_arr = str_split($planet_number);
		$element_arr = array("none","Hydrogen","Helium","Lithium","Beryllium","Boron","Carbon","Nitrogen","Oxygen","Fluorine","Neon","Sodium","Magnesium","Aluminum","Silicon","Phosphorus","Sulfur");
		foreach($element_arr as $element_number=>$element_name){
			if($planet_number == $element_number){
				$user_element_name = $element_name;
				$user_element_number = $element_number;
			}
		}
$element_file = plugins_url('img/tabel_element/',__FILE__);
$element_url = $element_file.$user_element_name.'.png';
//print_r($element_url);
 $element_info = array ( 
 "element_name" => $user_element_name,
 "element_number" => $user_element_number,
				   "discription" => $step1_price,
				   "element_icon" => $element_url
				   );

 return $element_info;
}
function get_atom_by_planet_number($planet_number) {
/* 1	Hydrogen H
2	Helium He
3	Lithium	Li
4	Beryllium Be
5	Boron B
6	Carbon C
7	Nitrogen N
8	Oxygen O
9	Fluorine F
10	Neon	Ne
11	Sodium	Na
12	Magnesium	Mg
13	Aluminum	Al
14	Silicon	Si
15	Phosphorus	P
16	Sulfur  S */
//$life_number_arr = str_split($planet_number);
		//$atom_arr = array("none","H","He","Li","Be","B","C","N","O","F","Ne","Na","Mg","Al","Si","P","S");
		$atom_arr = array("none","Hydrogen","Helium","Lithium","Beryllium","Boron","Carbon","Nitrogen","Oxygen","Fluorine","Neon","Sodium","Magnesium","Aluminum","Silicon","Phosphorus","Sulfur");
		foreach($atom_arr as $atom_number=>$atom_name){
			if($planet_number == $atom_number){
				$user_atom_name = $atom_name;
				$user_atom_number = $atom_number;
			}
		}

$atom_file = plugins_url('img/atoms/',__FILE__);
$atom_icon = $atom_file.$user_atom_name.'.png';
		$user_atom_info = array ( 
				"user_atom_number" => $user_atom_number,
				"user_atom_name" => $user_atom_name,
			   "discription" => "Sample",
			   "atom_icon" => $atom_icon
			   );

 return $user_atom_info;
}
function wp_astology_form($atts) {
global $redux_demo;
	if(isset($_POST['submit_astro'])){
		$user_dob = $_POST['user_dob1'];
		//print_r($user_dob);
		$user_dob_str = preg_replace("/[\s-]+/", "", $user_dob);
		//print_r($user_dob);
		$user_dob_arr = str_split($user_dob_str);
		$user_dob_arr_for_date = str_split($user_dob_str,2);
		$user_dob_date = $user_dob_arr_for_date[1];
		$user_dob_month = $user_dob_arr_for_date[0];
		$user_life_number = get_life_number_by_dob($user_dob_arr);
		$life_number = $user_life_number['life_number'];
		$user_planet = get_planet_by_life_number($life_number);
		$user_zodiac_sign = get_zodiac_by_dob($user_dob_date,$user_dob_month);
		$user_palent_number = $user_planet['palent_number'];		// Libra
		$user_element = get_element_by_planet_number($user_palent_number);    // Libra
		$user_atom = get_atom_by_planet_number($user_palent_number);    // Libra
		
		$rt_planet_option = $redux_demo['planet_option'];
		$planet_description = $redux_demo['planet_description'];
		$planet_comb = array_combine($rt_planet_option, $planet_description);
		foreach($planet_comb as $planet_key => $planet_value){
			if($planet_key == $user_palent_number ){
				$palent_descrption = $planet_value;
			}
		}
		
		$rt_life_number_option = $redux_demo['life_number_option'];
		$rt_life_number_descrption = $redux_demo['life_number_descrption'];
		$life_number_comb = array_combine($rt_life_number_option, $rt_life_number_descrption);
		foreach($life_number_comb as $life_number_key => $life_number_value){
			if($life_number_key == $life_number ){
				$life_number_descrption = $life_number_value;
			}
		}
		$rt_zodiacSign = $user_zodiac_sign['zodiacSign'];
		$rt_astrology_sign_option = $redux_demo['astrology_sign_option'];
		$rt_astrology_sign_descrption = $redux_demo['astrology_sign_descrption'];
		$astrology_sign_comb = array_combine($rt_astrology_sign_option, $rt_astrology_sign_descrption);
		foreach($astrology_sign_comb as $astrology_sign_key => $astrology_sign_value){
			if($astrology_sign_key == $rt_zodiacSign ){
				$astrology_sign_descrption = $astrology_sign_value;
			}
		}
		
		$des_element_number = $user_element['element_number'];
		$rt_element_option = $redux_demo['element_option'];
		$rt_element_descrption = $redux_demo['element_descrption'];
		$element_comb = array_combine($rt_element_option, $rt_element_descrption);
		foreach($element_comb as $element_key => $element_value){
			if($element_key == $des_element_number ){
				$element_descrption = $element_value;
			}
		}
		
		$des_atom_number = $user_atom['user_atom_number'];
		$rt_atom_option = $redux_demo['atom_option'];
		$rt_atom_descrption = $redux_demo['atom_descrption'];
		$atom_comb = array_combine($rt_atom_option, $rt_atom_descrption);
		foreach($atom_comb as $atom_key => $atom_value){
			if($atom_key == $des_atom_number ){
				$atom_descrption = $atom_value;
			}
		}
		?>
		<script>		
		jQuery(document).ready(function() {	
			jQuery(".user-form").hide();
		});
		</script>
		
		<div class="smart-wrap">
		<div class="smart-forms smart-container wrap-1">
		
		<div class="form-body smart-steps stp-five">
		<?php 
		$step1_price = $redux_demo['life_number_option'];
		?>
		<form  id="smart-form1" method="post">
		 <h2>Life Number</h2>
         <fieldset>
			<div class="section life_num_roman">
				<label class="field prepend-icon asteps_description">
					<span class="input-hint img_parent"><p>Your Life Number: <span class="sec_title" ><?php echo $user_life_number['life_number'];?></span></p></span>
					<span class="input-hint"><h2 class="life_num_rword"><?php echo $user_life_number['life_number_roman'];?></h2>
				</label>
			</div><!-- end section -->
			<div class="section description">
				<label class="field prepend-icon asteps_description">
					<span class="input-hint"> 
						<strong>Description:</strong> <?php echo $life_number_descrption; ?>
					</span>   
				</label>
			</div><!-- end section -->					
			
         </fieldset>
		 <h2>Astrology Sign</h2>
         <fieldset>
		   <div class="section zodiac_icon">
				<label class="field prepend-icon ">
				<span class="input-hint img_parent"> <p>Your Astrology Sign is:  <span class="sec_title" ><?php echo $user_zodiac_sign['zodiacSign'];?></span></p>
					</span> 
					<span class="input-hint img_parent"> 
					   <?php echo '<img src="'.$user_zodiac_sign['zodic_icon'].'">';?>
					</span>   
				</label>
			</div><!-- end section --> 
		   <div class="section description">
				<label class="field prepend-icon asteps_description">
					<span class="input-hint"> 
						<strong>Description:</strong> <?php echo $astrology_sign_descrption;?>
					</span>   
				</label>
			</div><!-- end section -->
		   
		 </fieldset>
		 <h2>Element</h2>
         <fieldset>       
		    <div class="section user_atom_name">
				<label class="field prepend-icon ">
				<span class="input-hint img_parent"> 
						<p>Your Element is:  <span class="sec_title" ><?php echo $user_element['element_name'];?></span></p>
					</span>
					<span class="input-hint img_parent"> 
						<?php echo '<img class="img_parent" src="'.$user_element['element_icon'].'">';?>
					</span>   
				</label>
			</div><!-- end section --> 
			<div class="section description">
				<label class="field prepend-icon asteps_description">
					<span class="input-hint"> 
						<strong>Description:</strong> <?php echo $atom_descrption;?>
					</span>   
				</label>
			</div><!-- end section -->
		 </fieldset>
		 
		<h2>Atom</h2>
         <fieldset>       
		    <div class="section user_atom_name">
				<label class="field prepend-icon ">
				<span class="input-hint img_parent"> 
						<p>Your Atom is:  <span class="sec_title" ><?php echo $user_atom['user_atom_name'];?></span></p>
					</span>
					<span class="input-hint img_parent"> 
						<?php echo '<img src="'.$user_atom['atom_icon'].'">';?>
					</span>   
				</label>
			</div><!-- end section --> 
			<div class="section description">
				<label class="field prepend-icon asteps_description">
					<span class="input-hint"> 
						<strong>Description:</strong> <?php echo $atom_descrption;?>
					</span>   
				</label>
			</div><!-- end section -->
		 </fieldset>
		  <h2>Planet</h2>
         <fieldset>
		  <div class="frm-row">
			<div class="section user_planet_icon">
				<label class="field">
				<span class="input-hint img_parent"> <p>Your Planet is:  <span class="sec_title" ><?php echo $user_planet['planet_name'];?></span></p>
					</span>  
					<span class="input-hint img_parent"> 
						<?php echo '<img src="'.$user_planet['planet_icon'].'">';?>
					</span>   
				</label>
			</div><!-- end section -->
			<div class="section description">
				<label class="field prepend-icon asteps_description">
					<span class="input-hint"> 
						<strong>Description:</strong> <?php echo $palent_descrption?>
					</span>   
				</label>
			</div><!-- end section -->
         </fieldset>	
		 	
		</form>
		
		</div>
				
		</div>
		</div>			
		
		
		
		
	<?php	
	}
	?>
	<?php ob_start(); ?>
		<div class="smart-wrap user-form">
		<div class="smart-forms smart-container wrap-1">
		
		<div class="form-body">
		<form  id="smart-form" method="post">
		
		<div class="frm-row">
		<div class="section colm colm6">
			<label for="Name" class="field prepend-icon">
			<input type="text" name="user_name" placeholder="Your Full Name" class="gui-input required percentage" id="user_name" value="" > 
			<b class="tooltip tip-right-top"><em> Hey buddy! iam a top left tooltip.</em></b>
			<span class="field-icon"><i class="fa fa-user"></i></span>  					
			</label>
		</div>		
		<div class="section colm colm6">
			<label for="Gender" class="field select prepend-icon">
			<select class="gui-input" id="user_gender" name="user_gender">	
			<option class="male" value="Gerber" > Male </option>
			<option class="female" value="Leatherman" > Female </option>
			</select><i class="arrow"></i>
			</label>
		</div>
		</div>
		
		<div class="frm-row">		
		<div class="section colm colm12">
			<label for="dateofbirth" class="field prepend-icon">
			<input type="text" name="user_dob1" placeholder="Date (MM/DD/YYY)" class="gui-input required Spercentage" id="user_dob1" value=""  required> 
			<b class="tooltip tip-right-top"><em> Hey buddy! iam a top left tooltip.</em></b>
			<span class="field-icon"><i class="fa fa-calendar"></i></span>  					
			</label>
		</div>
		</div>
		<div class="frm-row">		
		<div class="section colm colm12">
			<label for="dateofbirth" class="field prepend-icon">
			<input type="submit" name="submit_astro" placeholder="" class="gui-input required button btn-primary" id="user_dob_submit" value="Submit"  required> 
			</label>
		</div>
		</div>
		</form>
		</div>
		</div>
		</div>
		<br/>
		
			
		<script>
		
		jQuery(document).ready(function() {	
			jQuery("#user_dob1").datepicker({
				numberOfMonths: 1,
				prevText: '<i class="fa fa-chevron-left" />',
				nextText: '<i class="fa fa-chevron-right" />',            
				showButtonPanel: false,
				dateFormat: 'mm-dd-yy',
				changeMonth: true,
				changeYear: true,
				yearRange: '1900:2018'				
			});			
		});				
		</script>
<style>
.field.prepend-icon img {
width: 250px;
text-align: center;
}
</style>	
		
<?php
	return ob_get_clean();
}
function ts_add_plugin_action_links( $links ) {

	return array_merge(
		array(
			'settings' => '<a href="' . get_bloginfo( 'wpurl' ) . '/wp-admin/admin.php?page=SternTaxiPage">Settings</a>'
		),
		$links
	);
}

function stern_taxi_fares_script_back_css() {

}

function stern_taxi_fares_script_front_css() {
		/* CSS */

        wp_register_script('ins_jqueryuicostom', plugins_url('js/jquery-ui-custom.min.js', __FILE__ ),array('jquery'));
        wp_enqueue_script('ins_jqueryuicostom');
  
		//wp_register_style('stern_bootstrapValidatorMinCSS', plugins_url('css/bootstrapValidator.min.css',__FILE__));
		//wp_enqueue_style('stern_bootstrapValidatorMinCSS');
		
		wp_register_style('smartForms', plugins_url('css/smart-forms.css',__FILE__));
        wp_enqueue_style('smartForms');
        

		wp_register_style( 'ins_steps_css', plugins_url('css/jquery.steps.css', __FILE__ ));
		wp_enqueue_style( 'ins_steps_css');

		wp_register_style( 'astro_font_awesome', plugins_url('css/font-awesome.min.css', __FILE__ ));
		wp_enqueue_style( 'astro_font_awesome');

		wp_register_style( 'ins_stepper', plugins_url('css/jquery.stepper.css', __FILE__ ));
		wp_enqueue_style( 'ins_stepper');

		wp_register_style( 'ins_sfcustom', plugins_url('css/smartforms-custom.css', __FILE__ ));
		wp_enqueue_style( 'ins_sfcustom');
		
		wp_register_style( 'ins_smartaddon', plugins_url('css/smart-addons.css', __FILE__ ));
		wp_enqueue_style( 'ins_smartaddon');

		wp_register_style( 'ins_smartforms_modal', plugins_url('css/smartforms-modal.css', __FILE__ ));
		wp_enqueue_style( 'ins_smartforms_modal');
			
		wp_register_style( 'ins_popupn', plugins_url('css/popup.min.css', __FILE__ ));
		wp_enqueue_style( 'ins_popupn');

        wp_register_style('stern_taxi_fare_datetimepicker', plugins_url('css/bootstrap-datetimepicker.css',__FILE__));
        wp_enqueue_style('stern_taxi_fare_datetimepicker');

        wp_register_style('ins_style', plugins_url('css/ins_style.css',__FILE__));
        wp_enqueue_style('ins_style');
		wp_register_style('tabel_style', plugins_url('css/tabel_style.css',__FILE__));
        wp_enqueue_style('tabel_style');
		wp_register_style('tabel_style', plugins_url('css/bootstrap.css',__FILE__));
        wp_enqueue_style('tabel_style');
}

		add_action( 'wp_ajax_my_ajax_rt', 'my_ajax_rt' );
		add_action( 'wp_ajax_nopriv_my_ajax_rt', 'my_ajax_rt' );
function my_ajax_rt() {
	
}

function stern_taxi_fares_script_back_js() {
	
}



function stern_taxi_fares_script_front_js() {
 		

	  // wp_register_script('ins_additionalmethords', plugins_url('js/additional-methods.min.js', __FILE__ ),array('jquery'));
       //wp_enqueue_script('ins_additionalmethords');

		//wp_register_script('stern_bootstrapValidatorMin', plugins_url('js/bootstrapValidator.min.js', __FILE__ ),array('jquery'));
       // wp_enqueue_script('stern_bootstrapValidatorMin');
			   
        wp_register_script('ins_steps', plugins_url('js/jquery.steps.min.js', __FILE__ ),array('jquery'));
        wp_enqueue_script('ins_steps');

        wp_register_script('ins_sslider-pips', plugins_url('js/jquery-ui-slider-pips.min.js', __FILE__ ),array('jquery'));
        wp_enqueue_script('ins_sslider-pips');

        wp_register_script('ins_jqueryforms', plugins_url('js/jquery.form.min.js', __FILE__ ),array('jquery'));
        wp_enqueue_script('ins_jqueryforms');
       
	    wp_register_script('astro_validate', plugins_url('js/jquery.validate.min.js', __FILE__ ),array('jquery'));
        wp_enqueue_script('astro_validate');

       // wp_register_script('ins_jquery19', plugins_url('js/jquery-1.9.1.min.js', __FILE__ ),array('jquery'));
       // wp_enqueue_script('ins_jquery19');


        wp_register_script('ins_jqueryuicostom', plugins_url('js/jquery-ui-custom.min.js', __FILE__ ),array('jquery'));
        wp_enqueue_script('ins_jqueryuicostom');
        		
		wp_register_script('ins_jqueryplaceholder', plugins_url('js/jquery.placeholder.min.js', __FILE__ ),array('jquery'));
        wp_enqueue_script('ins_jqueryplaceholder');

		wp_register_script('ins_touch', plugins_url('js/jquery-ui-touch-punch.min.js', __FILE__ ),array('jquery'));
        wp_enqueue_script('ins_touch');
		
		wp_register_script('ins_smartforms', plugins_url('js/smart-form.js', __FILE__ ),array('jquery'));
        wp_enqueue_script('ins_smartforms');
		
		wp_register_script('jquery_formshowhide', plugins_url('js/jquery.formShowHide.min.js', __FILE__ ),array('jquery'));
        wp_enqueue_script('jquery_formshowhide');

       //	wp_register_script('stern_taxi_fare', plugins_url('js/stern_taxi_fare.js', __FILE__ ),array('jquery'));
		//wp_localize_script('stern_taxi_fare', 'my_ajax_object',	array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );
}
